import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'resize_card.dart';

class ErrorCard extends ConsumerWidget {
  final Widget child;
  const ErrorCard({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final double cardsHeight = ref.watch(
        panelStateProvider.select((value) => value.panelHeight.whenOpen));
    CardSizeManager cardSizeManager = CardSizeManager(Size(
        MediaQuery.of(context).size.width,
        cardsHeight * MediaQuery.of(context).size.height));
    return Resizer(
      cardSizeManager: cardSizeManager,
      child: LayoutBuilder(builder: (context, constraints) {
        double availabeHeight = constraints.maxHeight - 16 - 2; // Try padding 8
        double scale = (availabeHeight) / cardSizeManager.designHeight;

        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
          child: Container(
            //margin: EdgeInsets.only(bottom: bottomPad),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
              border: Border.all(
                  color: Theme.of(context).textTheme.bodySmall!.color!,
                  width: 1),
            ),
            child:
                SizedBox(height: availabeHeight, child: Center(child: child)),
          ),
        );
      }),
    );
  }
}
