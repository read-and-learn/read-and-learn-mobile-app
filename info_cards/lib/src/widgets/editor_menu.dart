import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class EditorMenu extends StatelessWidget {
  final Function() onCancel;
  final Future<void> Function() onSave;
  final TextEditingController textEditingController;

  const EditorMenu({
    Key? key,
    required this.onCancel,
    required this.onSave,
    required this.textEditingController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
      Flexible(
        child: CardMenuItem(
            onPressed: onCancel, text: "Cancel", iconData: MdiIcons.cancel),
      ),
      Flexible(
        child: CardMenuItem(
            onPressed: () async {
              onSave();
            },
            text: "Save",
            iconData: MdiIcons.contentSave),
      ),
      Flexible(
        child: CardMenuItem(
            onPressed: () async {
              textEditingController.text = await ClipboardManager.paste();
            },
            text: "Paste",
            iconData: MdiIcons.contentPaste),
      ),
    ]);
  }
}
