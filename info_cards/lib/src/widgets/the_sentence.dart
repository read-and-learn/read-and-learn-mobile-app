import 'package:flutter/material.dart';

class TheSentence extends StatelessWidget {
  final List<String> sentence;
  const TheSentence({Key? key, required this.sentence}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scale = MediaQuery.of(context).textScaleFactor;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 4 * scale),
      child: Align(
        alignment: Alignment.center,
        child: Text.rich(
          TextSpan(
            children: sentence
                .map((e) => TextSpan(
                    text: e,
                    style: Theme.of(context).textTheme.displaySmall!.copyWith(
                        decoration:
                            (sentence.length == 3) && (sentence.indexOf(e) == 1)
                                ? TextDecoration.underline
                                : null,
                        fontWeight:
                            (sentence.length == 3) && (sentence.indexOf(e) == 1)
                                ? FontWeight.bold
                                : FontWeight.normal)))
                .toList(),
          ),
          maxLines: 3,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
