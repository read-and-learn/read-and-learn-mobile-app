import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'editor_menu.dart';

// [ATTN] Can we replace by CustomTextField
class Editor extends StatelessWidget {
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  final double menuHeight;
  final double height;
  final Function() onCancel;
  final Future<void> Function() onSave;

  const Editor({
    Key? key,
    required this.focusNode,
    required this.textEditingController,
    required this.menuHeight,
    required this.height,
    required this.onCancel,
    required this.onSave,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: menuHeight,
          child: EditorMenu(
            onSave: onSave,
            textEditingController: textEditingController,
            onCancel: onCancel,
          ),
        ),
        SizedBox(
          height: height,
          child: TextField(
            focusNode: focusNode,
            controller: textEditingController,
            inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"\n"))],
            textInputAction: TextInputAction.done,
            maxLines: 2,
            showCursor: true,
            cursorColor: Theme.of(context).textTheme.bodyMedium!.color,
            cursorWidth: 1,
            decoration: InputDecoration(
              filled: true,
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.onSurface)),
              enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.onSurface)),
              errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide:
                      BorderSide(color: Theme.of(context).colorScheme.error)),
            ),
            style: Theme.of(context).textTheme.bodyMedium,
          ),
        ),
      ],
    );
  }
}
