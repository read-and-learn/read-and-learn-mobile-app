import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TheWord extends ConsumerWidget {
  const TheWord(
      {Key? key,
      required this.word,
      required this.onCopy,
      required this.lang,
      this.audioPlayer})
      : super(key: key);
  final String word;

  final Future<void> Function(String text) onCopy;
  final String lang;
  final Widget? audioPlayer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final scale = MediaQuery.of(context).textScaleFactor;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CardMenuItem(
                  text: "Copy",
                  iconData: MdiIcons.contentCopy,
                  onPressed: () async {
                    onCopy(word).then((value) =>
                        ScaffoldMessenger.of(context).showSnackBar(
                          (SnackBar(
                              content: Text("$word is copied into clipboard"))),
                        ));
                  },
                ),
                if (audioPlayer != null) audioPlayer!,
                if (audioPlayer != null)
                  SizedBox(
                    width: 4 * scale,
                  ),
                Flexible(
                  child: FittedBox(
                    child: Text(
                      word,
                      style: Theme.of(context).textTheme.displayLarge,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                const Spacer(),
              ],
            ),
          ),
          CardMenuItem(
              onPressed: () {
                ref
                    .read(panelActiveStateNotifierProvider.notifier)
                    .disablePanel();
              },
              text: "Close",
              iconData: Icons.close)
        ],
      ),
    );
  }
}
