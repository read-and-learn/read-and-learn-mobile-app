import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DictionaryLookup extends ConsumerWidget {
  const DictionaryLookup({
    Key? key,
    required this.title,
    required this.text,
  }) : super(key: key);

  final String title;
  final String text;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final panelState = ref.watch(panelStateProvider);
    bool isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected));

    final sourceLang = panelState.sourceLang;
    final targetLang = panelState.targetLang;
    final langCode =
        sourceLang == targetLang ? sourceLang : "$sourceLang → $targetLang";

    return Padding(
      padding: const EdgeInsets.only(bottom: 1, left: 1, right: 1),
      child: ColoredBox(
        color: Theme.of(context).disabledColor,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Row(
            children: [
              Flexible(
                //flex: 3,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TextButton(
                    onPressed: isConnected
                        ? () async {
                            await DictionaryManager.getDictionaryManager(
                                    text: text,
                                    sL: ref.watch(panelStateProvider
                                        .select((value) => value.sourceLang)),
                                    tL: ref.watch(panelStateProvider
                                        .select((value) => value.targetLang)))
                                .launch(context);
                          }
                        : null,
                    child: Text(
                      title,
                      style: isConnected
                          ? Theme.of(context).textTheme.headlineLarge
                          : Theme.of(context).textTheme.headlineLarge!.copyWith(
                                decoration: TextDecoration.lineThrough,
                              ),
                    ),
                  ),
                ),
              ),
              CardMenuItem(
                  iconData: MdiIcons.cogOutline,
                  onPressed: () {},
                  text: langCode)
            ],
          ),
        ),
      ),
    );
  }
}
