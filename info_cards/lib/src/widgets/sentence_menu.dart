import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SentenceMenu extends ConsumerWidget {
  final List<String> sentence;

  final String lang;
  final Widget? audioPlayer;

  const SentenceMenu({
    Key? key,
    required this.sentence,
    required this.lang,
    this.audioPlayer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    bool isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected));
    final panelState = ref.watch(panelStateProvider);
    //final scale = MediaQuery.of(context).textScaleFactor;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CardMenuItem(
              text: "Copy",
              iconData: MdiIcons.contentCopy,
              onPressed: () async {
                ClipboardManager.copy(sentence.join().trim())
                    .then((value) => ScaffoldMessenger.of(context).showSnackBar(
                          (SnackBar(
                              content: Text(
                                  "[${sentence.join().trim()}] is copied into clipboard"))),
                        ));
              },
            ),
            if (audioPlayer != null) audioPlayer!,
            if (!panelState.isSaved && isConnected)
              CardMenuItem(
                  text: "Save",
                  iconData: MdiIcons.contentSave,
                  onPressed: () async {
                    await ref
                        .read(textStringsNotifierProvider(
                                panelState.textString.lang)
                            .notifier)
                        .addFeat(
                            textString: panelState.textString,
                            feat: Feat(
                                attr: Attribute.usage,
                                value: TextString(
                                    text: sentence.join('').trim(),
                                    lang: panelState.textString.lang,
                                    type: TextType.sentence)));
                  })
            else if (isConnected)
              CardMenuItem(
                text: "Saved",
                iconData: MdiIcons.check,
                onPressed: () {},
              )
          ],
        ),
        CardMenuItem(
            onPressed: () {
              ref
                  .read(panelActiveStateNotifierProvider.notifier)
                  .disablePanel();
            },
            text: "Close",
            iconData: Icons.close)
      ],
    );
  }
}
