import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TheMeaning extends ConsumerWidget {
  final Function()? onEdit;
  final double menuHeight;
  final double height;
  const TheMeaning(
      {Key? key,
      required this.onEdit,
      required this.menuHeight,
      required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final meaning = ref.read(panelStateProvider).meaning;
    bool isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected));
    return Column(
      children: [
        if (meaning != null && isConnected)
          SizedBox(
            height: menuHeight,
            child: Align(
              alignment: Alignment.centerLeft,
              child: CardMenuItem(
                iconData: MdiIcons.pencilOutline,
                onPressed: onEdit,
                text: 'Edit',
              ),
            ),
          ),
        if (meaning == null)
          SizedBox(
            height: height + menuHeight,
            child: Align(
              alignment: Alignment.center,
              child: CardMenuItem(
                iconData:
                    isConnected ? MdiIcons.pencilOutline : MdiIcons.wifiOff,
                onPressed: isConnected ? onEdit : null,
                text: isConnected ? 'Add Meaning' : 'Offline',
              ),
            ),
          )
        else
          SizedBox(
            height: height,
            child: Text(
              meaning,
              style: Theme.of(context).textTheme.displayMedium,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          )
      ],
    );
  }
}
