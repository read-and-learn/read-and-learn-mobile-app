import 'dart:math';

import 'package:flutter/material.dart';

class CardSizeManager {
  final double designHeight = 400 / 2;
  final double aspectRatio = 1.25;

  late double height;
  late double width;
  final Size size;

  CardSizeManager(this.size) {
    height = max(40, size.height);
    width =
        max(40 * aspectRatio, size.width - 8); // 8 is for padding done in panel

    if (width < height * aspectRatio) {
      height = width / aspectRatio;
    } else {
      width = height * aspectRatio;
    }
  }
}

class Resizer extends StatelessWidget {
  final Widget child;
  final CardSizeManager cardSizeManager;
  const Resizer({Key? key, required this.child, required this.cardSizeManager})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SizedBox(
            height: cardSizeManager.height,
            width: cardSizeManager.width,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: AspectRatio(
                aspectRatio: cardSizeManager.aspectRatio,
                child: child,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
