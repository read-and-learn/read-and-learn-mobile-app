import 'package:basic_services/basic_services.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'resize_card.dart';

import 'widgets/dictionary_lookup.dart';
import 'widgets/editor.dart';
import 'widgets/the_meaning.dart';
import 'widgets/the_word.dart';

class MeaningCard extends ConsumerStatefulWidget {
  const MeaningCard({Key? key, this.audioPlayer}) : super(key: key);
  final Widget? audioPlayer;
  @override
  ConsumerState<MeaningCard> createState() => _MeaningCardState();
}

class _MeaningCardState extends ConsumerState<MeaningCard> {
  String? meaning;
  late bool editorEnabled;
  late FocusNode focusNode;
  late TextEditingController textEditingController;

  @override
  void initState() {
    focusNode = FocusNode();
    final meaning = ref.read(panelStateProvider).meaning;
    textEditingController = TextEditingController(text: meaning);
    editorEnabled = meaning == null;
    super.initState();
  }

  @override
  void dispose() {
    focusNode.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final cardsHeight = ref.watch(
        panelStateProvider.select((value) => value.panelHeight.whenOpen));
    CardSizeManager cardSizeManager = CardSizeManager(Size(
        MediaQuery.of(context).size.width,
        cardsHeight * MediaQuery.of(context).size.height));

    ref.listen<String?>(panelStateProvider.select((value) => value.meaning),
        (String? previous, String? meaning) {
      editorEnabled = (meaning == null);
      meaning = meaning;
      textEditingController.text = meaning ?? "";
    });

    return Resizer(
      cardSizeManager: cardSizeManager,
      child: LayoutBuilder(builder: (context, constraints) {
        double availabeHeight = constraints.maxHeight - 16 - 2; // Try padding 8
        double scale = (availabeHeight) / cardSizeManager.designHeight;
        List<double> heights;
        heights = [
          (availabeHeight * 0.2).floorToDouble(),
          (availabeHeight * 0.2).floorToDouble(),
          (availabeHeight * 0.3).floorToDouble(),
          (availabeHeight * 0.29).floorToDouble(),
        ];
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
          child: Container(
            //margin: EdgeInsets.only(bottom: bottomPad),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
              border: Border.all(
                  color: Theme.of(context).textTheme.bodySmall!.color!,
                  width: 1),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                    height: heights[0],
                    child: TheWord(
                        word: ref.watch(
                            panelStateProvider.select((value) => value.word)),
                        onCopy: ClipboardManager.copy,
                        lang: ref.watch(panelStateProvider
                            .select((value) => value.sourceLang)),
                        audioPlayer: widget.audioPlayer)),
                const Divider(),
                Padding(
                  padding: EdgeInsets.only(left: 4 * scale, right: 4 * scale),
                  child: meaningWidgets(heights),
                ),
                SizedBox(
                  height: heights[3],
                  child: DictionaryLookup(
                    title: "Open Dictionary",
                    text: ref.watch(
                        panelStateProvider.select((value) => value.word)),
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget meaningWidgets(List<double> heights) {
    bool isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected));
    if (editorEnabled && isConnected) {
      return SizedBox(
        height: heights[2] + heights[1],
        child: Editor(
          focusNode: focusNode,
          textEditingController: textEditingController,
          menuHeight: heights[1],
          height: heights[2],
          onSave: onSave,
          onCancel: onCancel,
        ),
      );
    } else {
      return SizedBox(
        height: heights[2] + heights[1],
        child: TheMeaning(
          onEdit: isConnected ? onEdit : null,
          menuHeight: heights[1],
          height: heights[2],
        ),
      );
    }
  }

  Future<void> onSave() async {
    final PanelState panelState = ref.read(panelStateProvider);

    if (textEditingController.text.isNotEmpty) {
      meaning = textEditingController.text;
      setState(() {
        if (focusNode.hasFocus) {
          focusNode.unfocus();
        }
        if (meaning != null) {
          textEditingController.text = meaning!;
        } else {
          textEditingController.text = "";
        }

        editorEnabled = false;
      });

      await ref
          .read(textStringsNotifierProvider(panelState.sourceLang).notifier)
          .addFeat(
              textString: panelState.textString,
              feat: Feat(
                  attr: Attribute.meaning,
                  value: TextString(
                      text: textEditingController.text,
                      lang: panelState.nativeLanguage)));
    }
  }

  onCancel() {
    setState(() {
      if (focusNode.hasFocus) {
        focusNode.unfocus();
      }
      if (meaning != null) {
        textEditingController.text = meaning!;
      } else {
        textEditingController.text = "";
      }

      editorEnabled = false;
    });
  }

  onEdit() {
    setState(() {
      focusNode.requestFocus();

      editorEnabled = true;
    });
  }
}
