import 'package:flutter/material.dart';

import 'resize_card.dart';

class WaitingCard extends StatelessWidget {
  final double cardsHeight;
  const WaitingCard({Key? key, required this.cardsHeight}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CardSizeManager cardSizeManager = CardSizeManager(Size(
        MediaQuery.of(context).size.width,
        cardsHeight * MediaQuery.of(context).size.height));
    return Resizer(
      cardSizeManager: cardSizeManager,
      child: LayoutBuilder(builder: (context, constraints) {
        double availabeHeight = constraints.maxHeight - 16 - 2; // Try padding 8
        double scale = (availabeHeight) / cardSizeManager.designHeight;

        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
          child: Container(
            //margin: EdgeInsets.only(bottom: bottomPad),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
              border: Border.all(
                  color: Theme.of(context).textTheme.bodySmall!.color!,
                  width: 1),
            ),
            child: SizedBox(
                height: availabeHeight,
                child: const Center(child: CircularProgressIndicator())),
          ),
        );
      }),
    );
  }
}
