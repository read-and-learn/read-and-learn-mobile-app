import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'resize_card.dart';

import 'widgets/dictionary_lookup.dart';
import 'widgets/sentence_menu.dart';
import 'widgets/the_sentence.dart';

class SentenceCard extends ConsumerWidget {
  const SentenceCard({key, this.audioPlayer}) : super(key: key);

  final Widget? audioPlayer;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final cardsHeight = ref.watch(
        panelStateProvider.select((value) => value.panelHeight.whenOpen));
    List<double> heights;
    final panelState = ref.watch(panelStateProvider);

    final sourceLang = panelState.sourceLang;

    CardSizeManager cardSizeManager = CardSizeManager(Size(
        MediaQuery.of(context).size.width,
        cardsHeight * MediaQuery.of(context).size.height));

    return Resizer(
      cardSizeManager: cardSizeManager,
      child: LayoutBuilder(builder: (context, constraints) {
        double availabeHeight = constraints.maxHeight - 16; // Try padding 8
        double scale = (availabeHeight) / cardSizeManager.designHeight;

        heights = [
          (availabeHeight * 0.2).floorToDouble(),
          (availabeHeight * 0.55).floorToDouble(),
          (availabeHeight * 0.24).floorToDouble(),
        ];
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: scale),
          child: Container(
            //margin: EdgeInsets.only(bottom: bottomPad),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
              border: Border.all(
                  color: Theme.of(context).textTheme.bodySmall!.color!,
                  width: 1),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: heights[0],
                  child: SentenceMenu(
                    sentence: panelState.sentenceParts,
                    lang: sourceLang,
                    audioPlayer: audioPlayer,
                  ),
                ),
                SizedBox(
                  height: heights[1],
                  child: TheSentence(sentence: panelState.sentenceParts),
                ),
                SizedBox(
                  height: heights[2],
                  child: DictionaryLookup(
                    title: "Open Translator",
                    text: panelState.sentence,
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
