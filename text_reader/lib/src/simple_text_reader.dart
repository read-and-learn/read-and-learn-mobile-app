import 'dart:math';

import 'package:flutter/material.dart';

import 'text_custom_painter.dart';

class SimpleTextReader extends StatefulWidget {
  final ScrollController controller;
  final String title;
  final List<String> content;
  final Widget appBar;
  final bool isPanelOpen;
  final double panelClosedSizeFract;
  final double panelOpenSizeFract;
  final double initialFontSize;

  final Future<bool> Function(TapDownDetails? tapDownDetails, int? textOffset,
      ScrollPosition? scrollPosition)? onTap;
  final Future<bool> Function(TapDownDetails tapDownDetails, int textOffset,
      ScrollPosition? scrollPosition)? onDoubleTap;
  const SimpleTextReader(
      {Key? key,
      required this.controller,
      this.onTap,
      this.onDoubleTap,
      required this.content,
      required this.title,
      required this.appBar,
      required this.isPanelOpen,
      required this.panelClosedSizeFract,
      required this.panelOpenSizeFract,
      required this.initialFontSize})
      : super(key: key);

  @override
  State<SimpleTextReader> createState() => _SimpleTextReaderState();
}

class _SimpleTextReaderState extends State<SimpleTextReader> {
  double blankEnd = 0.0;

  TapDownDetails? _tapDownDetails;
  late TextPainter textPainter;

  double _baseScaleFactor = 1;
  double _scaleFactor = 1;
  late double fontSize;

  @override
  void initState() {
    fontSize = widget.initialFontSize;

    super.initState();
  }

  void getTapDownDetails(details) {
    setState(() {
      _tapDownDetails = details;
    });
  }

  void scrollToPosition(double position) {
    widget.controller.animateTo(
      position,
      duration: const Duration(seconds: 2),
      curve: Curves.fastOutSlowIn,
    );
  }

  void onTap(BuildContext context) async {
    bool isOpening = false;
    var sPosition = (Scrollable.of(context) == null)
        ? null
        : Scrollable.of(context)!.position;

    if (widget.onTap != null) {
      isOpening = await widget.onTap!(
          _tapDownDetails,
          textPainter
              .getPositionForOffset(_tapDownDetails!.localPosition)
              .offset,
          sPosition);
    }
    if (isOpening && sPosition != null && _tapDownDetails != null) {
      double hidden = _tapDownDetails!.globalPosition.dy -
          (sPosition.viewportDimension -
              (widget.panelOpenSizeFract * sPosition.viewportDimension));
      if (hidden > 0) {
        scrollToPosition(sPosition.pixels + hidden + 40);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 4),
        child: contentScreen(
            contentBuilder(context, widget.content, widget.initialFontSize)),
      ),
    );
  }

  TextSpan contentBuilder(
      BuildContext context, List<String> contentListTemp, double fontSize) {
    if (contentListTemp.isEmpty) {
      return const TextSpan(text: "Story is blank!!!");
    }
    TextSpan textWidget;

    TextStyle readerNormalStyle =
        Theme.of(context).textTheme.bodySmall!.copyWith(fontSize: fontSize);
    TextStyle readerSentenceHighlightstyle = Theme.of(context)
        .textTheme
        .bodySmall!
        .copyWith(
            fontSize: fontSize, color: const Color.fromARGB(255, 252, 106, 95));
    TextStyle readerWordHighlightstyle = Theme.of(context)
        .textTheme
        .bodySmall!
        .copyWith(
            fontSize: fontSize,
            decoration: TextDecoration.underline,
            color: const Color.fromARGB(255, 252, 106, 95));

    Map<num, List<TextStyle>>? style = {
      1: [
        readerNormalStyle,
      ],
      3: [
        // only word
        readerNormalStyle,
        readerWordHighlightstyle,
        readerNormalStyle,
      ],
      5: [
        // word in sentence
        readerNormalStyle,
        readerSentenceHighlightstyle,
        readerWordHighlightstyle,
        readerSentenceHighlightstyle,
        readerNormalStyle,
      ]
    };
    if ([1, 3, 5].contains(contentListTemp.length)) {
      // We have captured either word, or word inside sentence
      textWidget = TextSpan(
          text: "",
          children: contentListTemp
              .map(
                (e) => TextSpan(
                    text: e,
                    style: style[contentListTemp.length]![
                        contentListTemp.indexOf(e)]),
              )
              .toList());
    } else {
      textWidget = TextSpan(
          text: "",
          children: contentListTemp
              .map(
                (e) => TextSpan(text: e, style: readerNormalStyle),
              )
              .toList());
    }
    return textWidget;
  }

  Widget contentScreen(TextSpan textWidget) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      double width = constraints.maxWidth;

      textPainter = TextPainter(
          text: textWidget,
          textAlign: TextAlign.justify,
          textDirection: TextDirection.ltr,
          textScaleFactor: _scaleFactor);
      textPainter.layout(minWidth: 20, maxWidth: width - 2 - 8);
      final height = textPainter.height;

      return Column(
        children: [
          const SizedBox(
            height: 8,
          ),
          Container(
            decoration: const BoxDecoration(),
            child: SizedBox(
              width: textPainter.width,
              height: textPainter.height,
              child: GestureDetector(
                onTap: () {
                  return onTap(context);
                },
                onDoubleTap: () {
                  return onTap(context);
                },
                onDoubleTapDown: getTapDownDetails,
                onTapDown: getTapDownDetails,
                onScaleStart: (details) {
                  _baseScaleFactor = _scaleFactor;
                  // print("Start: $details");
                },
                onScaleUpdate: (details) {
                  setState(() {
                    _scaleFactor = _baseScaleFactor * details.scale;
                    _scaleFactor = min(_scaleFactor, 3);
                    _scaleFactor = max(_scaleFactor, 0.3);
                  });
                },
                child: CustomPaint(
                  size: Size(width - 8, height), // Parent width, text height
                  painter: TextCustomPainter(textPainter),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 12.0,
          ),
          const EndIndicator(),
          SizedBox(
            height: widget.isPanelOpen
                ? widget.panelOpenSizeFract * MediaQuery.of(context).size.height
                : widget.panelClosedSizeFract *
                    MediaQuery.of(context).size.height,
          )
        ],
      );
    });
  }
}

class EndIndicator extends StatelessWidget {
  const EndIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Spacer(),
        Expanded(
          child: Column(
            children: const [
              Divider(
                thickness: 3,
              ),
            ],
          ),
        ),
        const Expanded(
            child: Align(alignment: Alignment.center, child: Text("The End"))),
        Expanded(
          child: Column(
            children: const [
              Divider(
                thickness: 3,
              ),
            ],
          ),
        ),
        const Spacer(),
      ],
    );
  }
}
