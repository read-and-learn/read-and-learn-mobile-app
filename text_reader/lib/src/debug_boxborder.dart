import 'package:flutter/material.dart';

class DebugBoxBorder extends StatelessWidget {
  const DebugBoxBorder(
      {Key? key,
      required this.child,
      required this.color,
      this.debugBorder = true})
      : super(key: key);
  final Widget child;
  final Color color;
  final bool debugBorder;
  @override
  Widget build(BuildContext context) {
    return debugBorder ? Container(color: color, child: child) : child;
  }
}
