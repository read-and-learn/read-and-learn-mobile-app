import 'dart:math';

import 'package:flutter/material.dart';

class ReaderSliverAppBar extends StatelessWidget {
  final TextSpan title;
  final Widget appBar;

  const ReaderSliverAppBar({
    Key? key,
    required this.title,
    required this.appBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      toolbarHeight: 0,
      automaticallyImplyLeading: false,
      expandedHeight: 80,
      floating: true,
      pinned: true,
      // stretch: true,
      backgroundColor: Theme.of(context).primaryColor,
      // backgroundColor: Colors.blue,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        //print(constraints.biggest.height);
        return Container(
          constraints: const BoxConstraints(),
          // padding: const EdgeInsets.symmetric(horizontal: 16),
          height: constraints.biggest.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: min(constraints.biggest.height, 24),
              ),
              Expanded(
                  child: TitleBar(
                title: title,
              )),
              // if (constraints.biggest.height > 76) Expanded(child: appBar),
            ],
          ),
        );
      }),
    );
  }
}

class TitleBar extends StatelessWidget {
  final TextSpan title;

  const TitleBar({
    Key? key,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Align(
        alignment: Alignment.center,
        child: FittedBox(child: Text.rich(title)),
      ),
    );
  }
}
