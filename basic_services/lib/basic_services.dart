library basic_services;

export 'src/dictionary_manager.dart';
export 'src/clipboard_manager.dart';
export 'src/url_manager.dart';
