import 'dart:io' as io;

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart' as tabs;
import 'package:flutter/foundation.dart' show kIsWeb;

abstract class DictionaryManager {
  final String url;

  DictionaryManager(String text, String sL, String tL)
      : url = "https://translate.google.com/"
            "?sl=$sL&tl=$tL&op=translate"
            "&text=${text.trim()}";

  Future<bool> launch(BuildContext context);
  static Future<void> onDictSettings(BuildContext context) async {}

  static DictionaryManager getDictionaryManager(
      {required String text, required String sL, required String tL}) {
    DictionaryManager dict;
    if (kIsWeb || io.Platform.isAndroid || io.Platform.isIOS) {
      dict = DictionaryManagerDefault(text, sL, tL);
    } else if (io.Platform.isMacOS) {
      dict = DictionaryManagerMac(text, sL, tL);
    } else {
      // Untested !!
      dict = DictionaryManagerDefault(text, sL, tL);
    }
    return dict;
  }
}

class DictionaryManagerMac extends DictionaryManager {
  DictionaryManagerMac(String text, String sL, String tL) : super(text, sL, tL);

  @override
  Future<bool> launch(BuildContext context) async {
    final Uri url = Uri.parse(super.url);

    try {
      if (!await launchUrl(url, webOnlyWindowName: "_self")) {
        throw 'Could not launch $url';
      }
    } on Exception {
      return false;
    }
    return true;
  }
}

class DictionaryManagerDefault extends DictionaryManager {
  DictionaryManagerDefault(String text, String sL, String tL)
      : super(text, sL, tL);
  @override
  Future<bool> launch(BuildContext context) async {
    try {
      await tabs.launch(
        url,
        customTabsOption: tabs.CustomTabsOption(
          toolbarColor: Theme.of(context).primaryColor,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          animation: tabs.CustomTabsSystemAnimation.slideIn(),
        ),
        safariVCOption: tabs.SafariViewControllerOption(
          preferredBarTintColor: Theme.of(context).primaryColor,
          preferredControlTintColor: Colors.white,
          barCollapsingEnabled: true,
          entersReaderIfAvailable: false,
          dismissButtonStyle: tabs.SafariViewControllerDismissButtonStyle.close,
        ),
      );
      return true;
    } on Exception {
      // An exception is thrown if browser app is not installed on Android device.
      return false;
    }
  }
}
