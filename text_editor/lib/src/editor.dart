import 'dart:math';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';

import 'edit_story.dart';
import 'edit_title.dart';

class Editor extends StatefulWidget {
  final String? title;
  final String? content;
  final List<Widget>? appBarActions;
  final Future<bool> Function({required String title, required String content})
      onSave;
  final Function()? onCancel;

  const Editor(
      {Key? key,
      this.title,
      this.content,
      this.appBarActions,
      this.onCancel,
      required this.onSave})
      : super(key: key);

  @override
  State<Editor> createState() => _EditorState();
}

class _EditorState extends State<Editor> {
  final _formKey = GlobalKey<FormState>();

  static const double toolbarHeight = 60;
  late FocusNode _titleFocusNode;
  late FocusNode _storyFocusNode;
  late TextEditingController _titleController;
  late TextEditingController _storyController;

  bool _savingStory = false;
  late bool nothingChanged;
  @override
  void initState() {
    super.initState();
    _titleFocusNode = FocusNode();
    _storyFocusNode = FocusNode();
    _titleController = TextEditingController();
    _storyController = TextEditingController();
    _storyFocusNode.requestFocus();

    _titleController.text = widget.title ?? "";
    _storyController.text = widget.content ?? "";
    nothingChanged = (_titleController.text == (widget.title ?? "")) ||
        (_storyController.text == (widget.content ?? ""));
    _titleController.addListener(watchChange);
    _storyController.addListener(watchChange);
    _storyController.selection = TextSelection.fromPosition(
        TextPosition(offset: _storyController.text.length));
  }

  watchChange() {
    setState(() {
      nothingChanged = (_titleController.text == (widget.title ?? "")) &&
          (_storyController.text == (widget.content ?? ""));
    });
  }

  @override
  void dispose() {
    _titleFocusNode.dispose();
    _storyFocusNode.dispose();
    _storyController.removeListener(watchChange);
    _titleController.removeListener(watchChange);
    _storyController.dispose();
    _titleController.dispose();

    super.dispose();
  }

  String? storyValidator(String? text) {
    // Valid story must have atleast 10 words.
    if (text == null) return null;
    if (text.isEmpty) {
      return "Write something";
    } else {
      if (text.length < 10) {
        return "Too small text for a story";
      }
      return null;
    }
  }

  String generateTitleIfMissing(String title, String content) {
    if (title.isNotEmpty) {
      return title;
    }
    if (content.isNotEmpty) {
      List<String>? firstLine = content.split(RegExp(r"[\r\n]"))[0].split(" ");
      title = firstLine.getRange(0, min(firstLine.length, 4)).join(" ");
      if (title.length > 25) {
        title = title.substring(0, 25);
      }
      title += " ...";
      return title;
    } else {
      return "unknown";
    }
  }

  Future<void> _saveStory() async {
    _storyFocusNode.unfocus();
    if (_formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Please wait, while saving the story')),
      );
      String content = _storyController.text.trim();
      String title =
          generateTitleIfMissing(_titleController.text.trim(), content);
      bool saveStatus = await widget.onSave(title: title, content: content);
      if (!saveStatus) {
        setState(() {
          _savingStory = false;
        });
      }
      return;
    }
    setState(() {
      _savingStory = false;
      _storyController.selection = TextSelection.fromPosition(
          TextPosition(offset: _storyController.text.length));
    });

    _storyFocusNode.requestFocus();
    return;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);
        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Form(
        key: _formKey,
        child: Scaffold(
          appBar: AppBar(
            leading: (kIsWeb)
                ? (widget.onCancel == null)
                    ? Container()
                    : IconButton(
                        onPressed: () {
                          widget.onCancel!();
                        },
                        icon: const Icon(
                          Icons.home,
                        ))
                : null,
            toolbarHeight: toolbarHeight,
            actions: widget.appBarActions,
            title: TitleEditor(
              toolbarHeight: toolbarHeight,
              controller: _titleController,
              focusNode: _titleFocusNode,
              nextNode: _storyFocusNode,
            ),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                  child: StoryEditor(context,
                      focusNode: _storyFocusNode,
                      controller: _storyController,
                      validator: storyValidator)),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    ElevatedButton.icon(
                        onPressed: nothingChanged
                            ? null
                            : () {
                                _titleController.text = widget.title ?? "";
                                _storyController.text = widget.content ?? "";
                                _storyController.selection =
                                    TextSelection.fromPosition(TextPosition(
                                        offset: _storyController.text.length));
                              },
                        icon: const Icon(Icons.restore),
                        label: const Text("")),
                    const SizedBox(
                      width: 8,
                    ),
                    Expanded(
                      child: ElevatedButton.icon(
                          icon: const Icon(Icons.add),
                          label: Text(_savingStory ? "Saving ..." : "Save"),
                          onPressed: (_savingStory || nothingChanged
                              ? null
                              : () {
                                  setState(() {
                                    _savingStory = true;
                                  });
                                  _saveStory();
                                })),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
