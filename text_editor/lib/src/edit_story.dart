import 'package:flutter/material.dart';

class StoryEditor extends StatelessWidget {
  const StoryEditor(
    this.context, {
    Key? key,
    required this.focusNode,
    required this.controller,
    this.validator,
  }) : super(key: key);

  final BuildContext context;

  final FocusNode focusNode;
  final TextEditingController controller;
  final String? Function(String?)? validator;

  @override
  Widget build(BuildContext context) {
    //Preferences preferences = Provider.of<Preferences>(context);
    return Stack(children: [
      Container(
          margin: const EdgeInsets.all(8),
          padding: const EdgeInsets.symmetric(horizontal: 4),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(
              color: Theme.of(context).colorScheme.primary,
              width: 2.0,
            ),
            color: Theme.of(context).colorScheme.surface,
          ),
          height: double.infinity,
          child: SingleChildScrollView(
            child: TextFormField(
              decoration: const InputDecoration(
                contentPadding: EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8.0),
                border: InputBorder.none,
              ),
              focusNode: focusNode,
              controller: controller,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              minLines: 10,
              textAlign: TextAlign.justify,
              validator: validator,
              showCursor: true,
              cursorRadius: const Radius.circular(2),
              cursorWidth: 5,
              //cursorColor: preferences.readerNormalStyle.color,
            ),
          )),
      Positioned(
        left: 8,
        top: 2,
        child: DecoratedBox(
          decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface.withOpacity(1.0)),
          child: Text("Story", style: Theme.of(context).textTheme.bodySmall),
        ),
      ),
    ]);
  }
}
