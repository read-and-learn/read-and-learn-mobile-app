import 'package:flutter/material.dart';
//import 'package:provider/provider.dart';
//import 'package:read_and_learn/providers/preferences.dart';

class TitleEditor extends StatefulWidget {
  const TitleEditor({
    Key? key,
    required this.toolbarHeight,
    required this.controller,
    required this.focusNode,
    this.nextNode,
  }) : super(key: key);

  final double toolbarHeight;
  final TextEditingController controller;
  final FocusNode focusNode;
  final FocusNode? nextNode;

  @override
  State<TitleEditor> createState() => _TitleEditorState();
}

class _TitleEditorState extends State<TitleEditor> {
  static const _noDecoration = false;
  late String previousText;
  late bool _editMode;

  @override
  void initState() {
    super.initState();
    previousText = widget.controller.text;
    _editMode = false;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_noDecoration) {
      return _coreWidget(context);
    } else {
      return Container(
        decoration: BoxDecoration(
          //border: Border.all(),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).colorScheme.surface,
              offset: const Offset(0.0, 1.0), //(x,y)
              blurRadius: 60.0,
            ),
          ],
        ),
        child: _coreWidget(context),
      );
    }
  }

  Stack _coreWidget(BuildContext context) {
    //Preferences preferences = Provider.of<Preferences>(context);
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.all(5),
          padding: const EdgeInsets.symmetric(vertical: 4),
          height: widget.toolbarHeight - 5,
          width: MediaQuery.of(context).size.width * 2 / 3,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: !_editMode
                    ? Align(
                        alignment: Alignment.center,
                        child: (widget.controller.text == "")
                            ? Text("No title",
                                style: Theme.of(context).textTheme.bodySmall)
                            : Text(
                                widget.controller.text,
                              ))
                    : TextFormField(
                        decoration: InputDecoration(
                          contentPadding:
                              const EdgeInsets.fromLTRB(12.0, 8.0, 12.0, 8.0),
                          floatingLabelAlignment: FloatingLabelAlignment.center,
                          alignLabelWithHint: true,
                          label: Text('Give a suitable title (optional)',
                              style: Theme.of(context).textTheme.labelSmall),
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          prefix: Text("Title ",
                              style: Theme.of(context).textTheme.labelSmall),
                          enabled: _editMode ? true : false,
                        ),
                        showCursor: true,
                        //cursorColor: preferences.readerNormalStyle.color,

                        cursorWidth: 5,
                        cursorRadius: const Radius.circular(3),
                        //cursorColor: Colors,
                        focusNode: widget.focusNode,
                        controller: widget.controller,
                        textInputAction: (widget.nextNode == null)
                            ? TextInputAction.done
                            : TextInputAction.next,

                        onFieldSubmitted: (v) {
                          setState(() {
                            _editMode = false;
                          });
                          if (widget.nextNode != null) {
                            widget.nextNode?.requestFocus();
                          }
                        },
                        maxLines: 1,
                      ),
              ),
              IconButton(
                icon: Icon(_editMode ? Icons.cancel : Icons.edit),
                onPressed: () {
                  setState(() {
                    if (_editMode) {
                      widget.controller.text = previousText;
                      if (widget.nextNode != null) {
                        widget.nextNode?.requestFocus();
                      }
                    } else {
                      previousText = widget.controller.text;
                      widget.focusNode.requestFocus();
                    }
                    _editMode = !_editMode;
                  });
                },
              )
            ],
          ),
        ),
      ],
    );
  }
}
