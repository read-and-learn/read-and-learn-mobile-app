import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../providers/image_handler.dart';

class ImagePickerView extends ConsumerStatefulWidget {
  final Function()? onDiscardImage;
  final Function()? onOpenTextEditor;
  const ImagePickerView(
      {super.key, this.onDiscardImage, required this.onOpenTextEditor});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => ImagePickerViewState();
}

class ImagePickerViewState extends ConsumerState<ImagePickerView> {
  bool waitOnPlatformCall = false;
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Center(
        child: SizedBox(
          child: Column(
            children: [
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                  onPressed: widget.onDiscardImage,
                  icon: const Icon(MdiIcons.close),
                ),
              ),
              Expanded(
                child: TabBarView(
                  children: [
                    InputSourceWidget(
                      title: "Open Editor",
                      hint:
                          "Edit your own story or copy text from any other source",
                      onSelection: widget.onOpenTextEditor,
                    ),
                    InputSourceWidget(
                      title: "Scan with Camera",
                      hint: "Scan a printed media or poster using your camera.",
                      onSelection: () async {
                        setState(() {
                          waitOnPlatformCall = true;
                        });
                        final pickedFile = await ImagePicker()
                            .pickImage(source: ImageSource.camera);
                        if (pickedFile != null) {
                          ref.read(imageHandlerProvider.notifier).imagePath =
                              pickedFile.path;
                        }
                        setState(() {
                          waitOnPlatformCall = false;
                        });
                      },
                    ),
                    InputSourceWidget(
                      title: "Choose from Gallery",
                      hint: "Select a image with text from your gallery",
                      onSelection: () async {
                        setState(() {
                          waitOnPlatformCall = true;
                        });
                        final pickedFile = await ImagePicker()
                            .pickImage(source: ImageSource.gallery);
                        if (pickedFile != null) {
                          ref.read(imageHandlerProvider.notifier).imagePath =
                              pickedFile.path;
                        }
                        setState(() {
                          waitOnPlatformCall = false;
                        });
                      },
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).selectedRowColor,
                  borderRadius: const BorderRadius.only(
                      bottomRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15)),
                ),
                child: TabBar(
                  labelColor: Theme.of(context).colorScheme.primary,
                  unselectedLabelColor:
                      Theme.of(context).colorScheme.inverseSurface,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorPadding: const EdgeInsets.all(5.0),
                  indicatorColor: Colors.blue,
                  indicator: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                          color: Theme.of(context).colorScheme.secondary,
                          width: 3.0),
                    ),
                  ),
                  tabs: const [
                    Tab(
                      text: "Editor",
                      icon: Icon(MdiIcons.pencil),
                    ),
                    Tab(
                      text: "Camera",
                      icon: Icon(MdiIcons.camera),
                    ),
                    Tab(
                      text: "Gallery",
                      icon: Icon(MdiIcons.viewGallery),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class InputSourceWidget extends StatelessWidget {
  final String title;
  final String hint;
  final Function()? onSelection;
  const InputSourceWidget(
      {super.key,
      required this.title,
      required this.hint,
      required this.onSelection});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Center(
              child: DottedBorder(
                radius: const Radius.circular(12.0),
                borderType: BorderType.RRect,
                dashPattern: const [8, 4],
                color: Theme.of(context).highlightColor.withOpacity(0.4),
                child: const Center(child: Icon(MdiIcons.youtube, size: 80)),
              ),
            ),
          ),
          const SizedBox(height: 8),
          Text(
            hint,
            style: Theme.of(context).textTheme.bodyLarge,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 8),
          ElevatedButton(onPressed: onSelection, child: Text(title))
        ],
      ),
    );
  }
}
