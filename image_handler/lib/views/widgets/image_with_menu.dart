import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../providers/image_handler.dart';

class ImageWithMenu extends ConsumerWidget {
  final void Function()? onCrop;
  final void Function()? onDelete;
  const ImageWithMenu({Key? key, this.onCrop, this.onDelete}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final croppedImage =
        ref.watch(imageHandlerProvider.select((value) => value?.croppedImage));
    if (croppedImage == null) return Container();

    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16, right: 16),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Image.file(
                File((croppedImage)),
              ),
            ),
            if (onCrop != null || onDelete == null)
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if (onCrop != null) cropButton(context),
                    if (onCrop != null && onDelete != null) ...[
                      const Divider(),
                      const Divider(),
                    ],
                    if (onDelete != null) deleteButton(context),
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }

  CircleAvatar deleteButton(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Theme.of(context).colorScheme.onSurface,
      child: IconButton(
        icon: Icon(
          Icons.delete,
          color: Theme.of(context).colorScheme.surface,
        ),
        tooltip: 'Delete',
        onPressed: onDelete,
      ),
    );
  }

  CircleAvatar cropButton(BuildContext context) {
    return CircleAvatar(
      radius: 20,
      backgroundColor: Theme.of(context).colorScheme.onSurface,
      child: IconButton(
        onPressed: onCrop,
        tooltip: 'Crop',
        icon: Icon(Icons.crop, color: Theme.of(context).colorScheme.surface),
      ),
    );
  }
}
