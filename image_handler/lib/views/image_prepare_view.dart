import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:language_picker/languages.dart';
import '../models/image_handler.dart';
import '../providers/image_handler.dart';
import 'widgets/image_with_menu.dart';

class ImagePrepareView extends ConsumerStatefulWidget {
  final Function()? onValidation;
  final Function()? onDiscardImage;
  final String? defaultLanguageCode;
  final Future<void> Function(BuildContext context,
          {required void Function(String langISOCode) onSelection})
      onSelectLanguage;
  const ImagePrepareView(
      {super.key,
      this.onValidation,
      this.defaultLanguageCode,
      this.onDiscardImage,
      required this.onSelectLanguage});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      ImagePrepareViewState();
}

class ImagePrepareViewState extends ConsumerState<ImagePrepareView> {
  bool hideImage = false;
  @override
  Widget build(BuildContext context) {
    final imageHandler = ref.watch(imageHandlerProvider);

    final primaryLanguageCode =
        imageHandler?.lang1 ?? widget.defaultLanguageCode;
    return LayoutBuilder(builder: (context, constraint) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          if (!hideImage)
            SizedBox(
              height: constraint.maxHeight * 0.7,
              child: ImageWithMenu(
                onCrop: (imageHandler != null &&
                        imageHandler.state == ImageHandlerState.cropImage)
                    ? _cropImage
                    : null,
                onDelete: widget.onDiscardImage,
              ),
            ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: FittedBox(fit: BoxFit.cover, child: langSelector())),
            ),
          ),
          if (!hideImage && primaryLanguageCode != null)
            if (imageHandler != null &&
                imageHandler.state == ImageHandlerState.cropImage)
              Expanded(child: okButton(context))
            else
              const SizedBox(
                height: 10,
              )
        ],
      );
    });
  }

  CircleAvatar okButton(BuildContext context) {
    return CircleAvatar(
      radius: 20,
      backgroundColor: Theme.of(context).colorScheme.onSurface,
      child: IconButton(
        onPressed: widget.onValidation,
        tooltip: 'Extract Text',
        icon: Icon(Icons.check, color: Theme.of(context).colorScheme.surface),
      ),
    );
  }

  Widget langSelector() {
    final handle = ref.watch(imageHandlerProvider);
    final primaryLanguageCode = handle?.lang1 ?? widget.defaultLanguageCode;
    final secondaryLanguageCode = handle?.lang2;

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Align(
            alignment: Alignment.topLeft,
            child: _CustomText("Extract Text of the following languages")),
        const Align(
            alignment: Alignment.centerLeft,
            child: _CustomText("(Tap on the language to change it)")),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const _CustomText("Primary: "),
            TextButton(
              onPressed: () => changeLang(onChange: (lang) {
                ref.read(imageHandlerProvider.notifier).lang1 = lang;
              }),
              child: (primaryLanguageCode != null)
                  ? _CustTextButton(
                      Language.fromIsoCode(primaryLanguageCode).name)
                  : const _CustError("Select Language to continue"),
            ),
            if (handle?.lang1 != null)
              Padding(
                padding: const EdgeInsets.only(left: 4),
                child: CircleAvatar(
                  child: IconButton(
                    onPressed: () {
                      ref.read(imageHandlerProvider.notifier).removeLang1();
                    },
                    icon: const Icon(Icons.restore),
                    //label: const Text("Remove"),
                  ),
                ),
              )
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const _CustomText("Optional Secondary: "),
            TextButton(
              onPressed: () => changeLang(onChange: (lang) {
                ref.read(imageHandlerProvider.notifier).lang2 = lang;
              }),
              child: _CustTextButton((secondaryLanguageCode != null)
                  ? Language.fromIsoCode(secondaryLanguageCode).name
                  : "None"),
            ),
            if (secondaryLanguageCode != null)
              Padding(
                padding: const EdgeInsets.only(left: 4),
                child: CircleAvatar(
                  child: IconButton(
                    onPressed: () {
                      ref.read(imageHandlerProvider.notifier).removeLang2();
                    },
                    icon: const Icon(Icons.close),
                    //label: const Text("Remove"),
                  ),
                ),
              )
          ],
        ),
      ],
    );
  }

  void changeLang({required void Function(String lang) onChange}) async {
    setState(() {
      hideImage = true;
    });
    await widget.onSelectLanguage(context, onSelection: onChange);
    await Future.delayed(const Duration(microseconds: 100));
    setState(() {
      hideImage = false;
    });
  }

  Future<void> _cropImage() async {
    final handle = ref.watch(imageHandlerProvider);
    if (handle?.image != null) {
      final croppedFile = await ImageCropper().cropImage(
        sourcePath: (handle?.image)!,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 100,
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: 'Cropper',
              toolbarColor: Colors.deepOrange,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          IOSUiSettings(
            title: 'Cropper',
          ),
          WebUiSettings(
            context: context,
            presentStyle: CropperPresentStyle.dialog,
            boundary: const CroppieBoundary(
              width: 520,
              height: 520,
            ),
            viewPort:
                const CroppieViewPort(width: 480, height: 480, type: 'circle'),
            enableExif: true,
            enableZoom: true,
            showZoomer: true,
          ),
        ],
      );
      if (croppedFile != null) {
        ref.read(imageHandlerProvider.notifier).croppedImagePath =
            croppedFile.path;
      }
    }
  }
}

class _CustomText extends ConsumerWidget {
  final String text;
  const _CustomText(this.text);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Text(
      text,
      style: Theme.of(context).textTheme.displaySmall,
    );
  }
}

class _CustTextButton extends ConsumerWidget {
  final String text;
  const _CustTextButton(this.text);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Text(
      text,
      style: Theme.of(context)
          .textTheme
          .displaySmall!
          .copyWith(color: Colors.blue.shade400),
    );
  }
}

class _CustError extends ConsumerWidget {
  final String text;
  const _CustError(this.text);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Text(
      text,
      style: Theme.of(context).textTheme.displaySmall!.copyWith(
          color: Colors.blue.shade400,
          decoration: TextDecoration.underline,
          decorationStyle: TextDecorationStyle.double,
          decorationColor: Colors.red.shade400),
    );
  }
}
/*

if (!hideImage)
            Expanded(
              child: Row(
                children: [
                  Expanded(
                    flex: 4,
                    child: Image.file(File((handle.croppedImagePath ??
                        handle.externalImagePath ??
                        handle.orgImagePath)!)),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Spacer(),
                        CircleAvatar(
                          backgroundColor: Colors.black12,
                          child: IconButton(
                            onPressed: () {
                              // _cropImage();
                            },
                            tooltip: 'Crop',
                            icon: Icon(
                              Icons.crop,
                              color: Colors.amber.shade900,
                            ),
                          ),
                        ),
                        const Spacer(),
                        CircleAvatar(
                          backgroundColor: Colors.black12,
                          child: IconButton(
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red.shade300,
                            ),
                            tooltip: 'Delete',
                            onPressed: () {
                              //_clear();
                              if (handle.externalImagePath != null) {
                                ref
                                    .read(imageBucketProvider.notifier)
                                    .removeImage();
                              } else {
                                // TODO: clear internal image
                              }
                            },
                          ),
                        ),
                        const Spacer()

                        // if (_croppedFile == null)
                      ],
                    ),
                  )
                ],
              ),
            ),
const SizedBox(height: 24.0),
          Flexible(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: kIsWeb ? 24.0 : 16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Align(
                      alignment: Alignment.topLeft,
                      child: _CustomText(
                          "Extract Text of the following languages")),
                  const Align(
                      alignment: Alignment.centerLeft,
                      child: _CustomText("(Tap on the language to change it)")),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _CustomText("Language1: "),
                      TextButton(
                        onPressed: () => changeLang(
                            onChange: (Language lang) => setState(() {
                                  lang1 = lang.name;
                                })),
                        child: _CustTextButton(
                            (lang1 != null) ? lang1! : "Select language"),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const _CustomText("Language2: "),
                      TextButton(
                        onPressed: () => changeLang(
                            onChange: (Language lang) => setState(() {
                                  lang2 = lang.name;
                                })),
                        child: _CustTextButton(
                            (lang2 != null) ? lang2! : "Select language"),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 24.0),
          _menu(),
          */