// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/image_handler.dart';

import '../providers/image_handler.dart';
import 'image_picker_view.dart';
import 'image_prepare_view.dart';

class ImageHandlerView extends StatelessWidget {
  final Future<bool> Function(
      {required String imagePath,
      required String lang1,
      required String? lang2}) onImageReady;
  final int? processingTimeoutImageReady;
  final String? defaultLanguageCode;
  final void Function(BuildContext context) onDiscardImage;
  final String? externalImagePath;
  final Function()? onOpenTextEditor;
  final Future<void> Function(BuildContext context,
          {required void Function(String langISOCode) onSelection})
      onSelectLanguage;
  const ImageHandlerView(
      {super.key,
      required this.externalImagePath,
      required this.onImageReady,
      this.processingTimeoutImageReady,
      required this.onDiscardImage,
      this.defaultLanguageCode,
      required this.onSelectLanguage,
      required this.onOpenTextEditor});

  @override
  Widget build(BuildContext context) {
    return ProviderScope(
      overrides: [
        // ignore: deprecated_member_use
        imageHandlerProvider.overrideWithProvider(
            StateNotifierProvider<ImageHandlerNotifier, ImageHandler?>((ref) {
          return (ImageHandlerNotifier(externalImagePath: externalImagePath));
        }))
      ],
      child: _ImageHandlerView(
        onDiscardImage: onDiscardImage,
        onImageReady: onImageReady,
        processingTimeoutImageReady: processingTimeoutImageReady,
        defaultLanguageCode: defaultLanguageCode,
        onSelectLanguage: onSelectLanguage,
        onOpenTextEditor: onOpenTextEditor,
      ),
    );
  }
}

class _ImageHandlerView extends ConsumerStatefulWidget {
  final Future<bool> Function(
      {required String imagePath,
      required String lang1,
      required String? lang2}) onImageReady;
  final int? processingTimeoutImageReady;
  final String? defaultLanguageCode;
  final Future<void> Function(BuildContext context,
          {required void Function(String langISOCode) onSelection})
      onSelectLanguage;
  final void Function(BuildContext context) onDiscardImage;
  final Function()? onOpenTextEditor;

  const _ImageHandlerView({
    required this.onImageReady,
    this.processingTimeoutImageReady,
    required this.onDiscardImage,
    this.defaultLanguageCode,
    required this.onSelectLanguage,
    required this.onOpenTextEditor,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ImageHandlerViewState();
}

class _ImageHandlerViewState extends ConsumerState<_ImageHandlerView> {
  @override
  Widget build(BuildContext context) {
    final Map<ImageHandlerState,
            Widget Function(BuildContext context, BoxConstraints constraints)>
        widgetBuilder = {
      ImageHandlerState.pickImage: pickImageWidgetBuilder,
      ImageHandlerState.cropImage: cropImageWidgetBuilder,
      ImageHandlerState.processImage: processImageWidgetBuilder,
    };
    final handle = ref.watch(imageHandlerProvider);

    return LayoutBuilder(
        builder: (handle == null)
            ? waitImageWidgetBuilder
            : widgetBuilder[handle.state]!);
  }

  Widget waitImageWidgetBuilder(
      BuildContext context, BoxConstraints constraints) {
    return const Center(child: Text("Waiting for image/language"));
  }

  Widget pickImageWidgetBuilder(
      BuildContext context, BoxConstraints constraints) {
    return ImagePickerView(
        onDiscardImage: onDiscardImage,
        onOpenTextEditor: widget.onOpenTextEditor);
  }

  Widget cropImageWidgetBuilder(
      BuildContext context, BoxConstraints constraints) {
    return ImagePrepareView(
      onValidation: imageProcess,
      onDiscardImage: onDiscardImage,
      defaultLanguageCode: widget.defaultLanguageCode,
      onSelectLanguage: widget.onSelectLanguage,
    );
  }

  Future<Uint8List> imagePrepare(String path) async {
    return File(path).readAsBytesSync();
  }

  imageProcess() async {
    final handle = ref.read(imageHandlerProvider);
    final croppedImage =
        ref.watch(imageHandlerProvider.select((value) => value?.croppedImage));
    if (croppedImage == null) return;
    ref.read(imageHandlerProvider.notifier).isImageReady = true;
    final success = await widget.onImageReady(
        imagePath: croppedImage,
        lang1: (handle?.lang1 ?? widget.defaultLanguageCode)!,
        lang2: handle?.lang2);
    if (!success) {
      ref.read(imageHandlerProvider.notifier).isImageReady = false;
    }
  }

  Widget processImageWidgetBuilder(
      BuildContext context, BoxConstraints constraints) {
    return ConstrainedBox(
        constraints: const BoxConstraints(),
        child: Stack(
          children: [
            ImagePrepareView(
              defaultLanguageCode: widget.defaultLanguageCode,
              onSelectLanguage: widget.onSelectLanguage,
            ),
            Container(
                width: constraints.maxWidth,
                height: constraints.maxHeight,
                color: Colors.black54.withAlpha(128),
                child: Center(
                  child: Container(
                    padding: const EdgeInsets.all(24),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Theme.of(context).colorScheme.surface),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const CircularProgressIndicator(),
                        const SizedBox(
                          height: 8,
                        ),
                        const Text("Text recognization in progress..."),
                        if (widget.processingTimeoutImageReady != null)
                          Text(
                              "This may take upto ${widget.processingTimeoutImageReady} seconds")
                      ],
                    ),
                  ),
                ))
          ],
        ));
  }

  Future<void> onDiscardImage() async {
    ref.read(imageHandlerProvider.notifier).removeImage();

    widget.onDiscardImage(context);
  }
}
