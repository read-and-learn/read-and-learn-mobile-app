enum ImageHandlerState { pickImage, cropImage, processImage }

class ImageHandler {
  final String? externalImagePath; // Readonly once created
  final String? imagePath;
  final String? _croppedImagePath;

  final bool isImageReady;

  final String? lang1;
  final String? lang2;

  ImageHandler({
    required this.externalImagePath,
    this.lang1,
    this.lang2,
  })  : _croppedImagePath = null,
        imagePath = null,
        isImageReady = false;

  ImageHandler._({
    this.externalImagePath,
    this.imagePath,
    String? croppedImagePath,
    this.isImageReady = false,
    this.lang1,
    this.lang2,
  }) : _croppedImagePath = croppedImagePath {
    if (croppedImagePath != null) {}
  }

  ImageHandler copyWith({
    String? imagePath,
    String? croppedImagePath,
    bool? isImageReady,
    String? lang1,
    String? lang2,
  }) {
    return ImageHandler._(
      externalImagePath: externalImagePath,
      imagePath: imagePath ?? this.imagePath,
      croppedImagePath: croppedImagePath ?? _croppedImagePath,
      isImageReady: isImageReady ?? this.isImageReady,
      lang1: lang1 ?? this.lang1,
      lang2: lang2 ?? this.lang2,
    );
  }

  ImageHandlerState get state {
    if (externalImagePath == null && imagePath == null) {
      return ImageHandlerState.pickImage;
    }
    if (!isImageReady) {
      return ImageHandlerState.cropImage;
    }
    return ImageHandlerState.processImage;
  }

  String? get image => (externalImagePath ?? imagePath);

  String? get croppedImage =>
      (_croppedImagePath ?? externalImagePath ?? imagePath);

  ImageHandler removeImage() => ImageHandler._(
        externalImagePath: externalImagePath,
        imagePath: null,
        croppedImagePath: null,
        isImageReady: false,
        lang1: lang1,
        lang2: lang2,
      );

  ImageHandler removeLang1() => ImageHandler._(
        externalImagePath: externalImagePath,
        imagePath: imagePath,
        croppedImagePath: _croppedImagePath,
        isImageReady: false,
        lang1: null,
        lang2: lang2,
      );

  ImageHandler removeLang2() => ImageHandler._(
        externalImagePath: externalImagePath,
        imagePath: imagePath,
        croppedImagePath: _croppedImagePath,
        isImageReady: false,
        lang1: lang1,
        lang2: null,
      );
}
