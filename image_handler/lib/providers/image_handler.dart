import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/image_handler.dart';

class ImageHandlerNotifier extends StateNotifier<ImageHandler?> {
  final String? externalImagePath;
  final String? _lang1;
  final String? _lang2;

  ImageHandlerNotifier({
    this.externalImagePath,
    String? lang1,
    String? lang2,
  })  : _lang1 = lang1,
        _lang2 = lang2,
        super(null) {
    create();
  }

  Future<void> create() async {
    state = ImageHandler(
        externalImagePath: externalImagePath, lang1: _lang1, lang2: _lang2);
  }

  _setcroppedImage(String val) async {
    state = state?.copyWith(croppedImagePath: val);
  }

  _setImage(String val) async {
    state = state?.copyWith(imagePath: val);
  }

  set croppedImagePath(String val) => _setcroppedImage(val);
  set imagePath(String val) => _setImage(val);

  void removeImage() => state = state?.removeImage();

  set lang1(String val) => state = state?.copyWith(lang1: val);
  set lang2(String val) => state = state?.copyWith(lang2: val);
  void removeLang2() => state = state?.removeLang2();
  void removeLang1() => state = state?.removeLang1();

  set isImageReady(bool val) => state = state?.copyWith(isImageReady: val);
}

final imageHandlerProvider =
    StateNotifierProvider<ImageHandlerNotifier, ImageHandler?>((ref) {
  return (ImageHandlerNotifier());
});
