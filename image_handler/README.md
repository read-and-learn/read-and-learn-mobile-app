This module is a replica of the example application provider 
as part of image_cropper with few custom changes
1. State is managed by riverpod. instead of StatefulWidget
2. Provide external image when calling to skip the picker
3. few additional menu to return the control to parent widget
   1. onImageReady (to handover the captured and/or cropped image to parent)
   2. onDiscardImage (to abort the process)
4. Language selection for the given image (upto two languages)
