import 'dart:convert';

import 'package:collection/collection.dart';

import 'entry.dart';

enum EditorType {
  singleLineTextField,
  selectSingleChip,
  chipMenuSelectMulti;
}

Map<EditorType, String> editorTypeToString = {
  EditorType.singleLineTextField: "singleLineTextField",
  EditorType.selectSingleChip: "selectSingleChip",
  EditorType.chipMenuSelectMulti: "multiValueSelectable"
};
Map<String, EditorType> editorTypeFromString = {
  "singleLineTextField": EditorType.singleLineTextField,
  "selectSingleChip": EditorType.selectSingleChip,
  "multiValueSelectable": EditorType.chipMenuSelectMulti,
};

class Field {
  final Entry _name;
  final EditorType editorType;
  final List<String> validFor;
  final List<Entry> candidates;
  Field({
    required Entry name,
    required this.editorType,
    required this.validFor,
    required this.candidates,
  }) : _name = name;

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': _name.toMap(),
      'editorType': editorTypeToString[editorType],
      'validFor': validFor,
      'candidates': candidates.map((x) => x.toMap()).toList(),
    };
  }

  factory Field.fromMap(Map<String, dynamic> map) {
    return Field(
      name: Entry.fromMap(map['name'] as Map<String, dynamic>),
      editorType: editorTypeFromString[map['editorType'] as String]!,
      validFor: List<String>.from((map['validFor'] as List<dynamic>)),
      candidates: List<Entry>.from(
        (map['candidates'] as List<dynamic>).map<Entry>(
          (x) => Entry.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory Field.fromJson(String source) =>
      Field.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Field(name: $_name, editorType: $editorType, validFor: $validFor, candidates: $candidates)';
  }

  @override
  bool operator ==(covariant Field other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other._name == _name &&
        other.editorType == editorType &&
        listEquals(other.validFor, validFor) &&
        listEquals(other.candidates, candidates);
  }

  @override
  int get hashCode {
    return _name.hashCode ^
        editorType.hashCode ^
        validFor.hashCode ^
        candidates.hashCode;
  }

  String get name => _name.entry.replaceAll('@', '');
  List<String> get selectableItems =>
      candidates.map((e) => e.entry.replaceAll('@', '')).toList();
}
