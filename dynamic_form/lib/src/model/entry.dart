import 'dart:convert';

class Entry {
  final String entry;
  final String helpText;
  Entry({
    required this.entry,
    required this.helpText,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'entry': entry,
      'helpText': helpText,
    };
  }

  factory Entry.fromMap(Map<String, dynamic> map) {
    return Entry(
      entry: map['entry'] as String,
      helpText: map['helpText'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory Entry.fromJson(String source) =>
      Entry.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'FieldCandidate(entry: $entry, helpText: $helpText)';

  @override
  bool operator ==(covariant Entry other) {
    if (identical(this, other)) return true;

    return other.entry == entry && other.helpText == helpText;
  }

  @override
  int get hashCode => entry.hashCode ^ helpText.hashCode;
}
