import 'dart:convert';

import 'package:collection/collection.dart';

import 'field.dart';

class Form {
  final List<Field> fields;
  Form({
    required this.fields,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'fields': fields.map((x) => x.toMap()).toList(),
    };
  }

  factory Form.fromMap(Map<String, dynamic> map) {
    return Form(
      fields: List<Field>.from(
        (map['fields'] as List<dynamic>).map<Field>(
          (x) => Field.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory Form.fromJson(String source) =>
      Form.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Form(fields: $fields)';

  @override
  bool operator ==(covariant Form other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return listEquals(other.fields, fields);
  }

  @override
  int get hashCode => fields.hashCode;

  Field getFieldByName(String name) {
    return fields
        .where(
          (element) => element.name == name,
        )
        .first;
  }
}
