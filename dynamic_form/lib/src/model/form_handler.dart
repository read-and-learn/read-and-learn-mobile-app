abstract class FormHandler {
  //dynamic object;

  String? getType();
  List<String> getCurrentValues(String key);
  Future<dynamic> saveUpdates(Map<String, List<String>> updates);
}

class FormInstanceIdentifier {
  final FormHandler formHandler;
  final String jsonFile;
  FormInstanceIdentifier({
    required this.formHandler,
    required this.jsonFile,
  });

  FormInstanceIdentifier copyWith({
    FormHandler? formHandler,
    String? jsonFile,
  }) {
    return FormInstanceIdentifier(
      formHandler: formHandler ?? this.formHandler,
      jsonFile: jsonFile ?? this.jsonFile,
    );
  }

  @override
  String toString() =>
      'FormInstanceIdentifier(formHandler: $formHandler, jsonFile: $jsonFile)';

  @override
  bool operator ==(covariant FormInstanceIdentifier other) {
    if (identical(this, other)) return true;

    return other.formHandler == formHandler && other.jsonFile == jsonFile;
  }

  @override
  int get hashCode => formHandler.hashCode ^ jsonFile.hashCode;
}
