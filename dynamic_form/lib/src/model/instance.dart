import 'package:collection/collection.dart';

import 'field.dart';
import 'form.dart';
import 'form_handler.dart';

class FormInstance {
  FormInstanceIdentifier formKey;
  final Form form;
  final Map<String, List<String>> state;
  final bool isPreview;

  FormInstance(
      {required this.formKey, required this.form, this.isPreview = true})
      : state = Map.fromEntries(form.fields.map((e) =>
            MapEntry(e.name, formKey.formHandler.getCurrentValues(e.name))));
  FormInstance._(
      {required this.formKey,
      required this.form,
      required this.state,
      required this.isPreview});

  FormInstance edit(bool enableEditor) {
    return FormInstance._(
        formKey: formKey, form: form, state: state, isPreview: !enableEditor);
  }

  FormInstance resetField(String field) {
    if (state.containsKey(field)) {
      final Map<String, List<String>> currState = Map.from(state);
      Field formField = form.fields.where((Field e) => e.name == field).first;
      currState[field] = formKey.formHandler.getCurrentValues(formField.name);
      return FormInstance._(
          formKey: formKey, form: form, state: currState, isPreview: isPreview);
    }
    return this;
  }

  FormInstance setCandidate(String field, String candidate, bool selected) {
    if (state.containsKey(field)) {
      final Map<String, List<String>> currState = Map.from(state);
      List<String> selectedValues;
      if (selected) {
        Field formField = form.fields.where((Field e) => e.name == field).first;

        selectedValues = [
          ...{
            if (formField.editorType == EditorType.chipMenuSelectMulti)
              ...currState[field]!,
            candidate
          }
        ];
      } else {
        selectedValues =
            currState[field]!.where((e) => e != candidate).toList();
      }
      currState[field] = selectedValues;
      return FormInstance._(
          formKey: formKey, form: form, state: currState, isPreview: isPreview);
    }
    return this;
  }

  FormInstance setValue(String field, String value) {
    if (state.containsKey(field)) {
      final Map<String, List<String>> currState = Map.from(state);
      currState[field] = [value];
      return FormInstance._(
          formKey: formKey, form: form, state: currState, isPreview: isPreview);
    }
    return this;
  }

  @override
  String toString() =>
      'FormInstance(formKey: $formKey, form: $form, state: $state)';

  @override
  bool operator ==(covariant FormInstance other) {
    if (identical(this, other)) return true;
    final mapEquals = const DeepCollectionEquality().equals;

    return other.formKey == formKey &&
        other.form == form &&
        mapEquals(other.state, state);
  }

  @override
  int get hashCode => formKey.hashCode ^ form.hashCode ^ state.hashCode;

  List<String> getCurrentValues(String fieldName) {
    return formKey.formHandler.getCurrentValues(fieldName);
  }

  bool isChangedAny() {
    for (var field in form.fields) {
      List<String> newValues = state[field.name]!;
      List<String> currValues = getCurrentValues(field.name);
      bool isChanged =
          !const DeepCollectionEquality().equals(currValues, newValues);
      if (isChanged) return true;
    }
    return false;
  }

  Map<String, List<String>> getUpdates() {
    Map<String, List<String>> updates = Map.from(state);
    for (var field in form.fields) {
      List<String> newValues = state[field.name]!;
      List<String> currValues = getCurrentValues(field.name);
      bool isChanged =
          !const DeepCollectionEquality().equals(currValues, newValues);
      if (!isChanged) {
        updates.remove(field.name);
      }
    }
    return updates;
  }
}
