import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../model/form.dart';

final formProvider =
    FutureProvider.family.autoDispose<Form, String>((ref, jsonFile) async {
  final String json = await rootBundle.loadString(jsonFile);
  return Form.fromJson(json);
});
