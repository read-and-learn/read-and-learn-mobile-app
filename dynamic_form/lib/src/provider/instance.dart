import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../model/form.dart';
import '../model/form_handler.dart';
import '../model/instance.dart';
import 'form.dart';

class FormInstanceNotifier extends StateNotifier<FormInstance> {
  FormInstanceIdentifier formKey;
  AsyncValue<Form> form;
  FormInstanceNotifier({required this.formKey, required this.form})
      : super(FormInstance(formKey: formKey, form: Form(fields: []))) {
    form.whenData((Form form) async => create(form));
  }
  create(Form form) async {
    state = FormInstance(formKey: formKey, form: form);
    const AsyncValue.loading();
  }

  void setCandidate(String field, String candidate, bool selected) {
    state = state.setCandidate(field, candidate, selected);
  }

  void setValue(String field, String value) {
    state = state.setValue(field, value);
  }

  void resetField(String field) {
    state = state.resetField(field);
  }

  void edit(bool enableEditor) {
    state = state.edit(enableEditor);
  }
}

final formInstanceProvider = StateNotifierProvider.family
    .autoDispose<FormInstanceNotifier, FormInstance, FormInstanceIdentifier>(
        (ref, formInstanceIdentifier) {
  final form = ref.watch(formProvider(formInstanceIdentifier.jsonFile));

  return FormInstanceNotifier(formKey: formInstanceIdentifier, form: form);
});
