extension StringExtension on String {
  String capitalize() {
    return trim()
        .split(' ')
        .map((e) => "${e[0].toUpperCase()}${e.substring(1).toLowerCase()}")
        .toList()
        .join(' ')
        .split('/')
        .map((e) => "${e[0].toUpperCase()}${e.substring(1)}")
        .toList()
        .join('/');
  }
}
