import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../model/form_handler.dart';
import '../provider/instance.dart';

class FormSave extends ConsumerWidget {
  final FormInstanceIdentifier formInstanceIdentifier;
  final String label;
  final IconData icon;
  final Function({required Map<String, List<String>> updates})? onPressed;
  final TextStyle textStyle;
  const FormSave({
    Key? key,
    required this.formInstanceIdentifier,
    required this.label,
    required this.icon,
    required this.onPressed,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formInstance =
        ref.watch(formInstanceProvider(formInstanceIdentifier));
    if (formInstance.isChangedAny()) {
      return BlinkButtonWarn(
        label: label,
        icon: icon,
        textStyle: textStyle,
        onPressed: onPressed == null
            ? null
            : () async {
                return await onPressed!(updates: formInstance.getUpdates());
              },
      );
    }
    return Container();
  }
}
