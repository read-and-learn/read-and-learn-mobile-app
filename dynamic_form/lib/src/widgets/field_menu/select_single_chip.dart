import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../model/field.dart';
import '../../model/form_handler.dart';
import '../../provider/instance.dart';
import '../../utils.dart';

import 'show_change.dart';

class SelectSingleChip extends ConsumerWidget {
  final FormInstanceIdentifier formInstanceIdentifier;
  final String fieldName;
  final bool isPreview;

  const SelectSingleChip({
    Key? key,
    required this.formInstanceIdentifier,
    required this.fieldName,
    required this.isPreview,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final formInstance =
        ref.watch(formInstanceProvider(formInstanceIdentifier));

    Field field = formInstance.form.getFieldByName(fieldName);
    List<String> newValues = formInstance.state[fieldName]!;
    List<String> currentValues = formInstance.getCurrentValues(field.name);

    return ListTile(
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "${field.name.capitalize()}:",
              )),
          ShowChange(
            isPreview: isPreview,
            title: field.name.capitalize(),
            currValues: currentValues,
            newValues: newValues,
            hideIfNotChanged: false,
            onReset: () {
              ref
                  .read(formInstanceProvider(formInstanceIdentifier).notifier)
                  .resetField(field.name);
            },
          ),
        ],
      ),
      subtitle: isPreview
          ? null
          : Container(
              margin: const EdgeInsets.symmetric(vertical: 8.0),
              //decoration: BoxDecoration(border: Border.all()),
              padding: const EdgeInsets.all(4),
              child: Wrap(
                alignment: WrapAlignment.start,
                runAlignment: WrapAlignment.spaceAround,
                spacing: 1.0,
                runSpacing: 0.5,
                children: [
                  for (String candidate in field.selectableItems)
                    CustomFilterChip(
                        label: candidate.capitalize(),
                        isSelected: newValues.contains(candidate),
                        onSelected: (bool selected) {
                          FocusManager.instance.primaryFocus?.unfocus();
                          return ref
                              .read(formInstanceProvider(formInstanceIdentifier)
                                  .notifier)
                              .setCandidate(field.name, candidate, selected);
                        }),
                ],
              ),
            ),
    );
  }
}

//title:
//currValue:
// newValue

