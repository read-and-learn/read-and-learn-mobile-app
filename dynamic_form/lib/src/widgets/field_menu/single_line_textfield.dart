import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../model/field.dart';
import '../../model/form_handler.dart';
import '../../provider/instance.dart';
import '../../utils.dart';

import 'show_change.dart';

class SingleLineTextField extends ConsumerStatefulWidget {
  final FormInstanceIdentifier formInstanceIdentifier;
  final String fieldName;
  final bool isPreview;
  const SingleLineTextField(
      {Key? key,
      required this.formInstanceIdentifier,
      required this.fieldName,
      required this.isPreview})
      : super(key: key);

  @override
  ConsumerState<SingleLineTextField> createState() => _TextFieldMenuState();
}

class _TextFieldMenuState extends ConsumerState<SingleLineTextField> {
  late TextEditingController textEditingController;
  String initialValue = "";
  @override
  void initState() {
    textEditingController = TextEditingController();
    final formInstance =
        ref.read(formInstanceProvider(widget.formInstanceIdentifier));
    Field field = formInstance.form.getFieldByName(widget.fieldName);

    List<String> selectedItems = formInstance.state[field.name]!;
    textEditingController.text =
        selectedItems.isNotEmpty ? selectedItems[0] : "";

    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final formInstance =
        ref.watch(formInstanceProvider(widget.formInstanceIdentifier));

    Field field = formInstance.form.getFieldByName(widget.fieldName);
    List<String> newValues = formInstance.state[field.name]!;

    List<String> currentValues = formInstance.getCurrentValues(field.name);

    return ListTile(
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "${field.name.capitalize()}:",
              )),
          ShowChange(
            isPreview: widget.isPreview,
            title: field.name.capitalize(),
            currValues: currentValues,
            newValues: newValues,
            hideIfNotChanged: true,
            onReset: () {
              ref
                  .read(formInstanceProvider(widget.formInstanceIdentifier)
                      .notifier)
                  .resetField(field.name);
              resetController();
            },
          ),
        ],
      ),
      subtitle: widget.isPreview
          ? null
          : CustomTextField(
              labelText: field.name.capitalize(),
              controller: textEditingController,
              onChanged: (String val) => onChanged(field, val),
            ),
    );
  }

  onChanged(Field field, String val) {
    ref
        .read(formInstanceProvider(widget.formInstanceIdentifier).notifier)
        .setValue(field.name, val);
  }

  resetController() {
    final formInstance =
        ref.read(formInstanceProvider(widget.formInstanceIdentifier));
    Field field = formInstance.form.getFieldByName(widget.fieldName);
    List<String> selectedItems = formInstance.state[field.name]!;
    textEditingController.text =
        selectedItems.isNotEmpty ? selectedItems[0] : "";
  }
}
