import 'package:collection/collection.dart';
import 'package:dynamic_form/src/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class ShowChange extends ConsumerWidget {
  final String title;
  final List<String> currValues;
  final List<String> newValues;
  final Function() onReset;
  final bool hideIfNotChanged;
  final bool isPreview;
  const ShowChange(
      {Key? key,
      required this.title,
      required this.currValues,
      required this.newValues,
      required this.onReset,
      required this.hideIfNotChanged,
      required this.isPreview})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    bool isChanged =
        !const DeepCollectionEquality().equals(currValues, newValues);

    if (!isPreview && !isChanged) return Container();

    return Padding(
      padding:
          EdgeInsets.only(left: 8, right: isPreview && !isChanged ? 16 : 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text.rich(
              TextSpan(children: [
                if (isChanged)
                  TextSpan(children: [
                    if (currValues.isNotEmpty)
                      TextSpan(children: [
                        TextSpan(
                            text: currValues[0].capitalize(),
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    decoration: TextDecoration.lineThrough)),
                        if (newValues.isNotEmpty) const TextSpan(text: " → "),
                        if (newValues.isNotEmpty)
                          TextSpan(
                            text: newValues[0].capitalize(),
                          )
                      ])
                    else
                      TextSpan(
                        text: newValues.isNotEmpty ? newValues[0] : "",
                      )
                  ])
                else if (isPreview)
                  TextSpan(children: [
                    if (currValues.isNotEmpty)
                      TextSpan(
                        text: currValues[0].capitalize(),
                      )
                    else
                      const TextSpan(
                        text: "Not Set",
                      )
                  ])
              ]),
              style: Theme.of(context).textTheme.bodyMedium!,
              textAlign:
                  isPreview && !isChanged ? TextAlign.right : TextAlign.left,
            ),
          ),
          if (isChanged)
            GestureDetector(
                onTap: onReset,
                child: const Icon(
                  MdiIcons.close,
                ))
        ],
      ),
    );
  }
}
