import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../dynamic_form.dart';
import '../provider/instance.dart';

class FormEdit extends ConsumerWidget {
  final FormInstanceIdentifier formInstanceIdentifier;
  final String labelEdit;
  final String labelPreview;
  final IconData iconEdit;
  final IconData iconPreview;

  final TextStyle textStyle;

  const FormEdit({
    Key? key,
    required this.formInstanceIdentifier,
    this.labelEdit = "Edit",
    this.labelPreview = "Done",
    this.iconEdit = MdiIcons.pencil,
    this.iconPreview = Icons.check,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isPreview = ref.watch(formInstanceProvider(formInstanceIdentifier)
        .select((value) => value.isPreview));
    return TextButton.icon(
        onPressed: () {
          ref
              .read(formInstanceProvider(formInstanceIdentifier).notifier)
              .edit(isPreview);
        },
        icon: Icon(
          !isPreview ? iconPreview : iconEdit,
        ),
        label: Text(!isPreview ? labelPreview : labelEdit, style: textStyle));
  }
}
