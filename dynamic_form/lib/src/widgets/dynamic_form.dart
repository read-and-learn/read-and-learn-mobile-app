import 'package:dynamic_form/src/model/field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../model/form_handler.dart';
import '../provider/instance.dart';
import 'field_menu/select_single_chip.dart';
import 'field_menu/single_line_textfield.dart';

class DynamicForm extends ConsumerStatefulWidget {
  final FormInstanceIdentifier formInstanceIdentifier;
  final Widget Function(Object?, StackTrace?)? onError;
  final Widget Function()? onLoading;

  const DynamicForm({
    Key? key,
    required this.formInstanceIdentifier,
    this.onError,
    this.onLoading,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _DynamicFormState();
}

class _DynamicFormState extends ConsumerState<DynamicForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final formInstance =
        ref.watch(formInstanceProvider(widget.formInstanceIdentifier));
    final bool isPreview = formInstance.isPreview;
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Form(
        key: _formKey,
        child: Center(
            child: ListView.separated(
                separatorBuilder: (context, index) {
                  return FieldDivider(
                    isPreview: isPreview,
                    field: formInstance.form.fields[index],
                    type: widget.formInstanceIdentifier.formHandler.getType(),
                  );
                },
                itemCount: formInstance.form.fields.length,
                itemBuilder: (context, index) {
                  var field = formInstance.form.fields[index];
                  if (field.validFor.contains(
                      widget.formInstanceIdentifier.formHandler.getType())) {
                    if (EditorType.selectSingleChip == field.editorType) {
                      return SelectSingleChip(
                          formInstanceIdentifier: widget.formInstanceIdentifier,
                          fieldName: field.name,
                          isPreview: isPreview);
                    } else {
                      if (EditorType.singleLineTextField == field.editorType) {
                        return SingleLineTextField(
                            formInstanceIdentifier:
                                widget.formInstanceIdentifier,
                            fieldName: field.name,
                            isPreview: isPreview);
                      }
                    }
                  }
                  return Container();
                })),
      ),
    );
  }
}

class FieldDivider extends ConsumerWidget {
  final bool isPreview;
  final Field field;
  final String? type;
  const FieldDivider({
    Key? key,
    required this.isPreview,
    required this.field,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (isPreview) {
      if (field.validFor.contains(type)) {
        return Row(
          children: [
            //Text(index.toString()),
            Expanded(
              child: Divider(
                color: Theme.of(context).disabledColor,
              ),
            ),
          ],
        );
      }
    }
    return Container();
  }
}
