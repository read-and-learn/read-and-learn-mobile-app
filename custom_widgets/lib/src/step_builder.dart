import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

enum _State {
  completed,
  active,
  waiting,
  invisible,
  error;

  // ("completed") ("active") ("waiting") ("invisible")("inerror")

  factory _State.fromIndex(myStep, currStep) {
    if (myStep < currStep) {
      return completed;
    }
    if (myStep == currStep) {
      return active;
    }
    return waiting;
  }
}

class StepContent {
  final String title;
  final Widget? subtitle;
  final Widget? content;
  final String? prevButtonLabel;
  final String? nextButtonLabel;

  StepContent({
    required this.title,
    this.subtitle,
    this.content,
    this.prevButtonLabel,
    this.nextButtonLabel,
  });

  StepContent copyWith({
    String? title,
    Widget? subtitle,
    Widget? content,
    String? prevButtonLabel,
    String? nextButtonLabel,
  }) {
    return StepContent(
      title: title ?? this.title,
      subtitle: subtitle ?? this.subtitle,
      content: content ?? this.content,
      prevButtonLabel: prevButtonLabel ?? this.prevButtonLabel,
      nextButtonLabel: nextButtonLabel ?? this.nextButtonLabel,
    );
  }
}

class StepBuilder extends StatefulWidget {
  final List<StepContent> steps;
  final int currStep;
  final bool useDefaultButtons;
  final Function(int currStep)? onActiveStepChange;
  const StepBuilder({
    Key? key,
    required this.steps,
    required this.currStep,
    this.useDefaultButtons = true,
    this.onActiveStepChange,
  }) : super(key: key);

  @override
  State<StepBuilder> createState() => _StepBuilderState();
}

class _StepBuilderState extends State<StepBuilder> {
  late int currStep;
  @override
  void didChangeDependencies() {
    currStep = widget.currStep;
    super.didChangeDependencies();
  }

  onPrev() {
    setState(() {
      currStep--;
    });
    widget.onActiveStepChange?.call(currStep);
  }

  onNext() {
    setState(() {
      currStep++;
    });
    widget.onActiveStepChange?.call(currStep);
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.steps.length,
        itemBuilder: (context, i) {
          return _Step(
            importState: _State.fromIndex(i, widget.currStep),
            stepContent: widget.steps[i].copyWith(
                prevButtonLabel: (i != 0)
                    ? widget.steps[i].prevButtonLabel ??
                        (widget.useDefaultButtons ? "Prev" : null)
                    : null,
                nextButtonLabel: (i != widget.steps.length - 1)
                    ? widget.steps[i].nextButtonLabel ??
                        (widget.useDefaultButtons ? "Next" : null)
                    : null),
            onPrev: onPrev,
            onNext: onNext,
          );
        });
  }
}

class _Step extends StatelessWidget {
  final _State importState;
  final StepContent stepContent;
  final Function() onPrev;
  final Function() onNext;
  const _Step({
    Key? key,
    required this.importState,
    required this.stepContent,
    required this.onPrev,
    required this.onNext,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (importState) {
      case _State.completed:
        return ListTile(
          leading: Icon(
            MdiIcons.lockOpenCheckOutline,
            color: Theme.of(context).textTheme.bodySmall!.color,
          ),
          title: Text(
            stepContent.title,
            style: Theme.of(context)
                .textTheme
                .bodySmall!
                .copyWith(color: Theme.of(context).disabledColor),
          ),
        );

      case _State.active:
        return Container(
          decoration: BoxDecoration(
              border: Border.all(color: Theme.of(context).disabledColor)),
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              ListTile(
                isThreeLine: (stepContent.subtitle != null),
                leading: Icon(
                  MdiIcons.lockOpenVariantOutline,
                  color: Theme.of(context).textTheme.bodySmall!.color,
                ),
                title: Text(
                  stepContent.title,
                  style: Theme.of(context)
                      .textTheme
                      .bodySmall!
                      .copyWith(color: Theme.of(context).disabledColor),
                ),
                subtitle: stepContent.subtitle,
              ),
              stepContent.content ??
                  Padding(
                    padding: const EdgeInsets.all(2),
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Theme.of(context).disabledColor)),
                      child: const SizedBox(
                        height: 16,
                        width: double.infinity,
                      ),
                    ),
                  ),
              const SizedBox(
                height: 8,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  if (stepContent.prevButtonLabel != null)
                    ElevatedButton(
                        onPressed: onPrev,
                        child: Text(stepContent.prevButtonLabel!))
                  else
                    const SizedBox(
                      width: 1,
                    ),
                  if (stepContent.nextButtonLabel != null)
                    ElevatedButton(
                        onPressed: onNext,
                        child: Text(stepContent.nextButtonLabel!))
                  else
                    const SizedBox(
                      width: 1,
                    ),
                ],
              )
            ],
          ),
        );

      case _State.waiting:
        return ListTile(
          leading: Icon(
            MdiIcons.lockOutline,
            color: Theme.of(context).textTheme.bodySmall!.color,
          ),
          title: Text(
            stepContent.title,
            style: Theme.of(context)
                .textTheme
                .bodySmall!
                .copyWith(color: Theme.of(context).disabledColor),
          ),
        );
      case _State.invisible:
        return Container();
      case _State.error:
        return Container();
    }
  }
}
