import 'package:flutter/material.dart';

class SectionSplitter extends StatelessWidget {
  final String msg;
  final TextStyle? textStyle;
  const SectionSplitter({Key? key, required this.msg, this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      const Expanded(child: Divider()),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          msg,
          style: textStyle,
        ),
      ),
      const Expanded(child: Divider()),
    ]);
  }
}
