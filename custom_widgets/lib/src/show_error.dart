import 'package:flutter/material.dart';

class ShowError extends StatelessWidget {
  const ShowError({
    Key? key,
    required this.errorMessage,
  }) : super(key: key);

  final String? errorMessage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Center(
        child: Text(
          errorMessage ??
              "You may be offline, and the offline data is not synced correctly\n"
                  "The Story you are loooking for is not found in the server."
                  " Someone might have deleted or never exists",
          maxLines: 5,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
