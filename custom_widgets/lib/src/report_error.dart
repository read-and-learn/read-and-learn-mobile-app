import 'package:flutter/material.dart';

class ReportError extends StatefulWidget {
  final String errorString;
  final String details;
  final Future<void> Function(String text)? onClipboardCopy;
  const ReportError(
      {super.key,
      required this.errorString,
      required this.details,
      this.onClipboardCopy});

  @override
  State<StatefulWidget> createState() => _ReportErrorState();

  static void snackBar(BuildContext context,
      {required String errorString,
      String? details,
      duration = const Duration(seconds: 5),
      Future<void> Function(String text)? onClipboardCopy}) {
    ScaffoldMessenger.of(context)
        .removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss);
    ScaffoldMessenger.of(context).showSnackBar(
      (SnackBar(
          elevation: 0,
          behavior: SnackBarBehavior.floating,
          padding: EdgeInsets.zero,
          margin: const EdgeInsets.all(4),
          backgroundColor: Colors.transparent,
          duration: duration,
          content: ReportError(
              errorString: errorString,
              details: details ?? errorString,
              onClipboardCopy: onClipboardCopy))),
    );
  }
}

class _ReportErrorState extends State<ReportError> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: DecoratedBox(
        decoration: const BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
              bottomRight: Radius.circular(5),
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(
              height: 16,
            ),
            Text(
              widget.errorString,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.redAccent.shade200,
                  fontSize:
                      Theme.of(context).textTheme.bodyLarge!.fontSize ?? 20),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (mounted) {
                        ScaffoldMessenger.of(context).removeCurrentSnackBar(
                            reason: SnackBarClosedReason.dismiss);
                      }
                    },
                    child: Text(
                      "OK",
                      style: TextStyle(
                          color: Colors.blue.shade200,
                          decoration: TextDecoration.underline,
                          fontSize:
                              Theme.of(context).textTheme.bodyLarge!.fontSize ??
                                  20),
                    ),
                  ),
                  if (widget.onClipboardCopy != null)
                    GestureDetector(
                      onTap: () async {
                        if (mounted) {
                          widget.onClipboardCopy!(widget.details).then(
                              (value) =>
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    (const SnackBar(
                                      content: Text("Copied"),
                                      duration: Duration(seconds: 1),
                                    )),
                                  ));
                          if (mounted) {
                            ScaffoldMessenger.of(context).removeCurrentSnackBar(
                                reason: SnackBarClosedReason.dismiss);
                          }
                        }
                      },
                      child: Text(
                        "Copy Error",
                        style: TextStyle(
                            color: Colors.blue.shade200,
                            decoration: TextDecoration.underline,
                            fontSize: Theme.of(context)
                                    .textTheme
                                    .bodyLarge!
                                    .fontSize ??
                                20),
                      ),
                    )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
