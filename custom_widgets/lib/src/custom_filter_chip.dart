import 'package:flutter/material.dart';

import 'app_chip_theme.dart';

class CustomFilterChip extends StatelessWidget {
  final String label;
  final bool isSelected;
  final Function(bool) onSelected;
  const CustomFilterChip({
    Key? key,
    required this.label,
    required this.isSelected,
    required this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          chipTheme: AppChipTheme.of(context, isSelected: isSelected).data),
      child: Transform(
        transform: Matrix4.identity()..scale(1.0),
        child: FilterChip(
            showCheckmark: false,
            label: Text(label),
            selected: isSelected,
            onSelected: onSelected),
      ),
    );
  }
}
