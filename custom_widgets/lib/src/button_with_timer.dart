import 'package:flutter/material.dart';

class ElevatedButtonWithTimer extends StatelessWidget {
  const ElevatedButtonWithTimer({
    Key? key,
    required this.label,
    required this.onPressed,
  }) : super(key: key);

  final String label;
  final Function() onPressed;

  Stream<int> timer() async* {
    for (int i = 3; i >= 0; i--) {
      await Future<void>.delayed(const Duration(seconds: 1));
      yield i;
    }
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: timer(),
        builder: (
          BuildContext context,
          AsyncSnapshot<int> snapshot,
        ) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }
          final seconds = snapshot.data as int;
          if (seconds == 0) {
            WidgetsBinding.instance.addPostFrameCallback((_) => onPressed());
          }

          return ElevatedButton(
              onPressed: onPressed,
              child: Text("$label. Auto close in $seconds"));
        });
  }
}
