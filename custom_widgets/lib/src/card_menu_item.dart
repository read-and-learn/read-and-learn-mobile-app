import 'package:flutter/material.dart';

class CardMenuItem extends StatelessWidget {
  final Function()? onPressed;
  final Function()? onLongPressed;
  final String text;
  final IconData iconData;
  const CardMenuItem({
    Key? key,
    required this.onPressed,
    required this.text,
    required this.iconData,
    this.onLongPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(),
      child: TextButton(
          onPressed: onPressed,
          onLongPress: onLongPressed,
          child: DecoratedBox(
            decoration: const BoxDecoration(),
            child: Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                // mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(iconData),
                  Text(text, style: Theme.of(context).textTheme.labelSmall)
                ],
              ),
            ),
          )),
    );
  }
}
