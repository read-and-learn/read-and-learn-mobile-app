import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final String labelText;
  final TextEditingController controller;
  final bool showLabel;
  final Function(String val)? onChanged;
  final FocusNode? focusNode;

  final Widget? suffix;
  final Widget? prefix;

  const CustomTextField({
    Key? key,
    required this.labelText,
    required this.controller,
    this.focusNode,
    this.showLabel = false,
    this.onChanged,
    this.suffix,
    this.prefix,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        focusNode: focusNode,
        controller: controller,
        keyboardType: TextInputType.text,
        showCursor: true,
        textInputAction: TextInputAction.done,
        inputFormatters: [FilteringTextInputFormatter.deny(RegExp(r"\n"))],
        decoration: InputDecoration(
            border: const OutlineInputBorder(),
            labelText: showLabel ? labelText : null,
            hintText: 'Enter $labelText',
            suffix: suffix,
            prefix: prefix),
        onChanged: onChanged,
      ),
    );
  }
}
