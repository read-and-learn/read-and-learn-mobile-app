import 'package:flutter/material.dart';

class AppChipTheme {
  BuildContext context;
  bool isSelected;

  AppChipTheme._(this.context, this.isSelected);

  factory AppChipTheme.of(
    BuildContext context, {
    bool isSelected = false,
  }) {
    return AppChipTheme._(context, isSelected);
  }

  ChipThemeData get data => isSelected ? _selected : _default;

  TextStyle get textStyle => Theme.of(context).textTheme.labelMedium!;
  Color get color => textStyle.color!;

  Color get colorSelected => (Theme.of(context).brightness == Brightness.dark)
      ? Colors.green.shade200
      : Colors.lightGreen.shade900;

  ChipThemeData get _default => ChipThemeData(
        side: BorderSide(width: 1.0, color: color),
        labelStyle: textStyle,
        backgroundColor: Colors.transparent,
        selectedColor: Colors.transparent,
      );
  ChipThemeData get _selected => _default.copyWith(
      side: _default.side!.copyWith(color: colorSelected),
      labelStyle: textStyle.copyWith(
          color: colorSelected, fontWeight: FontWeight.w700));
}
