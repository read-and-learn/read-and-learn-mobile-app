import 'package:flutter/material.dart';

import 'blink.dart';

class BlinkButtonWarn extends StatelessWidget {
  final String label;
  final IconData icon;
  final Function()? onPressed;
  final TextStyle textStyle;

  const BlinkButtonWarn({
    Key? key,
    required this.label,
    required this.icon,
    required this.onPressed,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Blink(
      blinkDuration: const Duration(milliseconds: 1000),
      child: TextButton.icon(
          onPressed: onPressed, icon: Icon(icon), label: Text(label)),
    );
  }
}
