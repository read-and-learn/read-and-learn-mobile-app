import 'dart:convert';

class SyncStatus {
  final DateTime timestamp;
  final int interval;
  SyncStatus({
    required this.timestamp,
    required this.interval,
  });

  SyncStatus copyWith({
    DateTime? timestamp,
    int? interval,
  }) {
    return SyncStatus(
      timestamp: timestamp ?? this.timestamp,
      interval: interval ?? this.interval,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'timestamp': timestamp.toIso8601String(),
      'interval': interval,
    };
  }

  factory SyncStatus.fromMap(Map<String, dynamic> map) {
    return SyncStatus(
      timestamp: DateTime.parse(map['timestamp']),
      interval: map['interval'].toInt() as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory SyncStatus.fromJson(String source) =>
      SyncStatus.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'SyncStatus(timestamp: $timestamp, interval: $interval)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SyncStatus &&
        other.timestamp == timestamp &&
        other.interval == interval;
  }

  @override
  int get hashCode => timestamp.hashCode ^ interval.hashCode;
}
