import 'dart:convert';

import 'package:dart_hocr/dart_hocr.dart';

import '../helpers/capture.dart';
import 'ocrimage_storyio.dart';
import 'query_word.dart';

class Story {
  final int id;
  final String title;
  final String lang;
  final String content;
  final int? offset;
  final String? contentType;
  late final List<String> tapMarkedContent;
  Story({
    required this.id,
    required this.title,
    required this.lang,
    required this.content,
    required this.contentType,
    this.offset,
  }) {
    if (offset == null) {
      tapMarkedContent = [text];
      return;
    }
    tapMarkedContent = tapMark(text, offset!);
  }

  String get text {
    if (contentType == null) return content;
    if (contentType == "ocr_image") {
      return OCRImageStoryIO.fromStory(this).text;
    }

    // Should not have come here
    assert(false);
    throw Error();
  }

  Story copyWith({
    int? id,
    String? title,
    String? lang,
    String? content,
    String? contentType,
    int? offset,
  }) {
    return Story(
      id: id ?? this.id,
      title: title ?? this.title,
      lang: lang ?? this.lang,
      content: content ?? this.content,
      contentType: contentType ?? this.contentType,
      offset: offset ?? this.offset,
    );
  }

  Map<String, dynamic> toMap() {
    if (id < 0) {
      return <String, dynamic>{
        'title': title,
        'lang': lang,
        'content': content,
      };
    }
    return <String, dynamic>{
      'id': id,
      'title': title,
      'lang': lang,
      'content': content,
    };
  }

  static bool validateJSON(dynamic content) {
    // Scale for other types here
    return OCRImage.isOCRImage(content);
  }

  factory Story.fromMap(Map<String, dynamic> map) {
    String? contentType;

    try {
      contentType = jsonDecode(map['content'])['contentType'];
    } on Exception {
      contentType = null;
    }

    return Story(
        id: (map['id']?.toInt() ?? -1) as int,
        title: map['title'] as String,
        lang: map['lang'] as String,
        content: map['content'] as String,
        contentType: contentType);
  }

  String toJson() => json.encode(toMap());

  factory Story.fromJson(String source) =>
      Story.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Story(id: $id, title: $title, lang: $lang, content: $content, offset: $offset)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Story &&
        other.title == title &&
        other.lang == lang &&
        other.content == content;
  }

  @override
  int get hashCode {
    return title.hashCode ^ lang.hashCode ^ content.hashCode;
  }

  static List<String> tapMark(String text, int offset) {
    List<String> sentence = text.capture(
      offset: offset,
      sep: r"\?!\.\n\r।",
    );
    List<String> tmpContentList = [text];
    switch (sentence.length) {
      case 1:
        List<String> word = sentence[0].capture(
            offset: offset,
            sep: r'!"#$¥%&()*+,.\/\\:;<=>?@[\]^_`{|}~\n\s“”।0-9'
                "'"
                r"\u{0001F600}-\u{0001F64F}\u{0001F300}-\u{0001F5FF}\u{0001F680}-\u{0001F6FF}\u{0001F1E0}-\u{0001F1FF}");
        switch (word.length) {
          case 1:
            break;
          case 3:
            tmpContentList = [word[0], "", word[1], "", word[2]];
        }
        break;
      case 3:
        List<String> word = sentence[1].capture(
            offset: offset - sentence[0].length,
            sep: r'!"#$¥%&()*+,.\/\\:;<=>?@[\]^_`{|}~\n\s“”।0-9'
                "'"
                r"\u{0001F600}-\u{0001F64F}\u{0001F300}-\u{0001F5FF}\u{0001F680}-\u{0001F6FF}\u{0001F1E0}-\u{0001F1FF}");
        switch (word.length) {
          case 1:
            // No word detected, but we have sentence.
            tmpContentList = [sentence[0], sentence[1], "", "", sentence[2]];
            break;
          case 3:
            tmpContentList = [
              sentence[0],
              word[0],
              word[1],
              word[2],
              sentence[2]
            ];
            break;
        }
        break;
    }
    return tmpContentList;
  }

  List<String>? get sentence {
    if ([3, 5].contains(tapMarkedContent.length)) {
      return tapMarkedContent.sublist(1, tapMarkedContent.length - 1);
    }
    return null;
  }

  String? get word {
    if ([3, 5].contains(tapMarkedContent.length)) {
      String word = tapMarkedContent[tapMarkedContent.length ~/ 2];
      if (word.trim().isEmpty) return null;
      return word.trim();
    }
    return null;
  }

  QueryWord? get queryWord => word == null
      ? null
      : QueryWord(word: word!, lang: lang, sentence: sentence);

  bool get isOCRImage => contentType == 'ocr_image';
}
