// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'user.dart';

class Auth {
  static const String key = 'auth';
  final String server;
  final User? user;
  Auth({
    required this.server,
    this.user,
  });

  Auth copyWith({
    String? server,
    User? user,
  }) {
    return Auth(
      server: server ?? this.server,
      user: user ?? this.user,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'server': server,
      'user': user?.toMap(),
    };
  }

  factory Auth.fromMap(Map<String, dynamic> map) {
    return Auth(
      server: map['server'] as String,
      user: map['user'] != null
          ? User.fromMap(map['user'] as Map<String, dynamic>)
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Auth.fromJson(String source) =>
      Auth.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Auth(server: $server, user: $user)';

  @override
  bool operator ==(covariant Auth other) {
    if (identical(this, other)) return true;

    return other.server == server && other.user == user;
  }

  @override
  int get hashCode => server.hashCode ^ user.hashCode;
}
