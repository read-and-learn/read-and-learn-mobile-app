import 'dart:convert';

import 'package:collection/collection.dart';

class FeatTag {
  final String tag;
  final String editorType;
  final List<String> type;
  final List<Candidate> candidates;

  FeatTag({
    required this.tag,
    required this.editorType,
    required this.type,
    required this.candidates,
    List<String>? selectedValues,
  });

  FeatTag copyWith(
      {String? tag,
      String? editorType,
      List<String>? type,
      List<Candidate>? candidates}) {
    return FeatTag(
        tag: tag ?? this.tag,
        editorType: editorType ?? this.editorType,
        type: type ?? this.type,
        candidates: candidates ?? this.candidates);
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'tag': tag,
      'editor': editorType,
      'type': type,
      'candidates': candidates.map((x) => x.toMap()).toList(),
    };
  }

  factory FeatTag.fromMap(Map<String, dynamic> map) {
    return FeatTag(
      tag: map['tag'] as String,
      editorType: map['editor'] as String,
      type: List<String>.from(
          (map['type'] as List<dynamic>).map((e) => e as String)),
      candidates: List<Candidate>.from(
        (map['candidates'] as List<dynamic>).map<Candidate>(
          (x) => Candidate.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory FeatTag.fromJson(String source) =>
      FeatTag.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'Tag(tag: $tag, editorType: $editorType, type: $type, candidates: $candidates)';
  }

  @override
  bool operator ==(covariant FeatTag other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other.tag == tag &&
        other.editorType == editorType &&
        listEquals(other.type, type) &&
        listEquals(other.candidates, candidates);
  }

  @override
  int get hashCode {
    return tag.hashCode ^
        editorType.hashCode ^
        type.hashCode ^
        candidates.hashCode;
  }
}

class Candidate {
  final String name;
  final String description;
  Candidate({
    required this.name,
    required this.description,
  });

  Candidate copyWith({
    String? name,
    String? description,
  }) {
    return Candidate(
      name: name ?? this.name,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'description': description,
    };
  }

  factory Candidate.fromMap(Map<String, dynamic> map) {
    return Candidate(
      name: map['name'] as String,
      description: (map['description'] ?? "") as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory Candidate.fromJson(String source) =>
      Candidate.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Candidate(name: $name, description: $description)';

  @override
  bool operator ==(covariant Candidate other) {
    if (identical(this, other)) return true;

    return other.name == name && other.description == description;
  }

  @override
  int get hashCode => name.hashCode ^ description.hashCode;
}
