import 'dart:convert';

class AudioPref {
  final double volume;
  final double rate;
  AudioPref({
    this.volume = 1.0,
    this.rate = 1.0,
  });

  AudioPref copyWith({
    double? volume,
    double? rate,
  }) {
    return AudioPref(
      volume: volume ?? this.volume,
      rate: rate ?? this.rate,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'volume': volume.toStringAsFixed(2),
      'rate': rate.toStringAsFixed(2),
    };
  }

  factory AudioPref.fromMap(Map<String, dynamic> map) {
    return AudioPref(
      volume: double.parse(map['volume']),
      rate: double.parse(map['rate']),
    );
  }

  String toJson() => json.encode(toMap());

  factory AudioPref.fromJson(String source) =>
      AudioPref.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'AudioPref(volume: $volume, rate: $rate)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AudioPref && other.volume == volume && other.rate == rate;
  }

  @override
  int get hashCode => volume.hashCode ^ rate.hashCode;
}
