import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'package:dart_hocr/dart_hocr.dart';

import 'story.dart';

extension OCRImageStoryIO on OCRImage {
  static OCRImage fromStory(Story story) {
    final content = jsonDecode(story.content);
    return OCRImage(
      id: story.id,
      title: story.title,
      lang1: story.lang,
      // From content

      width: content['width'] as double,
      height: content['height'] as double,
      lang2: content['lang2'] != null ? content['lang2'] as String : null,
      xml: content['xml'] as String,
      thumbnail: (content['thumbnail'] != null)
          ? OCRImage.decodeImage(
              Uint8List.fromList(content['thumbnail'].cast<int>()),
              optimalSize: false)!
          : null,
    );
  }

  static String generateTitleIfMissing(String title, String content) {
    if (title.isNotEmpty) {
      return title;
    }
    if (content.isNotEmpty) {
      List<String>? firstLine = content.split(RegExp(r"[\r\n]"))[0].split(" ");
      title = firstLine.getRange(0, min(firstLine.length, 4)).join(" ");
      if (title.length > 25) {
        title = title.substring(0, 25);
      }
      title += " ...";
      return title;
    } else {
      return "unknown";
    }
  }

  static const defaultImageName = "New Image";

  static Story toStory(OCRImage ocrImage) {
    if (ocrImage.edittable) {
      return Story(
        id: ocrImage.id ?? -1,
        title: ocrImage.title ?? defaultImageName,
        content: jsonEncode(ocrImage.toContentMap()),
        contentType: 'ocr_image',
        lang: ocrImage.lang1,
      );
    } else {
      return Story(
        id: ocrImage.id ?? -1,
        title: (ocrImage.title != null) && (ocrImage.title != defaultImageName)
            ? ocrImage.title!
            : generateTitleIfMissing("", ocrImage.text),
        content: ocrImage.text,
        contentType: null,
        lang: ocrImage.lang1,
      );
    }
  }
}
