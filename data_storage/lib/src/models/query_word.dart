// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:collection/collection.dart';

class QueryWord {
  String word;
  String lang;
  List<String>? sentence;
  QueryWord({
    required this.word,
    required this.lang,
    this.sentence,
  });

  QueryWord copyWith({
    String? word,
    String? lang,
    List<String>? sentence,
  }) {
    return QueryWord(
      word: word ?? this.word,
      lang: lang ?? this.lang,
      sentence: sentence ?? this.sentence,
    );
  }

  @override
  String toString() =>
      'QueryWord(word: $word, lang: $lang, sentence: $sentence)';

  @override
  bool operator ==(covariant QueryWord other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other.word == word &&
        other.lang == lang &&
        listEquals(other.sentence, sentence);
  }

  @override
  int get hashCode {
    var hash = word.hashCode ^ lang.hashCode;
    if (sentence == null) {
      return hash ^ sentence.hashCode;
    }
    for (var s in sentence!) {
      hash ^= s.hashCode;
    }
    return hash;
  }
}
