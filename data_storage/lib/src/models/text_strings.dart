import 'dart:convert';

import 'package:collection/collection.dart';

import 'csv_content.dart';
import 'text_string.dart';
import 'query_word.dart';

class TextStrings {
  String lang;
  Map<String, TextString> items;
  TextStrings({
    required this.lang,
    required this.items,
  });

  TextStrings copyWith({
    String? lang,
    Map<String, TextString>? items,
  }) {
    return TextStrings(
      lang: lang ?? this.lang,
      items: items ?? this.items,
    );
  }

  Map<String, dynamic> toMap() {
    final itemMap = Map.fromEntries(
        items.entries.map((e) => MapEntry(e.key, e.value.toMap())));
    return <String, dynamic>{
      'lang': lang,
      'items': itemMap,
    };
  }

  factory TextStrings.fromMap(Map<String, dynamic> map) {
    final itemsMap = map['items'] as Map<String, dynamic>;
    final Map<String, TextString> items = Map.fromEntries(itemsMap.entries
        .map((e) => MapEntry(e.key, TextString.fromMap(e.value))));

    return TextStrings(
      lang: map['lang'] as String,
      items: items,
    );
  }

  //for some reasons, nesting TextString seems not go well with encode
  String toJson() => json.encode(toMap());

  factory TextStrings.fromJson(String source) {
    final map = json.decode(source);

    return TextStrings.fromMap(map as Map<String, dynamic>);
  }

  @override
  String toString() => 'TextStrings(lang: $lang, items: $items)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final mapEquals = const DeepCollectionEquality().equals;

    return other is TextStrings &&
        other.lang == lang &&
        mapEquals(other.items, items);
  }

  @override
  int get hashCode => lang.hashCode ^ items.hashCode;

  factory TextStrings.fromCsv({required CsvContent csvContent}) {
    Map<String, TextString> items = {};

    for (var v in csvContent.content) {
      TextString textString = TextString.fromCsv(
          attributes: csvContent.header,
          values: v,
          langs: csvContent.langs.map((e) => e as String).toList());
      if (items.containsKey(v[0])) {
        items[v[0]]!.merge(textString);
      } else {
        items[v[0]] = textString;
      }
    }
    //
    return TextStrings(lang: csvContent.langs[0]!, items: items);
  }
  TextString? getWord(QueryWord qWord) {
    if (qWord.lang == lang && items.containsKey(qWord.word)) {
      return items[qWord.word];
    }
    return null;
  }
}
