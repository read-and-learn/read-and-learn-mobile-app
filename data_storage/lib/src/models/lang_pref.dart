import 'dart:convert';

import 'package:collection/collection.dart';

class LangPref {
  final List<String> learningLanguages;
  final String nativeLanguage;
  final String? currLearningLang;
  LangPref({
    required this.learningLanguages,
    required this.nativeLanguage,
    String? currLearningLang,
  }) : currLearningLang = (learningLanguages.contains(currLearningLang)
                ? currLearningLang
                : null) ??
            ((learningLanguages.isNotEmpty) ? learningLanguages.first : null);

  LangPref copyWith({
    List<String>? learningLanguages,
    String? nativeLanguage,
    String? currLearningLang,
  }) {
    return LangPref(
      learningLanguages: learningLanguages ?? this.learningLanguages,
      nativeLanguage: nativeLanguage ?? this.nativeLanguage,
      currLearningLang: currLearningLang ?? this.currLearningLang,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'learningLanguages': learningLanguages,
      'nativeLanguage': nativeLanguage,
      'currLearningLang': currLearningLang ?? "",
    };
  }

  factory LangPref.fromMap(Map<String, dynamic> map) {
    return LangPref(
      learningLanguages: (map['learningLanguages'] as List<dynamic>)
          .map((e) => e as String)
          .toList(),
      nativeLanguage: map['nativeLanguage'] as String,
      currLearningLang: map['currLearningLang'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory LangPref.fromJson(String source) =>
      LangPref.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'LangPref(learningLanguages: $learningLanguages, nativeLanguage: $nativeLanguage, currLearningLang: $currLearningLang)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is LangPref &&
        listEquals(other.learningLanguages, learningLanguages) &&
        other.nativeLanguage == nativeLanguage &&
        other.currLearningLang == currLearningLang;
  }

  @override
  int get hashCode =>
      learningLanguages.hashCode ^
      nativeLanguage.hashCode ^
      currLearningLang.hashCode;
}
