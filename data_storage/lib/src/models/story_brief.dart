import 'dart:convert';

class StoryBrief {
  final int id;
  final String title;
  StoryBrief({
    required this.id,
    required this.title,
  });

  StoryBrief copyWith({
    int? id,
    String? title,
  }) {
    return StoryBrief(
      id: id ?? this.id,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
    };
  }

  factory StoryBrief.fromMap(Map<String, dynamic> map) {
    return StoryBrief(
      id: map['id'].toInt() as int,
      title: map['title'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory StoryBrief.fromJson(String source) =>
      StoryBrief.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'StoryBrief(id: $id, title: $title)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is StoryBrief && other.id == id && other.title == title;
  }

  @override
  int get hashCode => id.hashCode ^ title.hashCode;
}
