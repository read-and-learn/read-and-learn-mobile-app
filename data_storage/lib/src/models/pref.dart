import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'audio_pref.dart';
import 'lang_pref.dart';
import 'sync_status.dart';

class ThemeModePref {
  static String enum2str(ThemeMode themeMode) {
    switch (themeMode) {
      case ThemeMode.system:
        return "system";

      case ThemeMode.dark:
        return "dark";

      case ThemeMode.light:
        return "light";
    }
  }

  static ThemeMode str2enum(String str) {
    switch (str) {
      case "auto":
      case "system":
        return ThemeMode.system;
      case "on":
      case "dark":
        return ThemeMode.dark;
      case "light":
      case "off":
      case "Off":
        return ThemeMode.light;
    }
    throw Exception("Unexpected value for darkmode");
  }
}

class Pref {
  final String key;
  final String value;
  Pref(this.key, this.value);
  static Map<String, dynamic> defaultPrefList = {
    "lang_pref": LangPref(learningLanguages: [], nativeLanguage: "en").toJson(),
    "dark_mode": ThemeModePref.enum2str(ThemeMode.system),
    "reader_font_size": jsonEncode(20.0),
    "min_font_size": jsonEncode(20.0 - 4.0),
    "max_font_size": jsonEncode(20.0 + 4.0),
    "audio_pref": AudioPref(rate: 1.0, volume: 1.0).toJson(),
    "sync_status": SyncStatus(
      timestamp: DateTime.utc(2022, 01, 14),
      interval: 24,
    ).toJson()
  };

  static Future<dynamic> getValue(String key) async {
    final prefs = await SharedPreferences.getInstance();

    final String? v = prefs.getString(key);
    return (v != null) ? v : defaultPrefList[key]!;
  }

  static Future<dynamic> setValue(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);

    return value;
  }

  static Future<bool> deleteValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return await prefs.remove(key);
  }
}
