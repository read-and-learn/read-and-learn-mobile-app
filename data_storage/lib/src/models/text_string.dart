import 'dart:convert';

import 'package:collection/collection.dart';

import 'story_brief.dart';

enum Attribute {
  unknown,
  meaning,
  usage,
  rootWord,
  partOfSpeech,
  gender,
  number,
  tense,
  caseSuffixes,
}

enum TextType {
  unknown,
  word,
  sentence,
  phrase,
}

String? textTypes2Map(TextType textTypes) {
  const Map<TextType, String?> map = {
    TextType.word: "word",
    TextType.sentence: "sentence",
    TextType.phrase: "phrase",
    TextType.unknown: null
  };
  return map[textTypes];
}

TextType map2TextType(String? str) {
  if (str == null) return TextType.unknown;
  const Map<String, TextType> map = {
    "word": TextType.word,
    "sentence": TextType.sentence,
  };
  return map[str] ?? TextType.unknown;
}

Map<Attribute, String> attr2String = {
  Attribute.meaning: "meaning",
  Attribute.usage: "usage",
  Attribute.unknown: "unknown",
  Attribute.rootWord: "root word",
  Attribute.partOfSpeech: "part of speech",
  Attribute.gender: "gender",
  Attribute.number: "number",
  Attribute.tense: "tense",
  Attribute.caseSuffixes: "case suffixes",
};

Map<String, Attribute> string2Attr = {
  "meaning": Attribute.meaning,
  "usage": Attribute.usage,
  "unknown": Attribute.unknown,
  "root word": Attribute.rootWord,
  "part of speech": Attribute.partOfSpeech,
  "gender": Attribute.gender,
  "number": Attribute.number,
  "tense": Attribute.tense,
  "case suffixes": Attribute.caseSuffixes,
};

class Feat {
  final Attribute attr;
  final TextString value;
  Feat({
    required this.attr,
    required this.value,
  });

  Feat copyWith({
    Attribute? attr,
    TextString? value,
  }) {
    return Feat(
      attr: attr ?? this.attr,
      value: value ?? this.value,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'attr': attr2String[attr],
      'value': value.toMap(),
    };
  }

  factory Feat.fromMap(Map<String, dynamic> map) {
    final feat = Feat(
      attr: string2Attr[map['attr']] ?? Attribute.unknown,
      value: TextString.fromMap(map['value'] as Map<String, dynamic>),
    );
    return feat;
  }

  String toJson() => json.encode(toMap());

  factory Feat.fromJson(String source) =>
      Feat.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Feat(attr: $attr, value: $value)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Feat && other.attr == attr && other.value == value;
  }

  @override
  int get hashCode => attr.hashCode ^ value.hashCode;
}

class TextString {
  final String text;
  final String lang;
  final TextType type;
  final List<Feat> feat;
  final List<StoryBrief> stories;
  final int featCount;
  TextString({
    required this.text,
    required this.lang,
    this.type = TextType.unknown,
    List<Feat>? feat,
    List<StoryBrief>? stories,
  })  : feat = feat ?? [],
        stories = stories ?? [],
        featCount = feat?.length ?? 0;

  TextString copyWith({
    String? text,
    String? lang,
    TextType? type,
    List<Feat>? feat,
    List<StoryBrief>? stories,
  }) {
    return TextString(
      text: text ?? this.text,
      lang: lang ?? this.lang,
      type: type ?? this.type,
      feat: feat ?? this.feat,
      stories: stories ?? this.stories,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'text': text,
      'lang': lang,
      'type': textTypes2Map(type),
      "stories": stories.map((x) => x.toMap()).toList(),
      'feat': feat.map((x) => x.toMap()).toList(),
    };
  }

  factory TextString.fromMap(Map<String, dynamic> map) {
    return TextString(
      text: map['text'] as String,
      lang: map['lang'] as String,
      type: map2TextType(map['type']),
      stories: map.containsKey('stories')
          ? List<StoryBrief>.from(
              (map['stories'] as List<dynamic>).map<StoryBrief>(
                (x) => StoryBrief.fromMap(x as Map<String, dynamic>),
              ),
            )
          : [],
      feat: map.containsKey('feat')
          ? List<Feat>.from(
              (map['feat'] as List<dynamic>).map<Feat>(
                (x) => Feat.fromMap(x as Map<String, dynamic>),
              ),
            )
          : [],
    );
  }

  String toJson() {
    final map = toMap();

    return json.encode(map);
  }

  factory TextString.fromJson(String source) =>
      TextString.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'TextString(text: $text, lang: $lang, type: $type, feat: $feat, feat_count: $featCount)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is TextString &&
        other.text == text &&
        other.lang == lang &&
        other.type == type &&
        listEquals(other.feat, feat) &&
        listEquals(other.stories, stories) &&
        other.featCount == featCount;
  }

  bool isSame(Object other) {
    if (identical(this, other)) return true;
    return other is TextString && other.text == text && other.lang == lang;
  }

  @override
  int get hashCode {
    return text.hashCode ^
        lang.hashCode ^
        type.hashCode ^
        feat.hashCode ^
        stories.hashCode ^
        featCount.hashCode;
  }

  static TextString fromCsv({
    required List<String> attributes,
    required List<String> values,
    required List<String> langs,
  }) {
    if (attributes[0] == "word") {
      List<Feat> feat =
          attributes.where((element) => element != 'word').map((e) {
        var index = attributes.indexOf(e);
        return Feat(
            attr: string2Attr[e] ?? Attribute.unknown,
            value: TextString(
                text: values[index],
                lang: langs[index],
                type: TextType.unknown));
      }).toList();
      return TextString(
        text: values[0],
        lang: langs[0],
        type: TextType.word,
        stories: [],
        feat: feat,
      );
    }
    throw Exception("textstring not found");
  }

  TextString merge(TextString other) {
    if (isSame(other)) {
      // TODO(anandas): confirm if this works as expected
      List<Feat> featList = [
        ...{...feat, ...other.feat}
      ];
      List<StoryBrief> storiesList = [
        ...{...stories, ...other.stories}
      ];
      return copyWith(
          feat: featList,
          stories: storiesList,
          type: (type != TextType.unknown) ? type : other.type);
    } else {
      throw Exception("Can't merge different cards");
    }
  }

  //rename this as getFeatByLang
  List<Feat> getFeature(Attribute attribute, String lang) {
    return feat
        .where((element) =>
            element.attr == attribute && element.value.lang == lang)
        .toList();
  }

  TextString addFeature(Feat feat) {
    List<Feat> featList = [
      ...{...this.feat, feat}
    ];
    return copyWith(feat: featList);
  }

  TextString replaceFeature(Feat feat) {
    final featListR = this.feat;
    featListR.removeWhere((Feat e) => e.attr == feat.attr);
    List<Feat> featList = [
      ...{...featListR, feat}
    ];
    return copyWith(feat: featList);
  }

  TextString removeFeature(Attribute attribute, String lang) {
    List<Feat> featList = feat
        .where((element) =>
            !(element.attr == attribute && element.value.lang == lang))
        .toList();

    return copyWith(feat: featList);
  }

  bool has(Attribute attribute) => getFeat(attribute).isNotEmpty;

  List<Feat> getFeat(Attribute attribute) =>
      feat.where((e) => e.attr == attribute).toList();

  List<Feat> get meaningList => getFeat(Attribute.meaning);
  List<Feat> get usageList => getFeat(Attribute.usage);

  bool isType(TextType textType) => textType == type;

  bool isTypeOneAmong(List<TextType> textTypes) => textTypes.contains(type);

  bool usedIn({required int storyId}) {
    return stories.where((story) => story.id == storyId).toList().isNotEmpty;
  }
}
