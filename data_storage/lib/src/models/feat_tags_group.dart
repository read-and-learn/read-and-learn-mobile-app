import 'dart:convert';

import 'package:collection/collection.dart';

import 'feat_tag.dart';

class FeatTagsGroup {
  final Map<String, FeatTag> tags;
  FeatTagsGroup({
    required this.tags,
  });

  FeatTagsGroup copyWith({
    Map<String, FeatTag>? tags,
  }) {
    return FeatTagsGroup(
      tags: tags ?? this.tags,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'tags': tags.values.toList(),
    };
  }

  factory FeatTagsGroup.fromMap(Map<String, dynamic> map) {
    List<FeatTag> tags = (map['tags'] as List<dynamic>)
        .map((x) => FeatTag.fromMap(x as Map<String, dynamic>))
        .toList();
    return FeatTagsGroup(tags: {for (var x in tags) x.tag: x});
  }

  String toJson() => json.encode(toMap());

  factory FeatTagsGroup.fromJson(String source) =>
      FeatTagsGroup.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'FeatTagsGroup(tags: $tags)';

  @override
  bool operator ==(covariant FeatTagsGroup other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return listEquals(other.tags, tags);
  }

  @override
  int get hashCode => tags.hashCode;
}
