import 'dart:convert';

class User {
  final String name;
  final String accessToken;
  final String refreshToken;
  User({
    required this.name,
    required this.accessToken,
    required this.refreshToken,
  });

  User copyWith({
    String? name,
    String? accessToken,
    String? refreshToken,
  }) {
    return User(
      name: name ?? this.name,
      accessToken: accessToken ?? this.accessToken,
      refreshToken: refreshToken ?? this.refreshToken,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'accessToken': accessToken,
      'refreshToken': refreshToken,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      name: map['name'] as String,
      accessToken: map['accessToken'] as String,
      refreshToken: map['refreshToken'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) =>
      User.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'User(name: $name, accessToken: $accessToken, refreshToken: $refreshToken)';

  @override
  bool operator ==(covariant User other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.accessToken == accessToken &&
        other.refreshToken == refreshToken;
  }

  @override
  int get hashCode =>
      name.hashCode ^ accessToken.hashCode ^ refreshToken.hashCode;
}
