import 'dart:convert';

import 'package:collection/collection.dart';

class CsvContent {
  final List<String> header;
  final List<String?> langs;
  final List<List<String>> content;
  CsvContent({
    required this.header,
    List<String?>? langs,
    required this.content,
  }) : langs = langs ?? header.map((e) => null).toList();

  CsvContent copyWith({
    List<String>? header,
    List<String?>? langs,
    List<List<String>>? content,
  }) {
    return CsvContent(
      header: header ?? this.header,
      langs: langs ?? this.langs,
      content: content ?? this.content,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'header': header,
      'langs': langs,
      'content': content,
    };
  }

  factory CsvContent.fromMap(Map<String, dynamic> map) {
    return CsvContent(
      header: List<String>.from((map['header'] as List<String>)),
      langs: List<String?>.from((map['langs'] as List<String?>)),
      content: List<List<String>>.from(
        (map['content'] as List<dynamic>).map<List<String>>(
          (x) => x,
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory CsvContent.fromJson(String source) =>
      CsvContent.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'CsvContent(header: $header, langs: $langs, content: $content)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is CsvContent &&
        listEquals(other.header, header) &&
        listEquals(other.langs, langs) &&
        listEquals(other.content, content);
  }

  @override
  int get hashCode => header.hashCode ^ langs.hashCode ^ content.hashCode;
}
