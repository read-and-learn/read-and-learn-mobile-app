import 'dart:convert';
import 'package:collection/collection.dart';

import 'auth.dart';
import 'story.dart';

class Stories {
  final List<Story> stories;
  Stories({
    required this.stories,
  }) {
    if (stories.isNotEmpty &&
        stories.where((element) => (element.id <= 0)).toList().isNotEmpty) {
      throw Exception("Story with id can't be present in Stories");
    }
  }

  Stories copyWith({
    List<Story>? stories,
  }) {
    return Stories(
      stories: stories ?? this.stories,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'stories': stories.map((x) => x.toMap()).toList(),
    };
  }

  factory Stories.fromMap(Map<String, dynamic> map) {
    return Stories(
      stories: List<Story>.from(
        (map['stories'] as List<dynamic>).map<Story>(
          (x) => Story.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory Stories.fromJson(String source) =>
      Stories.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'Stories(stories: $stories)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is Stories && listEquals(other.stories, stories);
  }

  @override
  int get hashCode => stories.hashCode;

  bool get isEmpty => stories.isEmpty;

  Stories add(Story story, {required Auth authData}) {
    return copyWith(stories: [
      ...stories.where((element) => element.id != story.id).toList(),
      story
    ]);
  }

  Story? findbyID(int id) {
    final list = stories.where((element) => element.id == id).toList();
    if (list.isNotEmpty) {
      return list.first;
    }
    return null;
  }
}
