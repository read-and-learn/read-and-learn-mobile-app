import '../models/text_string.dart';
import '../models/text_strings.dart';

abstract class TextStringsResource {
  Future<TextString?> get({required String text});
  Future<TextStrings> getAll();

  Future<void> insert({required TextString textString});
  Future<void> setAll(TextStrings textStrings);

  Future<void> remove({required String word});
  Future<void> clear();
}
