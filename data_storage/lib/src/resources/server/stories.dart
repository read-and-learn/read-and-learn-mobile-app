import 'dart:convert';

import '../../helpers/rest_api.dart';
import '../../models/auth.dart';
import '../../models/stories.dart';
import '../../models/story.dart';
import '../stories.dart';

class ServerStories implements StoriesResource {
  final Auth? authData;

  ServerStories({required this.authData});

  checkAuthentication() {
    if (authData == null) {
      throw Exception("Authentication failed");
    }
  }

  @override
  Future<Story> insert({required Story story}) async {
    checkAuthentication();
    late final Map<String, dynamic> resp;
    late final String respString;
    if (story.id >= 0) {
      resp = jsonDecode(await RestApi(authData!.server).put(
        '/story/${story.id}',
        bodyJSON: story.toJson(),
        auth: authData!.user!.accessToken,
      ));
    } else {
      try {
        respString = await RestApi(authData!.server).post(
          '/new_story',
          bodyJSON: story.toJson(),
          auth: authData!.user!.accessToken,
        );
        resp = jsonDecode(respString);
      } on Exception {
        throw Exception(
            "server returns non JSON, software throws exception in server");
      }
    }
    if (resp.containsKey('id')) {
      return story.copyWith(id: resp['id']);
    }

    throw Exception(
        "upload story (id: ${resp["id"]}): Server responded with $resp");
  }

  @override
  Future<Story?> getById({required int id}) async {
    checkAuthentication();
    final Map<String, dynamic> resp =
        jsonDecode(await RestApi(authData!.server).get(
      '/story/$id',
      auth: authData!.user!.accessToken,
    ));

    if (resp.containsKey("id")) {
      return Story.fromMap(resp);
    }

    throw Exception("download story (id: $id): Server responded with $resp");
  }

  @override
  Future<bool> remove({required int id}) async {
    checkAuthentication();
    final Map<String, dynamic> resp =
        jsonDecode(await RestApi(authData!.server).delete(
      '/story/$id',
      auth: authData!.user!.accessToken,
    ));

    if (resp.containsKey("success")) {
      return true;
    }

    throw Exception("delete story (id: $id): Server responded with $resp");
  }

  @override
  Future<Stories> getAll() async {
    checkAuthentication();
    final Map<String, dynamic> resp =
        jsonDecode(await RestApi(authData!.server).get(
      '/stories?content=1',
      auth: authData?.user?.accessToken,
    ));
    if (resp.containsKey("stories")) {
      return Stories.fromMap(resp);
    }
    throw Exception("download stories: Server responded with $resp");
  }

  @override
  Future<void> clear() async {
    checkAuthentication();
    // No API to clear all data in server!
    throw UnimplementedError();
  }
}
