import 'dart:convert';

import '../../helpers/rest_api.dart';
import '../../models/auth.dart';
import '../../models/text_string.dart';
import '../../models/text_strings.dart';
import '../text_strings.dart';

class ServerTextStrings implements TextStringsResource {
  final Auth? authData;
  final String lang;

  ServerTextStrings({required this.authData, required this.lang});

  checkAuthentication() {
    if (authData?.server == null) {
      throw Exception("Unknown server");
    }
    RestApi(authData!.server).checkConnection();
    if (authData?.user == null) {
      throw Exception("User not logged in");
    }
  }

  @override
  Future<void> insert(
      {required TextString textString, bool replace = false}) async {
    checkAuthentication();
    String query = replace ? '1' : '0';
    late final Map<String, dynamic> resp;
    final serverResponse = await RestApi(authData!.server).post(
      '/word/${textString.lang}/${textString.text}?replace=$query',
      bodyJSON: textString.toJson(),
      auth: authData!.user!.accessToken,
    );
    try {
      resp = jsonDecode(serverResponse);
    } catch (err) {
      throw Exception(
          "Failed to understand the server responds \n $serverResponse");
    }
    try {
      if (resp.containsKey('lang')) {
        // final received = TextString.fromMap(resp); -- server need not sent
        return;
      }
    } catch (err) {
      rethrow;
    }
    throw Exception("upload TextStrings Server responded with $resp");
  }

  @override
  Future<TextString?> get({required String text}) async {
    checkAuthentication();
    late final Map<String, dynamic> resp;
    final serverResponse = await RestApi(authData!.server).get(
      '/word/$lang/$text',
      auth: authData!.user!.accessToken,
    );
    try {
      resp = jsonDecode(serverResponse);
    } catch (err) {
      throw Exception(
          "Failed to understand the server responds \n $serverResponse");
    }
    try {
      if (resp.containsKey('lang')) {
        return TextString.fromMap(resp);
      }
    } catch (err) {
      rethrow;
    }
    throw Exception("Server responded with $resp");
  }

  @override
  Future<TextStrings> getAll() async {
    try {
      checkAuthentication();
      late final Map<String, dynamic> resp;

      resp = jsonDecode(await RestApi(authData!.server).get(
        '/word/$lang',
        auth: authData!.user!.accessToken,
      ));

      if (resp.containsKey('lang') && lang == resp['lang']) {
        return TextStrings.fromJson(jsonEncode(resp));
      }
      if (lang == resp['lang']) {
        throw Exception(
            "Incorrect language received, expected, $lang, received ${resp["lang"]}");
      }

      throw Exception("lang: $lang: Server responded with $resp");
    } on Exception {
      rethrow;
    }
  }

  @override
  Future<void> clear() async {
    checkAuthentication();
    // No API to clear all data in server!
    throw UnimplementedError();
  }

  @override
  Future<void> setAll(TextStrings textStrings) async {
    checkAuthentication();
    late final Map<String, dynamic> resp;
    final serverResponse = await RestApi(authData!.server).post(
      '/word/$lang',
      bodyJSON: textStrings.toJson(),
      auth: authData!.user!.accessToken,
    );
    try {
      resp = jsonDecode(serverResponse);
    } catch (err) {
      throw Exception(
          "Failed to understand the server responds \n $serverResponse");
    }
    try {
      if (resp.containsKey('lang')) {
        return;
      }
    } catch (err) {
      rethrow;
    }
    throw Exception("upload TextStrings Server responded with $resp");
  }

  @override
  Future<void> remove({required String word}) {
    checkAuthentication();
    // Implement when needed
    throw UnimplementedError();
  }
}
