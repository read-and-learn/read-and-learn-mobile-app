import 'package:data_storage/src/models/text_strings.dart';

import 'package:data_storage/src/models/text_string.dart';

import '../text_strings.dart';
import 'local.dart';

class LocalTextStrings implements TextStringsResource {
  final String lang;
  final String storageName;

  LocalTextStrings({required this.lang}) : storageName = "words_$lang.json";

  @override
  Future<TextStrings> clear() async {
    await Local.clear(storageName: storageName);
    return TextStrings(lang: lang, items: {});
  }

  @override
  Future<TextString?> get({required String text}) async => await Local.get(
      storageName: storageName, key: text, decoder: TextString.fromJson);

  @override
  Future<TextStrings> getAll() async => TextStrings(
      lang: lang,
      items: (await Local.getAll(
              storageName: storageName, decoder: TextString.fromJson))
          .map((key, value) => MapEntry(key, value as TextString)));

  @override
  Future<void> insert({required TextString textString}) async =>
      await Local.insert(
          storageName: storageName,
          key: textString.text,
          value: textString,
          encoder: (textString) => (textString as TextString).toJson());

  @override
  Future<void> remove({required String word}) async =>
      await Local.remove(storageName: storageName, key: word);

  @override
  Future<void> setAll(TextStrings textStrings) async => await Local.setAll(
      storageName: storageName,
      records: textStrings.items,
      encoder: (textString) => (textString as TextString).toJson());
}
