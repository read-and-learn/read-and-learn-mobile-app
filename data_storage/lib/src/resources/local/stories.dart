import '../../models/stories.dart';
import '../../models/story.dart';
import '../stories.dart';
import 'local.dart';

/// [Future] If we never get multiple stories, we could
/// convert most of them as static class, may save some memory
class LocalStories implements StoriesResource {
  String storageName = "stories.json";

  @override
  Future<Story> insert({required Story story}) async {
    await Local.insert(
        storageName: storageName,
        key: story.id.toString(),
        value: story,
        encoder: (story) => (story as Story).toJson());
    return story;
  }

  @override
  Future<Story?> getById({required int id}) async => await Local.get(
      storageName: storageName, key: id.toString(), decoder: Story.fromJson);

  @override
  Future<bool> remove({required int id}) async {
    await Local.remove(storageName: storageName, key: id.toString());
    return true;
  }

  @override
  Future<Stories> getAll() async => Stories(
      stories: (await Local.getAll(
              storageName: storageName, decoder: Story.fromJson))
          .values
          .map((e) => e as Story)
          .toList());

  Future<void> setAll(Stories stories) async => await Local.setAll(
      storageName: storageName,
      records: {for (var e in stories.stories) e.id.toString(): e},
      encoder: (story) => (story as Story).toJson());

  @override
  Future<Stories> clear() async {
    await Local.clear(storageName: storageName);
    return Stories(stories: []);
  }
}
