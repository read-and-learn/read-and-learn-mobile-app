import 'dart:convert';
import 'package:localstorage/localstorage.dart';

class Local {
  static Future<void> set({
    required String storageName,
    required String key,
    required dynamic value,
    required String Function(Object) encoder,
  }) async {
    final storage = LocalStorage(storageName);
    await storage.ready;
    //final json = story.toJson();
    await storage.setItem(key, value, encoder);
  }

  static Future<dynamic> get({
    required String storageName,
    required String key,
    required Object Function(String) decoder,
  }) async {
    final storage = LocalStorage(storageName);
    await storage.ready;
    final storyMap = await storage.getItem(key);
    if (storyMap == null) return null;
    return decoder(storyMap);
  }

  static Future<void> delete({
    required String storageName,
    required String key,
  }) async {
    final storage = LocalStorage(storageName);
    await storage.ready;
    await storage.deleteItem(key);
  }

  static Future<void> clear({
    required String storageName,
  }) async {
    final storage = LocalStorage(storageName);
    await storage.ready;
    await storage.clear();
  }

  static Future<Map<String, dynamic>> getAll({
    required String storageName,
    required Object Function(String) decoder,
  }) async {
    List<String> keys = await _getKeys(storageName: storageName);
    Map<String, dynamic> items = {};
    if (keys.isEmpty) {
      return items;
    }
    for (String key in keys) {
      dynamic item =
          await get(storageName: storageName, key: key, decoder: decoder);
      if (item == null) {
        throw Exception("Local databse is corrupted. Regeneration required");
      }
      items[key] = item;
    }
    return items;
  }

  static Future<void> setAll({
    required String storageName,
    required Map<String, dynamic> records,
    required String Function(Object) encoder,
  }) async {
    await clear(storageName: storageName);
    if (records.isEmpty) {
      return;
    }
    for (MapEntry e in records.entries) {
      await set(
          storageName: storageName,
          key: e.key,
          value: e.value,
          encoder: encoder);
    }
    await set(
        storageName: storageName,
        key: "keys",
        value: records.keys.toList(),
        encoder: jsonEncode);
  }

  static Future<void> insert({
    required String storageName,
    required String key,
    required dynamic value,
    required String Function(Object) encoder,
  }) async {
    await set(
        storageName: storageName, key: key, value: value, encoder: encoder);
    if ((await _addKey(storageName: storageName, key: key)).isEmpty) {
      clear(storageName: storageName);
    }
  }

  static Future<void> remove({
    required String storageName,
    required String key,
  }) async {
    await delete(storageName: storageName, key: key);
    if ((await _removeKey(storageName: storageName, key: key)).isEmpty) {
      clear(storageName: storageName);
    }
  }

  static Future<List<String>> _getKeys({
    required String storageName,
  }) async {
    List<dynamic> keys_ = await get(
            storageName: storageName,
            key: "keys",
            decoder: (string) => jsonDecode(string)) ??
        [];
    List<String> existingItemKeys = keys_.map((e) => e as String).toList();
    return existingItemKeys;
  }

  static Future<List<String>> _addKey({
    required String storageName,
    required String key,
  }) async {
    List<String> existingItemKeys = await _getKeys(storageName: storageName);
    List<String> updatedItemKeys = [
      ...{
        ...existingItemKeys,
        key,
      }
    ];
    if (updatedItemKeys.isNotEmpty) {
      await set(
          storageName: storageName,
          key: "keys",
          value: updatedItemKeys,
          encoder: jsonEncode);
    }
    return updatedItemKeys;
  }

  static Future<List<String>> _removeKey({
    required String storageName,
    required String key,
  }) async {
    List<String> existingItemKeys = await _getKeys(storageName: storageName);
    List<String> updatedItemKeys =
        existingItemKeys.where((element) => element != key).toList();
    if (updatedItemKeys.isNotEmpty) {
      await set(
          storageName: storageName,
          key: "keys",
          value: updatedItemKeys,
          encoder: jsonEncode);
    }
    return updatedItemKeys;
  }
}
