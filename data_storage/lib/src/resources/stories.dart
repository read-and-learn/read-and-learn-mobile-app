import '../models/stories.dart';
import '../models/story.dart';

abstract class StoriesResource {
  Future<Story> insert({required Story story});
  Future<Story?> getById({required int id});
  Future<bool> remove({required int id});
  Future<Stories> getAll();
  Future<void> clear();
}
