import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/story.dart';
import '../resources/local/stories.dart';

class StoryNotifier extends StateNotifier<AsyncValue<Story?>> {
  final int? id;

  LocalStories localStories;
  StoryNotifier(this.id)
      : localStories = LocalStories(),
        super(const AsyncValue.loading()) {
    _get();
  }

  Future<void> _get() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      if (id == null) {
        return null;
      }
      Story? story = await localStories.getById(id: id!);
      //[Future] Should we check in server?
      if (story != null) return story;
      throw Exception("Story not found, try after sync with server");
    });
  }

  Future<void> update(Story story, int offset) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      return story.copyWith(offset: offset);
    });
  }

  set offset(int offset) => state.when(
      data: (story) => story != null ? update(story, offset) : {},
      error: (err, _) {},
      loading: () {});
}

final storyNotifierProvider = StateNotifierProvider.autoDispose
    .family<StoryNotifier, AsyncValue<Story?>, int?>((
  ref,
  id,
) {
  return StoryNotifier(id);
});
