import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/pref.dart';

class PrefNotifier extends StateNotifier<AsyncValue<Pref>> {
  final String key;

  PrefNotifier(this.key) : super(const AsyncValue.loading()) {
    _get();
  }

  Future<void> _get() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final dynamic value = await getValue(key);
      return Pref(key, value);
    });
  }

  Future<void> _set(dynamic val) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final dynamic value = await setValue(key, val);
      return Pref(key, value);
    });
  }

  Future<void> clear() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      await deleteValue(key);
      final dynamic value = await getValue(key);
      return Pref(key, value);
    });
  }

  set(String val) => _set(val);
  //dynamic get value => state.value;

  static Future<dynamic> getValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      final String? v = prefs.getString(key);

      return (v != null) ? v : Pref.defaultPrefList[key]!;
    } on Exception {
      await prefs.remove(key);
      final String? v = prefs.getString(key);

      return (v != null) ? v : Pref.defaultPrefList[key]!;
    }
  }

  static Future<dynamic> setValue(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);

    return value;
  }

  static Future<bool> deleteValue(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return await prefs.remove(key);
  }
}

final prefNotifierProvider =
    StateNotifierProviderFamily<PrefNotifier, AsyncValue<Pref>, String>(
        (ref, key) {
  return PrefNotifier(key);
});
