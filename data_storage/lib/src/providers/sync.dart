import 'package:collection/collection.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SyncTasks {
  List<String> tasks;
  SyncTasks({
    required this.tasks,
  });

  @override
  String toString() => 'SyncTasks(tasks: $tasks)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return other is SyncTasks && listEquals(other.tasks, tasks);
  }

  @override
  int get hashCode => tasks.hashCode;
}

Stream<int> storageSyncStream({required SyncTasks tasks}) async* {
  for (int i = 0; i < tasks.tasks.length; i++) {
    switch (tasks.tasks[i]) {
      case "stories":
        break;
      default:
        await Future<void>.delayed(const Duration(seconds: 5));
        break;
    }

    yield i;
  }
}

final syncServiceProvider =
    StreamProvider.family.autoDispose<int, SyncTasks>((ref, tasks) {
  return storageSyncStream(tasks: tasks);
});
