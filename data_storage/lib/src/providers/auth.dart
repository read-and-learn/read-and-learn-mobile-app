import 'dart:convert';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../helpers/logger.dart';
import '../helpers/rest_api.dart';
import '../models/auth.dart';
import '../models/user.dart';

class AuthNotifier extends StateNotifier<Auth?> {
  AuthNotifier() : super(null) {
    _infoLogger("AuthNotifier/Construct: state = $state");
    restore();
  }

  Future<bool> restore() async {
    Auth? auth = await load();

    if (auth != null && auth.user != null) {
      User user = auth.user!;

      bool isTokenExpired = JwtDecoder.isExpired(user.accessToken);
      if (isTokenExpired && (user.refreshToken != "")) {
        final Map<String, dynamic> resp = jsonDecode(await RestApi(auth.server)
            .post('/refresh',
                bodyJSON: jsonEncode({}), auth: user.refreshToken));

        if (resp.containsKey("access_token")) {
          user = user.copyWith(accessToken: resp["access_token"]);
        }
        isTokenExpired = JwtDecoder.isExpired(auth.user!.accessToken);
      }
      if (user != auth.user) {}

      if (isTokenExpired) {
        await delete(); // remove user
        auth = Auth(server: auth.server, user: user);
      }
    }
    if (state != auth) {
      state = auth;
      _infoLogger("AuthNotifier/restore: state = $state");
    }

    return true;
  }

  Future<String?> login({
    required String name,
    required String password,
    required bool isConnected,
  }) async {
    try {
      final Auth? auth = state;
      if (auth == null) throw Exception("Unknown server");

      final Map<String, dynamic> resp = jsonDecode(await RestApi(auth.server)
          .post('/login',
              bodyJSON: jsonEncode({"username": name, "password": password})));
      if (resp.containsKey("access_token")) {
        final newAuth = auth.copyWith(
          user: User(
              name: name,
              accessToken: resp["access_token"]! as String,
              refreshToken: resp.containsKey("refresh_token")
                  ? resp["refresh_token"]! as String
                  : ""),
        );
        save(newAuth);
        state = newAuth;
        _infoLogger("AuthNotifier/login: state = $state");
        return null;
      }
      if (resp["message"] != null) {
        throw Exception(resp["message"]);
      }
    } on Exception catch (e) {
      return e.toString();
    }
    return "unknown error";
  }

  Future<String?> signup({
    required bool isConnected,
    required String name,
    required String password,
  }) async {
    try {
      final Auth? auth = state;
      if (auth == null) throw Exception("Unknown server");

      final Map<String, dynamic> resp = jsonDecode(await RestApi(auth.server)
          .post('/register',
              bodyJSON: jsonEncode({"username": name, "password": password})));
      if (resp.containsKey("success")) {
        return null;
      }
      if (resp["message"] != null) {
        throw Exception(resp["message"]);
      }
    } on Exception catch (e) {
      return e.toString();
    }
    return "unknown error";
  }

  Future<bool> logout(bool isConnected, {required bool deleteUser}) async {
    Auth? auth = state;
    String? error;
    if (auth != null && auth.user != null) {
      try {
        if (isConnected) {
          if (deleteUser) {
            final Map<String, dynamic> resp = jsonDecode(
                await RestApi(auth.server).delete('/user/${auth.user!.name}',
                    auth: auth.user!.accessToken));
            if (resp["success"] != "User deleted.") {
              throw Exception(
                  "delete user failed with message: ${resp["message"]}");
            }
          } else {
            final Map<String, dynamic> resp = jsonDecode(
                await RestApi(auth.server).post('/logout',
                    bodyJSON: jsonEncode(""), auth: auth.user!.accessToken));
            if (resp["success"] != "Successfully logged out") {
              throw Exception("logout failed with message: ${resp["message"]}");
            }
          }
        }
      } on Exception catch (e) {
        error = e.toString();
      }
      delete();
      final newAuth = Auth(server: auth.server);
      //save(newAuth);
      state = newAuth;
      _infoLogger("AuthNotifier/logout: state = $state");
    }

    if (error != null) {
      throw Exception(error);
    }
    return true;
  }

  Future<String?> recover(String name) async {
    return "This feature is not yet implemented. Contact admin";
  }

  Future<void> save(Auth auth) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(Auth.key, auth.toJson());
  }

  static Future<Auth?> load() async {
    final prefs = await SharedPreferences.getInstance();
    final s = prefs.getString(Auth.key);

    if (s == null) return null;
    final Auth authData = Auth.fromJson(s);

    return authData;
  }

  Future<bool> delete() async {
    final prefs = await SharedPreferences.getInstance();
    return await prefs.remove(Auth.key);
  }

  Future<void> newServer(
      {required String serverURI, required bool isConnected}) async {
    if (state != null && state!.server == serverURI) {
      // Nothing to do
      return;
    }
    if (state != null && state!.user != null) {
      // logut from current server
      await logout(isConnected, deleteUser: false);
    }
    Auth newAuth = Auth(server: serverURI);
    await save(newAuth);
    state = newAuth;
    _infoLogger("AuthNotifier/newServer: state = $state");
    return;
  }
}

final authNotifierProvider = StateNotifierProvider<AuthNotifier, Auth?>(
  (ref) {
    _infoLogger("AuthNotifier changed");
    return AuthNotifier();
  },
);

// const String defaultServer = "http://127.0.0.1:5000";
// const String defaultServer = "http://194.163.34.8:5002";

// TODO(anandas): Don't forget to revert this!
// const String defaultServer = "https://api.anandas.in";
// const String defaultServer = "https://api.cloudonlanapps.com";
const String defaultServer = "https://api.cloudonlanapps.in";

final defaultServerURIProvider = Provider<String>(
  (ref) => defaultServer,
);

const bool _disableInfoLogger = true;
void _infoLogger(String msg) {
  if (!_disableInfoLogger) {
    logger.i(msg);
  }
}
