import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'stories.dart';
import 'text_strings.dart';

class RepoReloader {
  final Ref ref;
  RepoReloader(this.ref);
  deleteStory({required int storyId, required String lang}) async {
    await ref.read(storiesNotifierProvider.notifier).removeStory(storyId);
    await ref
        .read(textStringsNotifierProvider(lang).notifier)
        .localTextStrings!
        .clear();
    await ref.read(textStringsNotifierProvider(lang).notifier).onRefresh();
  }
}

final repoReloaderProvider = Provider<RepoReloader>((ref) {
  return RepoReloader(ref);
});
