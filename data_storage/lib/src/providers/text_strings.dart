import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/auth.dart';
import '../models/csv_content.dart';
import '../models/text_string.dart';
import '../models/text_strings.dart';
import '../resources/local/text_strings.dart';
import '../resources/server/text_strings.dart';
import 'auth.dart';

class TextStringsNotifier extends StateNotifier<AsyncValue<TextStrings?>> {
  final String? lang;

  final LocalTextStrings? localTextStrings;
  final ServerTextStrings? serverTextStrings;
  TextStringsNotifier({required this.lang, required Auth? authData})
      : serverTextStrings = (lang == null)
            ? null
            : ServerTextStrings(authData: authData, lang: lang),
        localTextStrings = (lang == null) ? null : LocalTextStrings(lang: lang),
        super(const AsyncValue.loading()) {
    _reload();
  }

  Future<void> _reload() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      if (lang == null) return null;
      return await loadTextStrings();
    });
  }

  addTextStringsfromCSV(CsvContent csvContent) async {
    state = const AsyncValue.loading();
    TextStrings textStrings = TextStrings.fromCsv(csvContent: csvContent);
    /* final merged = */ await serverTextStrings?.setAll(textStrings);
    await localTextStrings?.clear();
    await _reload();
  }

  Future<TextStrings> loadTextStrings() async {
    TextStrings textStrings;
    if (localTextStrings != null) {
      textStrings = await localTextStrings!.getAll();
      if (textStrings.items.isNotEmpty) {
        return textStrings;
      }
    }

    if (serverTextStrings != null) {
      try {
        textStrings = await serverTextStrings!.getAll();
        if (localTextStrings != null) {
          await localTextStrings!.setAll(textStrings);
          textStrings = await localTextStrings!.getAll();
        }
        return textStrings;
      } on Exception {
        return TextStrings(items: {}, lang: lang!);
      }
    }
    throw Exception("No source to find the vocabulary");
  }

  clear() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      if (lang == null) return null;
      return await localTextStrings?.clear();
    });
  }

  Future<void> addFeat(
      {required TextString textString, required Feat feat}) async {
    // Using replace feature, later we need to go back to addFeature
    TextString updated = textString.replaceFeature(feat);
    await save(updated, replace: true);
  }

  Future<void> save(TextString textString, {bool replace = false}) async {
    state = const AsyncValue.loading();
    if (serverTextStrings != null) {
      await serverTextStrings!.insert(textString: textString, replace: replace);
      final fromServer = await serverTextStrings!.get(text: textString.text);
      if (fromServer == null) {
        throw Exception(
            "Unexpected, server can't find the word ${textString.text}");
      }
      textString = fromServer;
    }
    if (localTextStrings != null) {
      await localTextStrings!.insert(textString: textString);
    }

    await _reload();
  }

  onRefresh() => _reload();
}

final textStringsNotifierProvider = StateNotifierProvider.family<
    TextStringsNotifier, AsyncValue<TextStrings?>, String?>((ref, lang) {
  Auth? authData = ref.watch(authNotifierProvider);

  return TextStringsNotifier(lang: lang, authData: authData);
});
