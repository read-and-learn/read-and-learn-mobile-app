import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/auth.dart';
import '../models/stories.dart';
import '../models/story.dart';
import '../resources/local/stories.dart';
import '../resources/server/stories.dart';
import 'auth.dart';

class StoriesNotifier extends StateNotifier<AsyncValue<Stories>> {
  final Auth? authData;
  final LocalStories localStories;
  final ServerStories serverStories;

  StoriesNotifier({required this.authData})
      : localStories = LocalStories(),
        serverStories = ServerStories(authData: authData),
        super(const AsyncValue.loading()) {
    _get();
  }

  Future<void> _get() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      return await _loadStories();
    });
  }

  Future<Stories> _loadStories({bool fromServer = false}) async {
    if (fromServer == false) {
      Stories stories = await localStories.getAll();
      if (stories.stories.isNotEmpty) {
        return stories;
      }
    }
    Stories stories;
    stories = await serverStories.getAll();
    await localStories.setAll(stories);

    /// This is redundant, can be removed if perfromance becoem an issue
    /// while loading
    stories = await localStories.getAll();
    return stories;
  }

  Future<Story> insertStory(Story story) async {
    Story storyWithID = await serverStories.insert(story: story);
    await localStories.insert(story: storyWithID);
    await onRefresh();
    return storyWithID;
  }

  Future<void> removeStory(int id) async {
    state = const AsyncValue.loading();
    if (await serverStories.remove(id: id)) {
      await localStories.remove(id: id);
      await onRefresh();
      return;
    }
    throw Exception("Coudn't find story with id $id in server");
  }

  clear() async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      return await localStories.clear();
    });
  }

  onRefresh() async => await _get();
}

final storiesNotifierProvider =
    StateNotifierProvider<StoriesNotifier, AsyncValue<Stories>>((
  ref,
) {
  Auth? authData = ref.watch(authNotifierProvider);

  return StoriesNotifier(authData: authData);
});
