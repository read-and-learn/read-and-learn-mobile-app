import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/text_string.dart';
import '../../models/text_strings.dart';
import '../models/vocab_filter.dart';
import '../models/vocab_filters.dart';
import '../../providers/text_strings.dart';
import '../resources/filters.dart';

class VocabFiltersNotifier extends StateNotifier<AsyncValue<VocabFilters>> {
  AsyncValue<TextStrings?> textStringsAsync;
  VocabFiltersNotifier({required this.textStringsAsync})
      : super(const AsyncValue.loading()) {
    _create();
  }

  _create() async {
    state = const AsyncValue.loading();
    textStringsAsync.whenOrNull(
      data: (textStrings) async {
        state = await AsyncValue.guard(() async {
          return VocabFilters(textStrings: textStrings, vocabFilters: {
            "textType": textTypeFilter,
            "meaning": meaningFilter,
            "usage": usageFilter,
          });
        });
      },
      // How to communicate if any error occurs.
      // error: (err, _) => throw Exception(err),
    );
  }

  void filterKeepKey(String filter, dynamic key, bool value) {
    state.whenOrNull(data: (VocabFilters vocabFilters) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabFilters.filterKeepKey(filter, key, value);
      });
    });
  }

  void filterKeepHit(String filter, bool value) {
    state.whenOrNull(data: (VocabFilters vocabFilters) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabFilters.filterKeepHit(filter, value);
      });
    });
  }

  void filterKeepMiss(String filter, bool value) {
    state.whenOrNull(data: (VocabFilters vocabFilters) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabFilters.filterKeepMiss(filter, value);
      });
    });
  }

  void upsertFilter(String key, VocabFilter<dynamic, TextString> vocabFilter) {
    state.whenOrNull(data: (VocabFilters vocabFilters) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabFilters.upsertFilter(key, vocabFilter);
      });
    });
  }

  void removeFilter(String key) {
    state.whenOrNull(data: (VocabFilters vocabFilters) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabFilters.removeFilter(key);
      });
    });
  }

  clear() => _create();
}

final vocabFiltersNotifierProvider = StateNotifierProvider.family
    .autoDispose<VocabFiltersNotifier, AsyncValue<VocabFilters>, String?>(
        (ref, String? lang) {
  AsyncValue<TextStrings?> textStrings =
      ref.watch(textStringsNotifierProvider(lang));

  return VocabFiltersNotifier(textStringsAsync: textStrings);
});
