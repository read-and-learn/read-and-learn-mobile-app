// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/connectivity_state.dart';

class ConnectivityStateNotifier extends StateNotifier<ConnectivityState> {
  late final StreamSubscription<ConnectivityResult>? subscription;
  ConnectivityStateNotifier()
      : super(ConnectivityState(connectivityResult: ConnectivityResult.none)) {
    subscribe();
  }
  subscribe() async {
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      state = state.copyWith(connectivityResult: result);
    });
    state = state.copyWith(
        connectivityResult: await (Connectivity().checkConnectivity()));
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }
}

final connectivityStateNotifierProvider =
    StateNotifierProvider<ConnectivityStateNotifier, ConnectivityState>((ref) {
  return ConnectivityStateNotifier();
});
