import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/pref.dart';
import '../../providers/pref.dart';
import '../models/styles.dart';

class StyleNotifier extends StateNotifier<StyleManager> {
  final ThemeMode themeMode;
  StyleNotifier(this.themeMode) : super(StyleManager(themeMode)) {
    if (themeMode == ThemeMode.system) {
      var window = WidgetsBinding.instance.window;
      window.onPlatformBrightnessChanged =
          () => state = StyleManager.platform();
    }
  }
}

final styleNotifierProvider =
    StateNotifierProvider<StyleNotifier, StyleManager>((ref) {
  ThemeMode themeMode = ref.watch(prefNotifierProvider("dark_mode")).maybeWhen(
      data: (Pref pref) => ThemeModePref.str2enum(pref.value),
      orElse: () =>
          (WidgetsBinding.instance.window.platformBrightness == Brightness.dark)
              ? ThemeMode.dark
              : ThemeMode.light);

  return StyleNotifier(themeMode);
});
