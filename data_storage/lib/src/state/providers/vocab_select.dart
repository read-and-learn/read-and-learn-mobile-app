import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/text_strings.dart';
import '../models/vocab_select.dart';
import '../../providers/text_strings.dart';

class VocabSelectNotifier extends StateNotifier<AsyncValue<VocabSelect>> {
  AsyncValue<TextStrings?> textStringsAsync;

  VocabSelectNotifier({required this.textStringsAsync})
      : super(const AsyncValue.loading()) {
    _refresh();
  }
  _refresh() async {
    state = const AsyncValue.loading();
    textStringsAsync.whenOrNull(
      data: (TextStrings? textStrings) async {
        state = await AsyncValue.guard(() async {
          if (textStrings == null) return VocabSelect();
          return VocabSelect(textStrings: textStrings);
        });
      },
      // How to communicate if any error occurs.
      //error: (err, _) => throw Exception(err),
    );
  }

  select(String item) {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.select(item);
      });
    });
  }

  deselect(String item) {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.deselect(item);
      });
    });
  }

  toggle(String item) {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.toggle(item);
      });
    });
  }

  selectMultiple(List<String> item) {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.selectMultiple(item);
      });
    });
  }

  deselectMultiple(List<String> item) {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.deselectMultiple(item);
      });
    });
  }

  clear() {
    state.whenOrNull(data: (VocabSelect vocabSelect) async {
      state = const AsyncValue.loading();
      state = await AsyncValue.guard(() async {
        return vocabSelect.clear();
      });
    });
  }
}

final vocabSelectNotifierProvider = StateNotifierProvider.family
    .autoDispose<VocabSelectNotifier, AsyncValue<VocabSelect>, String?>(
        (ref, String? lang) {
  AsyncValue<TextStrings?> textStrings =
      ref.watch(textStringsNotifierProvider(lang));

  return VocabSelectNotifier(textStringsAsync: textStrings);
});
