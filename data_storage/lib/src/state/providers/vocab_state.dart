import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/lang_pref.dart';
import '../../models/pref.dart';
import '../models/vocab_state.dart';
import '../../providers/pref.dart';

class VocabStateNotifier extends StateNotifier<VocabState> {
  AsyncValue<Pref> pref;
  VocabStateNotifier(this.pref) : super(VocabState()) {
    pref.whenData((Pref pref) {
      LangPref langPref = LangPref.fromJson(pref.value);
      state = state.copyWith(
          displayLang: langPref.currLearningLang,
          learningLanguages: langPref.learningLanguages);
    });
  }

  set searchView(bool value) => state = state.copyWith(searchView: value);

  set selectView(bool value) => state = state.copyWith(selectView: value);

  set displayLang(String lang) => state = state.copyWith(displayLang: lang);

  set tappedIndex(int? index) => state = state.copyWith(tappedIndex: index);

  set isFocussed(bool flag) => state = state.copyWith(isFocussed: flag);
}

final vocabStateProvider =
    StateNotifierProvider<VocabStateNotifier, VocabState>((ref) {
  final AsyncValue<Pref> langPrefAsync =
      ref.watch(prefNotifierProvider("lang_pref"));

  return VocabStateNotifier(langPrefAsync);
});
