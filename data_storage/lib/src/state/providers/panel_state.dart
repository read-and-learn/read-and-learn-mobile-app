import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../models/text_string.dart';
import '../models/panel_state.dart';

final panelStateProvider = Provider<PanelState>((ref) {
  return PanelState(
      textString: TextString(
          text: "not set yet",
          lang: PanelState.placeholderLang,
          type: TextType.unknown));
});
