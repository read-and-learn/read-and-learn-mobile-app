import 'package:flutter_riverpod/flutter_riverpod.dart';

class PanelActiveStateNotifier extends StateNotifier<bool> {
  PanelActiveStateNotifier() : super(true);

  void enablePanel() => state = true;
  void disablePanel() => state = false;
}

final panelActiveStateNotifierProvider =
    StateNotifierProvider<PanelActiveStateNotifier, bool>((ref) {
  return PanelActiveStateNotifier();
});
