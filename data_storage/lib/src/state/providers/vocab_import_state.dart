import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/vocab_import_state.dart';

class VocabImportNotifier extends StateNotifier<VocabImportState> {
  VocabImportNotifier() : super(VocabImportState(currStep: 0));

  //set currStep(val) => state = state.copyWith(currStep: val);
  set errMessage(val) => state = state.copyWith(errMessage: val);
  set fileContent(val) => state = state.copyWith(fileContent: val, currStep: 1);
  set uploadContent(val) => state =
      state.copyWith(uploadContent: val, currStep: 2, fileContent: null);

  clear() {
    state = state.copyWith(
        fileContent: null, uploadContent: null, currStep: 0, errMessage: null);
  }
}

final vocabImportNotifierProvider =
    StateNotifierProvider<VocabImportNotifier, VocabImportState>((ref) {
  return VocabImportNotifier();
});
