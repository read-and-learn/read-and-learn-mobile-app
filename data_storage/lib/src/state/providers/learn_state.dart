import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/learn_state.dart';

class LearnStateNotifier extends StateNotifier<LearnState> {
  LearnStateNotifier(super.learnState);

  set index(int val) => state = state.copyWith(currIndex: val);
  setWords(
          {required List<String> words,
          required String vocablang,
          required String learnersLang}) =>
      state = state.copyWith(
          words: words,
          currIndex: 0,
          vocabLang: vocablang,
          learnersLang: learnersLang);
}

final learnStateNotifierProvider =
    StateNotifierProvider<LearnStateNotifier, LearnState>((ref) {
  return LearnStateNotifier(LearnState(words: [], currIndex: 0));
});
