import '../../models/text_string.dart';
import '../models/vocab_filter.dart';

final textTypeFilter = VocabFilter<TextType, TextString>(
    title: "Text Type",
    keysLabel: {
      TextType.word: "Words",
      TextType.sentence: "Sentences",
      TextType.unknown: "Unclassified"
    },
    exceptsLabel: null,
    isHit: (TextString textString, List<TextType> keys) =>
        textString.isTypeOneAmong(keys));

final meaningFilter = VocabFilter<Attribute, TextString>(
  title: "Meaning",
  keysLabel: {Attribute.meaning: "has Meaning"},
  exceptsLabel: "no meaning",
  isHit: (TextString textString, List<Attribute> key) =>
      textString.has(key.first),
);

final usageFilter = VocabFilter<Attribute, TextString>(
  title: "Example",
  keysLabel: {Attribute.usage: "Has Example"},
  exceptsLabel: "No Example",
  isHit: (TextString textString, List<Attribute> key) =>
      textString.has(key.first),
);

VocabFilter<int, TextString> vocabularyOf(
    {required int id, required String title}) {
  return VocabFilter<int, TextString>(
      title: title,
      keysLabel: {id: "Used"},
      exceptsLabel: "Not used",
      isHit: (TextString textString, List<int> key) =>
          textString.usedIn(storyId: id),
      keepKeys: [id],
      defaultKeepMiss: false);
}
