import 'package:collection/collection.dart';

import 'vocab_filter.dart';

import '../../models/text_string.dart';
import '../../models/text_strings.dart';

class VocabFilters {
  final TextStrings? _textStrings;
  Map<String, VocabFilter<dynamic, TextString>> vocabFilters;

  VocabFilters({
    TextStrings? textStrings,
    required this.vocabFilters,
  }) : _textStrings = textStrings;

  VocabFilters copyWith(
      {TextStrings? textStrings,
      Map<String, VocabFilter<dynamic, TextString>>? vocabFilters}) {
    return VocabFilters(
        textStrings: textStrings ?? _textStrings,
        vocabFilters: vocabFilters ?? this.vocabFilters);
  }

  @override
  String toString() =>
      'VocabFilters(_textStrings: $_textStrings, vocabFilters: $vocabFilters)';

  @override
  bool operator ==(covariant VocabFilters other) {
    if (identical(this, other)) return true;
    final mapEquals = const DeepCollectionEquality().equals;

    return other._textStrings == _textStrings &&
        mapEquals(other.vocabFilters, vocabFilters);
  }

  @override
  int get hashCode => _textStrings.hashCode ^ vocabFilters.hashCode;

  List<String> activeFilters() {
    return Map.fromEntries(vocabFilters.entries
        .where((filt) => filt.value.removeCondition != null)).keys.toList();
  }

  TextStrings? _filterred() {
    if (_textStrings == null) return _textStrings;
    final Map<String, TextString> updateItems = Map.from(_textStrings!.items);
    updateItems.removeWhere((String text, TextString textString) {
      for (MapEntry<String, VocabFilter> vocabFilters
          in vocabFilters.entries.toList()) {
        final remove =
            vocabFilters.value.removeCondition?.call(textString as dynamic) ??
                false;
        if (remove) {
          return true;
        }
      }
      return false;
    });
    return _textStrings!.copyWith(items: updateItems);
  }

  List<String> get getFilterred => _filterred()?.items.keys.toList() ?? [];

  VocabFilters filterKeepKey(String filter, dynamic key, bool value) {
    final Map<String, VocabFilter<dynamic, TextString>> vocabFilters =
        Map.from(this.vocabFilters);
    if (vocabFilters.containsKey(filter)) {
      vocabFilters[filter] = vocabFilters[filter]!.keep(key, value);
      return copyWith(vocabFilters: vocabFilters);
    }
    return this;
  }

  VocabFilters filterKeepHit(String filter, bool value) {
    final Map<String, VocabFilter<dynamic, TextString>> vocabFilters =
        Map.from(this.vocabFilters);
    if (vocabFilters.containsKey(filter)) {
      vocabFilters[filter] = vocabFilters[filter]!.setKeepHit(value);
      return copyWith(vocabFilters: vocabFilters);
    }
    return this;
  }

  VocabFilters filterKeepMiss(String filter, bool value) {
    final Map<String, VocabFilter<dynamic, TextString>> vocabFilters =
        Map.from(this.vocabFilters);
    if (vocabFilters.containsKey(filter)) {
      vocabFilters[filter] = vocabFilters[filter]!.setKeepMiss(value);
      return copyWith(vocabFilters: vocabFilters);
    }
    return this;
  }

  VocabFilters upsertFilter(
      String key, VocabFilter<dynamic, TextString> vocabFilter) {
    final Map<String, VocabFilter<dynamic, TextString>> vocabFilters =
        Map.from(this.vocabFilters);
    vocabFilters[key] = vocabFilter;

    return copyWith(vocabFilters: vocabFilters);
  }

  VocabFilters removeFilter(String key) {
    final Map<String, VocabFilter<dynamic, TextString>> vocabFilters =
        Map.from(this.vocabFilters);
    if (vocabFilters.containsKey(key)) {
      vocabFilters.remove(key);
    }
    return copyWith(vocabFilters: vocabFilters);
  }
}
