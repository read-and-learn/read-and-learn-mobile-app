import 'package:collection/collection.dart';

import '../../models/text_string.dart';
import '../../models/text_strings.dart';

class VocabSelect {
  final TextStrings? _textStrings;
  final Map<String, bool> _selected;
  VocabSelect({TextStrings? textStrings})
      : _textStrings = textStrings,
        _selected =
            textStrings?.items.map((key, value) => MapEntry(key, false)) ?? {};

  VocabSelect._(
      {required TextStrings? textStrings, required Map<String, bool> selected})
      : _textStrings = textStrings,
        _selected = selected;

  VocabSelect copyWith({Map<String, bool>? selected}) {
    return VocabSelect._(
        textStrings: _textStrings, selected: selected ?? _selected);
  }

  @override
  String toString() =>
      'VocabSelect(_textStrings: $_textStrings, _selected: $_selected)';

  @override
  bool operator ==(covariant VocabSelect other) {
    if (identical(this, other)) return true;
    final mapEquals = const DeepCollectionEquality().equals;

    return other._textStrings == _textStrings &&
        mapEquals(other._selected, _selected);
  }

  @override
  int get hashCode => _textStrings.hashCode ^ _selected.hashCode;

  validate(String item) {
    if ((_textStrings == null) || !_textStrings!.items.keys.contains(item)) {
      throw Exception("Invalid selection");
    }
  }

  validateMultiple(List<String> items) {
    if ((_textStrings == null)) {
      throw Exception("Invalid selection");
    }
    if (items
        .where((element) => !_textStrings!.items.keys.contains(element))
        .toList()
        .isNotEmpty) {
      throw Exception("Invalid selection");
    }
  }

  VocabSelect select(String item) {
    validate(item);
    final selected = _selected;
    selected[item] = true;
    return copyWith(selected: selected);
  }

  VocabSelect deselect(String item) {
    validate(item);
    final selected = _selected;
    selected[item] = false;
    return copyWith(selected: selected);
  }

  VocabSelect toggle(String item) {
    final selected = _selected;
    selected[item] = !selected[item]!;
    return copyWith(selected: selected);
  }

  VocabSelect selectMultiple(List<String> items) {
    validateMultiple(items);
    final selected = _selected;
    for (var item in items) {
      selected[item] = true;
    }
    return copyWith(selected: selected);
  }

  VocabSelect deselectMultiple(List<String> items) {
    validateMultiple(items);
    final selected = _selected;
    for (var item in items) {
      selected[item] = false;
    }
    return copyWith(selected: selected);
  }

  VocabSelect clear() {
    final selected = _selected;
    for (var item in selected.keys.toList()) {
      selected[item] = false;
    }
    return copyWith(selected: selected);
  }

  bool isSelected(String item) => _selected[item]!;

  List<String> get selected =>
      _selected.keys.where((element) => _selected[element] == true).toList();

  List<String> getSelectedWords({List<String>? list}) =>
      list?.where((e) => selected.contains(e)).toList() ?? selected;

  List<TextString> getSelected({List<String>? list}) {
    if (_selected.isEmpty) return [];

    final words = getSelectedWords(list: list);
    return words.map((e) => _textStrings!.items[e]!).toList();
  }
}
