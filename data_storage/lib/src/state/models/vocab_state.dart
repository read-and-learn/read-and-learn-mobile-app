import 'package:collection/collection.dart';

class VocabState {
  final List<String> learningLanguages;
  final String? displayLang;
  final bool searchView;
  final bool selectView;
  final bool isFocussed;
  final int? tappedIndex;

  VocabState(
      {this.displayLang,
      this.searchView = false,
      this.selectView = false,
      this.isFocussed = false,
      List<String>? learningLanguages,
      this.tappedIndex})
      : learningLanguages = learningLanguages ?? [];

  VocabState copyWith(
      {List<String>? learningLanguages,
      String? displayLang,
      bool? searchView,
      bool? selectView,
      int? tappedIndex,
      bool? isFocussed}) {
    return VocabState(
        learningLanguages: learningLanguages ?? this.learningLanguages,
        displayLang: displayLang ?? this.displayLang,
        searchView: searchView ?? this.searchView,
        selectView: selectView ?? this.selectView,
        tappedIndex: tappedIndex ?? this.tappedIndex,
        isFocussed: isFocussed ?? this.isFocussed);
  }

  @override
  String toString() {
    return 'VocabState(learningLanguages: $learningLanguages,'
        ' displayLang: $displayLang, searchView: $searchView, '
        'selectView: $selectView, isFocussed: $isFocussed)';
  }

  @override
  bool operator ==(covariant VocabState other) {
    if (identical(this, other)) return true;
    final listEquals = const DeepCollectionEquality().equals;

    return listEquals(other.learningLanguages, learningLanguages) &&
        other.displayLang == displayLang &&
        other.searchView == searchView &&
        other.selectView == selectView;
  }

  @override
  int get hashCode {
    return learningLanguages.hashCode ^
        displayLang.hashCode ^
        searchView.hashCode ^
        selectView.hashCode;
  }
}
