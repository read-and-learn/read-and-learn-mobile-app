import '../../models/csv_content.dart';

class VocabImportState {
  final int currStep;
  final String? errMessage;
  final CsvContent? fileContent;
  final CsvContent? uploadContent;
  VocabImportState(
      {required this.currStep,
      this.errMessage,
      this.fileContent,
      this.uploadContent});

  VocabImportState copyWith({
    int? currStep,
    String? errMessage,
    CsvContent? fileContent,
    CsvContent? uploadContent,
  }) {
    return VocabImportState(
        currStep: currStep ?? this.currStep,
        errMessage: errMessage ?? this.errMessage,
        fileContent: fileContent ?? this.fileContent,
        uploadContent: uploadContent ?? this.uploadContent);
  }

  @override
  String toString() =>
      'VocabImportState(currStep: $currStep, errMessage: $errMessage, fileContent: $fileContent, uploadContent: $uploadContent)';

  @override
  bool operator ==(covariant VocabImportState other) {
    if (identical(this, other)) return true;

    return other.currStep == currStep &&
        other.errMessage == errMessage &&
        other.fileContent == fileContent &&
        other.uploadContent == uploadContent;
  }

  @override
  int get hashCode =>
      currStep.hashCode ^
      errMessage.hashCode ^
      fileContent.hashCode ^
      uploadContent.hashCode;
}
