import 'package:connectivity_plus/connectivity_plus.dart';

class ConnectivityState {
  final ConnectivityResult connectivityResult;
  ConnectivityState({
    required this.connectivityResult,
  });

  ConnectivityState copyWith({
    ConnectivityResult? connectivityResult,
  }) {
    return ConnectivityState(
      connectivityResult: connectivityResult ?? this.connectivityResult,
    );
  }

  bool get isConnected => (connectivityResult != ConnectivityResult.none);
  bool get onWifi => (connectivityResult != ConnectivityResult.wifi);

  @override
  String toString() =>
      'ConnectionState(connectivityResult: $connectivityResult)';

  @override
  bool operator ==(covariant ConnectivityState other) {
    if (identical(this, other)) return true;

    return other.connectivityResult == connectivityResult;
  }

  @override
  int get hashCode => connectivityResult.hashCode;
}
