import 'package:collection/collection.dart';

class VocabFilter<keyType, ItemType> {
  final bool? keepMiss;
  final List<keyType> keepKeys;
  final String title;
  final Map<keyType, String> keysLabel;
  final String? exceptsLabel;
  final bool Function(ItemType item, List<keyType> key) isHit;

  VocabFilter({
    required this.title,
    required this.keysLabel,
    required this.exceptsLabel,
    required this.isHit,
    List<keyType>? keepKeys,
    bool? defaultKeepMiss,
  })  : keepMiss = (exceptsLabel != null) ? defaultKeepMiss ?? true : null,
        keepKeys = keepKeys ?? keysLabel.keys.toList();

  VocabFilter<keyType, ItemType> copyWith({
    List<keyType>? keepKeys,
    bool? keepMiss,
  }) {
    return VocabFilter<keyType, ItemType>(
        defaultKeepMiss: keepMiss ?? this.keepMiss,
        keepKeys: keepKeys ?? this.keepKeys,
        title: title,
        keysLabel: keysLabel,
        exceptsLabel: exceptsLabel,
        isHit: isHit);
  }

  @override
  String toString() {
    return 'VocabFilter(keepMiss: $keepMiss, keepKeys: $keepKeys, title: $title, keysLabel: $keysLabel, exceptsLabel: $exceptsLabel, isHit: $isHit)';
  }

  @override
  bool operator ==(covariant VocabFilter<keyType, ItemType> other) {
    if (identical(this, other)) return true;
    final collectionEquals = const DeepCollectionEquality().equals;

    return other.keepMiss == keepMiss &&
        collectionEquals(other.keepKeys, keepKeys) &&
        other.title == title &&
        collectionEquals(other.keysLabel, keysLabel) &&
        other.exceptsLabel == exceptsLabel &&
        other.isHit == isHit;
  }

  @override
  int get hashCode {
    return keepMiss.hashCode ^
        keepKeys.hashCode ^
        title.hashCode ^
        keysLabel.hashCode ^
        exceptsLabel.hashCode ^
        isHit.hashCode;
  }

  VocabFilter<keyType, ItemType> addKey(keyType key) {
    if (keysLabel.containsKey(key)) {
      return copyWith(keepKeys: [
        ...{...keepKeys, key}
      ]);
    }
    throw Exception("Unrecognized key");
  }

  VocabFilter<keyType, ItemType> removeKey(keyType key) {
    if (keysLabel.containsKey(key)) {
      return copyWith(
          keepKeys: keepKeys.where((element) => element != key).toList());
    }
    throw Exception("Unrecognized key");
  }

  VocabFilter<keyType, ItemType> keep(keyType key, bool value) {
    if (value) {
      return addKey(key);
    } else {
      return removeKey(key);
    }
  }

  VocabFilter<keyType, ItemType> keepAllKeys(bool value) {
    if (value) {
      return copyWith(keepKeys: keysLabel.keys.toList());
    } else {
      return copyWith(keepKeys: []);
    }
  }

  VocabFilter<keyType, ItemType> setKeepHit(bool value) => keepAllKeys(value);

  VocabFilter<keyType, ItemType> setKeepMiss(bool value) {
    return copyWith(keepMiss: value);
  }

  bool isKeySelected(keyType key) => keepKeys.contains(key);

  bool get isActive =>
      fewSelected || ((keepMiss != null) && (keepKeys.isNotEmpty ^ keepMiss!));

  bool get fewSelected =>
      (keepKeys.length != keysLabel.length) && keepKeys.isNotEmpty;

  bool Function(ItemType)? get removeCondition => !isActive
      ? null
      : ((dynamic p0) {
          if (keepMiss == null) {
            return !isHit(p0, keepKeys);
          }

          bool isIn = isHit(p0 as ItemType, keysLabel.keys.toList());

          return (!isIn && !keepMiss!) ||
              (isIn && (keepKeys.isEmpty || !isHit(p0, keepKeys)));
        });
}
