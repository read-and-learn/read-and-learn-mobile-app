import 'package:flutter/material.dart';

import '../resources/styles.dart';
import '../resources/text_theme.dart';

class StyleManager {
  final ThemeData themeData;

  StyleManager._({required this.themeData});

  factory StyleManager(ThemeMode themeMode) {
    Map<ThemeMode, StyleManager> map = {
      ThemeMode.system: StyleManager.platform(),
      ThemeMode.light: StyleManager.light(),
      ThemeMode.dark: StyleManager.dark()
    };
    return map[themeMode]!;
  }
  factory StyleManager.platform() {
    var window = WidgetsBinding.instance.window;
    return StyleManager._(
        themeData: StyleResource(window.platformBrightness).themeData);
  }
  factory StyleManager.dark() {
    return StyleManager._(themeData: StyleResource(Brightness.dark).themeData);
  }
  factory StyleManager.light() {
    return StyleManager._(themeData: StyleResource(Brightness.light).themeData);
  }

  TextThemeResource get textThemeResource =>
      TextThemeResource(themeData.brightness);

  Color get textColor => textThemeResource.textColor;
  Color get textBackgroundColor => textThemeResource.textBackgroundColor;
}
