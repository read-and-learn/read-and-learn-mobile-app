// ignore_for_file: public_member_api_docs, sort_constructors_first

import '../../models/lang_pref.dart';
import '../../models/pref.dart';
import '../../models/text_string.dart';

class PanelHeight {
  final double whenOpen;
  final double whenClosed;

  PanelHeight({required this.whenOpen, required this.whenClosed});
}

class PanelState {
  static final placeholderLang =
      LangPref.fromJson(Pref.defaultPrefList["lang_pref"]).nativeLanguage;
  final TextString textString;
  final String nativeLanguage;
  final List<String>? _sentenceParts;
  final PanelHeight panelHeight;

  PanelState(
      {required this.textString,
      String? nativeLanguage,
      List<String>? sentenceParts,
      PanelHeight? panelHeight})
      : _sentenceParts = sentenceParts,
        nativeLanguage = nativeLanguage ?? placeholderLang,
        panelHeight = panelHeight ?? PanelHeight(whenOpen: 0, whenClosed: 0);

  @override
  String toString() =>
      'PanelState(textStrings: $textString, nativeLanguage: $nativeLanguage)';

  @override
  bool operator ==(covariant PanelState other) {
    if (identical(this, other)) return true;

    return other.textString == textString &&
        other.nativeLanguage == nativeLanguage;
  }

  @override
  int get hashCode => textString.hashCode ^ nativeLanguage.hashCode;

  List<Feat>? get meanings =>
      textString.getFeature(Attribute.meaning, nativeLanguage);

  String get word => textString.text;

  List<String> get sentenceParts => _sentenceParts ?? ["not set yet"];
  String get sentence => sentenceParts.join().trim();
  String get sourceLang => textString.lang;
  String get targetLang => nativeLanguage;
  String? get meaning {
    final meaning = meanings?.map((e) => e.value.text).join(',');
    if (meaning == null) return meaning;
    return meaning.trim().isNotEmpty ? meaning : null; // Redundant?
  }

  List<Feat>? get usageSentences =>
      textString.getFeature(Attribute.usage, textString.lang);
  bool get hasSentence =>
      (_sentenceParts != null) && _sentenceParts!.isNotEmpty;
  bool get isSaved =>
      usageSentences
          ?.where((element) => element.value.text == sentence)
          .toList()
          .isNotEmpty ??
      false;
}
