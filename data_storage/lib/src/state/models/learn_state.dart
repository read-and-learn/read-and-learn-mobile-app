class LearnState {
  int currIndex;
  int total;
  final List<String> words;
  final String? vocabLang;
  final String? learnersLang;
  LearnState(
      {required this.currIndex,
      required this.words,
      this.learnersLang,
      this.vocabLang})
      : total = words.length;

  LearnState copyWith(
      {int? currIndex,
      List<String>? words,
      String? vocabLang,
      String? learnersLang}) {
    return LearnState(
        currIndex: currIndex ?? this.currIndex,
        words: words ?? this.words,
        learnersLang: learnersLang ?? this.learnersLang,
        vocabLang: vocabLang ?? this.vocabLang);
  }

  @override
  String toString() =>
      'LearnState(currIndex: $currIndex, total: $total, words: $words, learnersLang: $learnersLang vocabLang: $vocabLang)';

  @override
  bool operator ==(covariant LearnState other) {
    if (identical(this, other)) return true;

    return other.currIndex == currIndex &&
        other.total == total &&
        other.learnersLang == learnersLang &&
        other.vocabLang == vocabLang;
  }

  @override
  int get hashCode =>
      currIndex.hashCode ^
      total.hashCode ^
      learnersLang.hashCode ^
      vocabLang.hashCode;
}
