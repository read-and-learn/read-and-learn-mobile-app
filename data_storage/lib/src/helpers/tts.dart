import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../models/auth.dart';
import 'rest_api.dart';
import '../models/query_word.dart';
import '../providers/auth.dart';
import '../state/providers/connectivity_state.dart';

class _TTSFunctions {
  static Future<Uint8List> ttsAudio(String word, String lang,
      {Auth? authData}) async {
    if (authData == null || authData.user == null) {
      return Uint8List(0);
    }
    final resp = await RestApi(authData.server).audio("/tts?lang=$lang",
        bodyJSON: jsonEncode(
          {"text": word},
        ),
        auth: authData.user!.accessToken);
    // TODO(anandas): Error not handled
    return resp;
  }
}

final ttsAudioProvider = FutureProvider.family
    .autoDispose<Uint8List?, QueryWord>((ref, QueryWord qWord) {
  Auth? authData = ref.watch(authNotifierProvider);
  bool isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected));
  if (!isConnected) return null;
  return _TTSFunctions.ttsAudio(qWord.word, qWord.lang, authData: authData);
});
