import 'dart:async';
import 'dart:typed_data';
import 'dart:convert' show jsonDecode, utf8;

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dart_hocr/dart_hocr.dart';
import 'package:http/http.dart' as http;

class RestApi {
  final String _server;
  final bool connectViaMobile;
  final Map<String, String> postMethodHeader = {
    "Content-Type": "application/json"
  };
  final Map<String, String> getMethodHeader = {
    "Content-Type": "application/json"
  };
  static const uploadimageTimeout = 15;

  RestApi(this._server, {this.connectViaMobile = true});

  Uri _generateURI(String endPoint) {
    return Uri.parse("$_server$endPoint");
  }

  Future<void> checkConnection() async {
    ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      throw Exception("Check your internet connection");
    }
    if (!connectViaMobile) {
      throw Exception("Connect to Wifi");
    }
    String? serverStatus = await getURLStatus();
    if (serverStatus != null) {
      throw (serverStatus);
    }
  }

  Future<String?> getURLStatus() async {
    //await Future.delayed(const Duration(seconds: 2));
    try {
      http.Response response = await http
          .get(Uri.parse(_server))
          .timeout(const Duration(seconds: 2));
      if (response.statusCode == 200) {
        if (response.body ==
            "No API defined for this endpoint. Use appropriate API call") {
          return null;
        }
        throw "This is not a Read and Learn API server";
      } else {
        throw response.statusCode.toString();
      }
    } on TimeoutException catch (_) {
      return "Server not reachable";
    } on Exception catch (e) {
      return e.toString();
    }
  }

  Future<dynamic> call(String method, String endPoint,
      {String? auth,
      String bodyJSON = "",
      Uint8List? imageData,
      String? langStr,
      Map<String, String>? extraHeaders}) async {
    await checkConnection();
    var headers = postMethodHeader;
    if (auth != null) {
      headers["Authorization"] = "Bearer $auth";
    }
    if (extraHeaders != null) {
      for (MapEntry hdrEntry in extraHeaders.entries) {
        headers[hdrEntry.key] = hdrEntry.value;
      }
    }

    final uri = _generateURI(endPoint);

    http.Response? response;
    switch (method) {
      case "post":
        response = await http.post(uri, headers: headers, body: bodyJSON);
        break;
      case 'put':
        response = await http.put(uri, headers: headers, body: bodyJSON);
        break;
      case "get":
        response = await http.get(uri, headers: headers);
        break;
      case 'delete':
        response = await http.delete(uri, headers: headers);
        break;

      case 'img_upload':
        if (imageData == null) {
          throw Exception("Image not provider");
        }
        try {
          final uploadUIR = Uri.parse(
              "$_server$endPoint${langStr == null ? "" : "?lang=$langStr"}");

          var request = http.MultipartRequest("POST", uploadUIR);
          request.fields["filename"] = "TODO";

          request.files.add(http.MultipartFile.fromBytes("file", imageData,
              filename: "image"));
          var streamedResponse = await request
              .send()
              .then((value) => value.stream.toBytes())
              .timeout(
                  const Duration(
                    seconds: uploadimageTimeout,
                  ),
                  onTimeout: () => throw Exception("Connection timedout"));
          // TODO(anandas): Error handling here...
          var decoded = utf8.decode(streamedResponse);
          return decoded;
        } on Exception catch (e) {
          throw Exception(e);
        }

      case 'audio':
        response = await http.post(uri, headers: headers, body: bodyJSON);

        //assert(response.bodyBytes.runtimeType == Uint8List);
        //print("Returning Result");
        return response.bodyBytes;
    }
    if (response != null) {
      return response.body;
    }
    throw Exception(
        "Unknown http method $method expected get, post, put or delete");
  }

  Future<String> post(String endPoint,
      {String? auth, String bodyJSON = ""}) async {
    return await call('post', endPoint, auth: auth, bodyJSON: bodyJSON);
  }

  Future<String> put(String endPoint,
      {String? auth, String bodyJSON = ""}) async {
    return await call('put', endPoint, auth: auth, bodyJSON: bodyJSON);
  }

  Future<String> get(String endPoint, {String? auth}) async {
    return await call('get', endPoint, auth: auth);
  }

  Future<String> delete(String endPoint, {String? auth}) async {
    return await call('delete', endPoint, auth: auth);
  }

  Future<Uint8List> audio(String endPoint,
      {String? auth, String bodyJSON = ""}) async {
    return await call('audio', endPoint,
        extraHeaders: {"Accept": "Application/octet-stream"},
        bodyJSON: bodyJSON,
        auth: auth);
  }

  Future<String> uploadimage(
      {String? auth,
      required Uint8List imageData,
      required String lang1,
      String? lang2}) async {
    lang1 = langCodeConversion[lang1] ?? "eng";
    if (lang2 != null) {
      lang2 = langCodeConversion[lang2] ?? "eng";
    }

    final langStr = "$lang1${(lang2 == null) ? "" : ",$lang2"}";

    final xml = await call('img_upload', '/upload/image',
        imageData: imageData, langStr: langStr);
    try {
      return uploadimageValidateServerResponseXML(xml);
    } on Exception catch (e) {
      final error = e.toString().replaceAll("Exception:", "").trim();
      throw Exception(
          "error: \"$error\", image of size: ${imageData.length}, lang(s): $langStr");
    }
  }

  String uploadimageValidateServerResponseXML(
    String xmlString,
  ) {
    if (xmlString.isEmpty) {
      throw Exception("Empty response from server");
    }
    // Try parsing as xml
    try {
      HOCRImport.fromXMLString(xmlString: xmlString);
      return xmlString;
    } on Exception {
      // Try parsing as json

      final Map<String, dynamic> jsonString = jsonDecode(xmlString);
      if (jsonString.containsKey('message')) {
        throw Exception(jsonString['message']);
      }
      throw Exception("Unexpected response from Server : $xmlString");
    }
  }
}

Map<String, String> langCodeConversion = {
  "aa": "aar",
  "ab": "abk",
  "ae": "ave",
  "af": "afr",
  "ak": "aka",
  "am": "amh",
  "an": "arg",
  "ar": "ara",
  "as": "asm",
  "av": "ava",
  "ay": "aym",
  "az": "aze",
  "ba": "bak",
  "be": "bel",
  "bg": "bul",
  "bh": "bih",
  "bi": "bis",
  "bm": "bam",
  "bn": "ben",
  "bo": "bod",
  "br": "bre",
  "bs": "bos",
  "ca": "cat",
  "ce": "che",
  "ch": "cha",
  "co": "cos",
  "cr": "cre",
  "cs": "ces",
  "cu": "chu",
  "cv": "chv",
  "cy": "cym",
  "da": "dan",
  "de": "deu",
  "dv": "div",
  "dz": "dzo",
  "ee": "ewe",
  "el": "ell",
  "en": "eng",
  "eo": "epo",
  "es": "spa",
  "et": "est",
  "eu": "eus",
  "fa": "fas",
  "ff": "ful",
  "fi": "fin",
  "fj": "fij",
  "fo": "fao",
  "fr": "fra",
  "fy": "fry",
  "ga": "gle",
  "gd": "gla",
  "gl": "glg",
  "gn": "grn",
  "gu": "guj",
  "gv": "glv",
  "ha": "hau",
  "he": "heb",
  "hi": "hin",
  "ho": "hmo",
  "hr": "hrv",
  "ht": "hat",
  "hu": "hun",
  "hy": "hye",
  "hz": "her",
  "ia": "ina",
  "id": "ind",
  "ie": "ile",
  "ig": "ibo",
  "ii": "iii",
  "ik": "ipk",
  "io": "ido",
  "is": "isl",
  "it": "ita",
  "iu": "iku",
  "ja": "jpn",
  "jv": "jav",
  "ka": "kat",
  "kg": "kon",
  "ki": "kik",
  "kj": "kua",
  "kk": "kaz",
  "kl": "kal",
  "km": "khm",
  "kn": "kan",
  "ko": "kor",
  "kr": "kau",
  "ks": "kas",
  "ku": "kur",
  "kv": "kom",
  "kw": "cor",
  "ky": "kir",
  "la": "lat",
  "lb": "ltz",
  "lg": "lug",
  "li": "lim",
  "ln": "lin",
  "lo": "lao",
  "lt": "lit",
  "lu": "lub",
  "lv": "lav",
  "mg": "mlg",
  "mh": "mah",
  "mi": "mri",
  "mk": "mkd",
  "ml": "mal",
  "mn": "mon",
  "mr": "mar",
  "ms": "msa",
  "mt": "mlt",
  "my": "mya",
  "na": "nau",
  "nb": "nob",
  "nd": "nde",
  "ne": "nep",
  "ng": "ndo",
  "nl": "nld",
  "nn": "nno",
  "no": "nor",
  "nr": "nbl",
  "nv": "nav",
  "ny": "nya",
  "oc": "oci",
  "oj": "oji",
  "om": "orm",
  "or": "ori",
  "os": "oss",
  "pa": "pan",
  "pi": "pli",
  "pl": "pol",
  "ps": "pus",
  "pt": "por",
  "qu": "que",
  "rm": "roh",
  "rn": "run",
  "ro": "ron",
  "ru": "rus",
  "rw": "kin",
  "sa": "san",
  "sc": "srd",
  "sd": "snd",
  "se": "sme",
  "sg": "sag",
  "si": "sin",
  "sk": "slk",
  "sl": "slv",
  "sm": "smo",
  "sn": "sna",
  "so": "som",
  "sq": "sqi",
  "sr": "srp",
  "ss": "ssw",
  "st": "sot",
  "su": "sun",
  "sv": "swe",
  "sw": "swa",
  "ta": "tam",
  "te": "tel",
  "tg": "tgk",
  "th": "tha",
  "ti": "tir",
  "tk": "tuk",
  "tl": "tgl",
  "tn": "tsn",
  "to": "ton",
  "tr": "tur",
  "ts": "tso",
  "tt": "tat",
  "tw": "twi",
  "ty": "tah",
  "ug": "uig",
  "uk": "ukr",
  "ur": "urd",
  "uz": "uzb",
  "ve": "ven",
  "vi": "vie",
  "vo": "vol",
  "wa": "wln",
  "wo": "wol",
  "xh": "xho",
  "yi": "yid",
  "yo": "yor",
  "za": "zha",
  "zh": "zho",
  "zu": "zul",
};
