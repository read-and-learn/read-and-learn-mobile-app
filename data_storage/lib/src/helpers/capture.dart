extension WordSelect on String {
  List<String> capture({required int offset, required String sep}) {
    if (offset > length) {
      return [this];
    }
    final String match = "[^$sep]+";
    final String separatorMatch = "[$sep]*";
    final int start = offset;
    final RegExp re1 = RegExp(
      "$separatorMatch($match)\$",
      multiLine: false,
      unicode: true,
    );
    final re2 = RegExp(
      "^($match)$separatorMatch",
      multiLine: false,
      unicode: true,
    );
    RegExpMatch? t1 = re1.firstMatch(substring(0, start));
    RegExpMatch? t2 = re2.firstMatch(substring(start));
    final part21 = (t1 == null) ? "" : t1.group(1) ?? "";
    final part22 = (t2 == null) ? "" : t2.group(1) ?? "";
    // Fix for blank word returned
    if ((part21 + part22).trim().isEmpty) {
      return [this];
    }
    int l = length;
    int lp0 = substring(0, start).length;
    //int lp1 = substring(start).length;
    int l21 = part21.length;
    int l22 = part22.length;
    int s1 = 0;
    int e1 = lp0 - l21;
    int s3 = lp0 + l22;
    int e3 = l;
    final part1 = substring(s1, e1);
    final part2 = part21 + part22;
    final part3 = substring(s3, e3);
    if (part1.isEmpty && part3.isEmpty) {
      return [this];
    }
    if (length == (part1.length + part2.length + part3.length)) {
      return [part1, part2, part3];
    } else {
      //print("Something went wrong !!!");
      return [this];
    }
  }
}
