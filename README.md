I created this app to help my son to learn Marathi. you can see more details on [cloud on lan apps' website](https://cloudonlanapps.com/readandlearn_about).

## Current Status:
* User can add new stories, and read.
* User can add meaning and example sentence for each word.
* In vocabulary section, it is possible to upload the csv with word, meaning and usage.
* In vocabulary, it is possible to add more info.	(json way)
* For both words and sentences TTS is working.
* Google translator is integrated for quick access.
* vocabulary import from CSV

## Available views:
* Story List,
* Story Reader
* Story Editor
* Vocabulary Import 
* Vocabulary list 
* Vocabulary learn
* Vocabulary edit 
* Settings.

## Planned tasks:
1. Image to Text (currently I am working on this) OCR using tesseract
1. Understand Anki and check if we can integrate it into this product for learning
1. The vocabulary is mixed for all the users, and we may need to add a field to tag the user who added. There must be an option if the entire vocabulary to be used or only user specific
1. The TTS integration - using soundpool is very basic and not working in Web interface. review the implementation.
1. Internationalization 
1. Export stories and selected vocabularies (pdf flash card, csv and json)
1. Jsonify Dictinary module, so that we can add different dictionaries
1. Expose field JSON to user
1. JSONify settings and make it backed up on server
1. Interactiveness - edit directly while reading for mistakes.
1. Allow user to add cards (like questions, info etc)
1. Gather knowledge about other languages and insert language specific tools.
    1. Identify word category, suffixes and prefixes
    2. Identify sentence type and tense
    3. Explore Named entity recognition (NER) - techniques and use it to boose intelligence 
1. Add a credit page that points to other packages used
1. add a help screen at start


## Implementation related:
1. Move to gitlab issues instead of this notes for features and bugs
1. Code review and second opinion on the implementation
1. UI/UX needs review and improvement 
1. start documentation
1. Testing - Can we have automated testing?
1. Introduce another development server.

## known issue:
1. The vocabulary is not in sync and manual sync is required everytime something changed (Date based sync and auto sync at start up needs to be checked)
2. When offline, we should allow adding content and cache is for upload - This logic is removed today.




