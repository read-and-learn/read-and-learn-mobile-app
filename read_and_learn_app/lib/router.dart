import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:read_and_learn/pages/receive_content/receive_image_page.dart';

import 'constants.dart';
import 'logger.dart';
import 'pages/authenticate/authenticate_page.dart';
import 'pages/authenticate/logout_page.dart';
import 'pages/authenticate/server_uri_page.dart';
import 'pages/common/widgets/app_theme.dart';
import 'pages/editor/editor_page.dart';
import 'pages/exceptions/offline_page.dart';
import 'pages/ocr/ocr_page.dart';
import 'pages/reader/reader_page.dart';
import 'pages/settings/settings.dart';
import 'pages/stories/stories_page.dart';
import 'pages/sync/sync_page.dart';
import 'pages/vocab_import/vocab_import_page.dart';
import 'pages/vocabulary/vocabulary_page.dart';
import 'pages/word/learn_page.dart';
import 'pages/word/word_page.dart';
import 'recieve_shared/providers/watch_shared_image.dart';

class RaLRouter extends ConsumerStatefulWidget {
  const RaLRouter({Key? key}) : super(key: key);

  @override
  ConsumerState<RaLRouter> createState() => _RaLRouterState();
}

class _RaLRouterState extends ConsumerState<RaLRouter> {
  late GoRouter _router;
  Widget transitionBuilder(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    const scheme = 'scale';
    switch (scheme) {
      case 'slide':
        return SlideTransition(
          position: Tween(begin: const Offset(1, 0), end: Offset.zero)
              .animate(animation),
          child: child,
        );
      case 'scale':
        return ScaleTransition(
          scale: animation,
          child: child,
        );
      case 'size':
        return Align(
          child: SizeTransition(
            sizeFactor: animation,
            child: child,
          ),
        );
      case 'rotation':
        return RotationTransition(
          turns: animation,
          child: child,
        );
      case 'fade':
      default:
        return FadeTransition(opacity: animation, child: child);
    }
  }

  @override
  Widget build(BuildContext context) {
    _infoLogger('RaLRouter.build');
    _router = GoRouter(
      routes: <GoRoute>[
        GoRoute(
          path: OfflinePage.path,
          name: OfflinePage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: OfflinePage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: ServerURIPage.path,
          name: ServerURIPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: ServerURIPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: SyncPage.path,
          name: SyncPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: SyncPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: StoriesPage.path,
          name: StoriesPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: StoriesPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: AuthenticatePage.path,
          name: AuthenticatePage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: AuthenticatePage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: EditorPage.path,
          name: 'NewStory',
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: EditorPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: '${EditorPage.path}/:sid',
          name: EditorPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: AppTheme(
              child: EditorPage(id: int.parse(state.params['sid']!)),
            ),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: '${ReaderPage.path}/:sid',
          name: ReaderPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: AppTheme(
              child: ReaderPage(id: int.parse(state.params['sid']!)),
            ),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: SettingsPage.path,
          name: SettingsPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: SettingsPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: VocabImportPage.path,
          name: VocabImportPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: VocabImportPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: VocabularyPage.path,
          name: VocabularyPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: VocabularyPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
        GoRoute(
          path: WordPage.path,
          name: WordPage.name,
          pageBuilder: (context, state) {
            if (state.queryParams['text'] == null) {
              //throw Exception("WordPage can't be launched without text");

            }
            return CustomTransitionPage<void>(
              key: state.pageKey,
              child: AppTheme(
                child: WordPage(
                  from: state.queryParams['from'],
                  text: state.queryParams['text'] ?? 'आता',
                ),
              ),
              transitionsBuilder: transitionBuilder,
            );
          },
        ),
        GoRoute(
          path: LearnPage.path,
          name: LearnPage.name,
          pageBuilder: (context, state) {
            return CustomTransitionPage<void>(
              key: state.pageKey,
              child: const AppTheme(child: AppTheme(child: LearnPage())),
              transitionsBuilder: transitionBuilder,
            );
          },
        ),
        GoRoute(
          path: ImageHandlerPage.path,
          name: ImageHandlerPage.name,
          pageBuilder: (context, state) {
            return CustomTransitionPage<void>(
              key: state.pageKey,
              child: AppTheme(
                child: AppTheme(
                  child: ImageHandlerPage(
                    pushedFrom: state.queryParams['from'],
                  ),
                ),
              ),
              transitionsBuilder: transitionBuilder,
            );
          },
        ),
        GoRoute(
          path: '${OCRPage.readerPath}/:sid',
          name: OCRPage.reader,
          pageBuilder: (context, state) {
            return CustomTransitionPage<void>(
              key: state.pageKey,
              child: AppTheme(
                child: AppTheme(
                  child: OCRPage(
                    key: const ValueKey('${OCRPage.readerPath}/:sid'),
                    pushedFrom: state.queryParams['from'],
                    ocrImageIndex: int.parse(state.params['sid']!),
                    edit: false,
                  ),
                ),
              ),
              transitionsBuilder: transitionBuilder,
            );
          },
        ),
        GoRoute(
          path: '${OCRPage.editorPath}/:sid',
          name: OCRPage.editor,
          pageBuilder: (context, state) {
            return CustomTransitionPage<void>(
              key: state.pageKey,
              child: AppTheme(
                child: AppTheme(
                  child: OCRPage(
                    key: const ValueKey('${OCRPage.editorPath}/:sid'),
                    pushedFrom: state.queryParams['from'],
                    ocrImageIndex: int.parse(state.params['sid']!),
                    edit: true,
                  ),
                ),
              ),
              transitionsBuilder: transitionBuilder,
            );
          },
        ),
        GoRoute(
          path: LogOutUserPage.path,
          name: LogOutUserPage.name,
          pageBuilder: (context, state) => CustomTransitionPage<void>(
            key: state.pageKey,
            child: const AppTheme(child: LogOutUserPage()),
            transitionsBuilder: transitionBuilder,
          ),
        ),
      ],
      redirect: (context, state) async {
        final result = await redirector(state);
        if (result != null) {
          _infoLogger('redirecting to $result');
        }
        return result;
      },
    );

    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      //routeInformationProvider: _router.routeInformationProvider,
      //routeInformationParser: _router.routeInformationParser,
      routerConfig: _router,
      title: Constants.appName,
    );
  }

  Future<String?> redirector(GoRouterState state) async {
    printGoRouterState(state);
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );

    final auth = ref.watch(authNotifierProvider);
    final sharedImageAvailable = ref.watch(watchSharedImage);
    _infoLogger('auth: $auth ');
    // If Auth is not set, we need to point to server selection
    if (auth?.server == null) {
      return state.subloc == ServerURIPage.path ? null : ServerURIPage.path;
    } else if (state.subloc == ServerURIPage.path) {
      return null;
    }

    // if the user is not logged in, they need to login
    final loggedIn = auth != null && auth.user != null;

    final loggingIn = state.subloc == AuthenticatePage.path;
    if (!loggedIn) {
      return loggingIn ? null : AuthenticatePage.path;
    }

    // if the user is logged in but still on the login page, send them to
    // the home page
    if (loggingIn) {
      return state.queryParams['from'] ?? '/';
    }
    if (sharedImageAvailable) {
      return (state.subloc == ImageHandlerPage.path)
          ? null
          : ImageHandlerPage.path;
    }
    if (state.subloc == '/' || state.subloc == '') {
      // TO DO: while debugging, open stories directly
      return isConnected ? SyncPage.path : StoriesPage.path;
      //return StoriesPage.path;
    }
    if (!isConnected) {
      if ([
        VocabImportPage.path,
      ].contains(state.subloc)) {
        return OfflinePage.path;
      }
    }
    // if (state.subloc == '/') return '${VocabularyPage.path}';
    //if (state.subloc == '/') return ReceiveImagePage.path;
    // no need to redirect at all
    return null;
  }
}

void printGoRouterState(GoRouterState state) {
  _infoLogger(' state: ${state.extra} ${state.error} ${state.fullpath}'
      ' ${state.location} ${state.name} ${state.pageKey} ${state.params} '
      '${state.queryParams} ${state.path} ${state.subloc}');
}

bool _disableInfoLogger = true;
void _infoLogger(String msg) {
  if (!_disableInfoLogger) {
    logger.i(msg);
  }
}
