import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:dart_hocr/dart_hocr.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:hocr_editor/hocr_editor.dart';

import '../../recieve_shared/providers/image_bucket.dart';

import '../editor/editor_page.dart';
import 'ocr_handler.dart';
import 'user_preferences.dart';

class OCRMain extends ConsumerStatefulWidget {
  const OCRMain({
    required this.pushedFrom,
    required this.ocrImage,
    required this.langPref,
    required this.edit,
    super.key,
  });
  final bool edit;
  final String? pushedFrom;
  final OCRImage ocrImage;
  final LangPref langPref;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => OCRMainState();
}

class OCRMainState extends ConsumerState<OCRMain> with OCRHandler {
  bool isSaving = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        HOCREditor(
          ocrImage: widget.ocrImage,
          ocrViewController: HOCREditingController(
            edit: widget.edit,
            onDiscardImage: onDiscardImage,
            onCopyToClipboard: (text) => onCopyToClipboard(context, text),
            onDictLookup: (text, {required lang}) => onDictLookup(
              context,
              text,
              sL: lang,
              tL: widget.langPref.nativeLanguage,
            ),
            onSaveImage: widget.edit
                ? (img, {asStory = false}) async {
                    setState(() {
                      isSaving = true;
                    });

                    final savedImage = await onSaveImage(img);
                    setState(() {
                      isSaving = false;
                    });
                    return savedImage;
                  }
                : null,
          ),
        ),
        if (isSaving)
          Scaffold(
            backgroundColor: Colors.black.withAlpha(100),
            body: const Center(child: CircularProgressIndicator()),
          )
      ],
    );
  }

  void onDiscardImage() {
    if (widget.pushedFrom != null) {
      context.pop();
    } else {
      final imageBucket = ref.watch(imageBucketProvider);
      final externalImagePath =
          imageBucket.isNotEmpty() ? imageBucket.imagePathList[0] : null;

      if (externalImagePath != null) {
        ref.read(imageBucketProvider.notifier).removeImage();
      }
    }
  }

  Future<OCRImage?> onSaveImage(OCRImage? ocrImage) async {
    if (ocrImage == null) {
      return ocrImage;
    }

    final img2Store = await showDialog<OCRImage>(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
        content: OCRUserPreference(ocrImage: ocrImage),
        backgroundColor: Colors.transparent,
      ),
    );
    if (img2Store != null) {
      final lang = img2Store.lang1;
      try {
        final story = OCRImageStoryIO.toStory(img2Store);
        final storyWithID =
            await ref.read(storiesNotifierProvider.notifier).insertStory(story);
        await ref.read(textStringsNotifierProvider(lang).notifier).clear();
        await ref.read(textStringsNotifierProvider(lang).notifier).onRefresh();
        if (img2Store.edittable) {
          return OCRImageStoryIO.fromStory(storyWithID);
        }
        if (widget.pushedFrom != null) {
          final result = await showOkCancelAlertDialog(
            context: context,
            message:
                'Stroy is now editable in text editor. Do you want to open text editor?',
            okLabel: 'Open Text Editor',
            cancelLabel: 'Goto Story view',
          );

          if (result == OkCancelResult.ok) {
            if (mounted) {
              context.pop();
            }
            openTextEditor(storyWithID.id);
          } else {
            if (mounted) {
              context.pop();
            }
          }
        } else {
          onDiscardImage();
        }

        return img2Store;
        //
      } on Exception catch (e) {
        if (mounted) {
          ReportError.snackBar(
            context,
            errorString: 'Unable to save the story',
            details: e.toString().replaceAll('Exception:', '').trim(),
            onClipboardCopy: (text) async => ClipboardManager.copy(text),
          );
        }
        return null;
      }
    }
    return null;
  }

  void openTextEditor(int storyId) {
    context.pushNamed(EditorPage.name, params: {'sid': storyId.toString()});
  }
}
