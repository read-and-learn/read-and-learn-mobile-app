import 'package:custom_widgets/custom_widgets.dart';
import 'package:dart_hocr/dart_hocr.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class OCRUserPreference extends StatefulWidget {
  const OCRUserPreference({
    required this.ocrImage,
    Key? key,
  }) : super(key: key);
  final OCRImage ocrImage;

  @override
  State<OCRUserPreference> createState() => _OCRUserPreferenceState();
}

class _OCRUserPreferenceState extends State<OCRUserPreference> {
  late bool saveImage;
  bool asStory = true;
  late final bool hasNoImage;

  late final TextEditingController textEditingController;
  late final FocusNode focusNode;
  @override
  void initState() {
    hasNoImage = widget.ocrImage.image != null;
    saveImage = hasNoImage;
    focusNode = FocusNode();
    textEditingController = TextEditingController(
      text: widget.ocrImage.title ?? OCRImageStoryIO.defaultImageName,
    );
    textEditingController.selection = TextSelection(
      baseOffset: 0,
      extentOffset: textEditingController.text.length,
    );
    super.initState();
    focusNode.requestFocus();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      decoration: BoxDecoration(
        color: Theme.of(context).selectedRowColor,
        //color: const Color.fromARGB(255, 255, 0, 0),
        borderRadius: BorderRadius.circular(CLFullscreenBox.borderRadius),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CustomTextField(
            labelText: 'Give a Title',
            controller: textEditingController,
            focusNode: focusNode,
            showLabel: true,
          ),
          TextButton.icon(
            onPressed: () {
              setState(() {
                asStory = !asStory;
              });
            },
            icon: Icon(
              !asStory
                  ? MdiIcons.checkboxMarked
                  : MdiIcons.checkboxBlankOutline,
            ),
            label: const Text('Preserve the layout?'),
          ),
          const SizedBox(height: 8),
          if (hasNoImage && !asStory) ...[
            TextButton.icon(
              onPressed: () {
                setState(() {
                  saveImage = !saveImage;
                });
              },
              icon: Icon(
                saveImage
                    ? MdiIcons.checkboxMarked
                    : MdiIcons.checkboxBlankOutline,
              ),
              label: const Text('preserve the image?'),
            ),
            const SizedBox(height: 8)
          ],
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context, null);
                },
                child: const Text('Cancel'),
              ),
              ElevatedButton(
                onPressed: () async {
                  final image = widget.ocrImage.image;

                  if (mounted) {
                    Navigator.pop(
                      context,
                      widget.ocrImage.copyWith(
                        title: textEditingController.text,
                        edittable: !asStory,
                        image: (image != null && saveImage) ? image : null,
                      ),
                    );
                  }
                },
                child: const Text('save'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
