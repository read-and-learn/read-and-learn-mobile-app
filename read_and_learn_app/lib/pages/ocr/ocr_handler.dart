import 'package:basic_services/basic_services.dart';
import 'package:flutter/material.dart';
import '../../utils/text_cleanup.dart';

mixin OCRHandler {
  Future<void> onCopyToClipboard(
    BuildContext context,
    String text_,
  ) async {
    var text = text_;
    // if it is single word, don't include
    // any puchtuations
    if (!text_.contains(' ')) {
      text = text_.symbolTrim;
    }
    final snackBar = SnackBar(
      duration: const Duration(seconds: 1),
      elevation: 0,
      content: Text(
        ' ${text.summary} is copied into clipboard',
        maxLines: 1,
        textAlign: TextAlign.center,
      ),
    );
    await ClipboardManager.copy(text).then(
      (value) => ScaffoldMessenger.of(context).showSnackBar(
        snackBar,
      ),
    );
  }

  Future<void> onDictLookup(
    BuildContext context,
    String text_, {
    required String sL,
    required String tL,
  }) async {
    var text = text_;
    // if it is single word, don't include
    // any puchtuations
    if (!text_.contains(' ')) {
      text = text_.symbolTrim;
    }
    await DictionaryManager.getDictionaryManager(text: text, sL: sL, tL: tL)
        .launch(context);
  }
}
