import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common/widgets/error_view.dart';
import '../common/widgets/loading_view.dart';
import 'ocr_main.dart';

class OCRPage extends ConsumerWidget {
  const OCRPage({
    required this.ocrImageIndex,
    required this.edit,
    this.pushedFrom,
    super.key,
  });
  static const reader = 'read-ocrimage';
  static const editor = 'edit-ocrimage';
  static const String readerPath = '/$reader';
  static const String editorPath = '/$editor';
  final int? ocrImageIndex;
  final String? pushedFrom;
  final bool edit;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (ocrImageIndex == null) {
      return const ErrorView(
        errorMessage: 'No Image Provided',
      );
    }
    final storyProvider = ref.watch(storyNotifierProvider(ocrImageIndex));
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));

    return storyProvider.when(
      loading: () => const LoadingView(
        title: 'Read and Learn',
      ),
      error: (err, _) => ErrorView(
        errorMessage: err.toString(),
      ),
      data: (story) {
        try {
          return langPrefProvider.maybeWhen(
            orElse: () => const LoadingView(),
            data: (pref) {
              return OCRMain(
                pushedFrom: pushedFrom,
                ocrImage: OCRImageStoryIO.fromStory(story!),
                langPref: LangPref.fromJson(pref.value),
                edit: edit,
              );
            },
          );
        } on Exception catch (e) {
          return ErrorView(
            errorMessage: e.toString(),
          );
        }
      },
    );
  }
}
