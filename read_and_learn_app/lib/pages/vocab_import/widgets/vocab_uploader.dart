import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:language_picker/language_picker.dart';
import 'package:language_picker/languages.dart';

class VocabUploader extends ConsumerStatefulWidget {
  const VocabUploader({
    required this.csvContent,
    required this.onUpload,
    required this.onCancel,
    Key? key,
  }) : super(key: key);
  final CsvContent csvContent;
  final Function(CsvContent csvContent) onUpload;
  final Function() onCancel;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _VocabUploaderState();
}

class _VocabUploaderState extends ConsumerState<VocabUploader> {
  late List<String?> langs;
  @override
  void initState() {
    langs = widget.csvContent.langs;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textStringsProvider =
        ref.watch(textStringsNotifierProvider(langs[0]));

    return textStringsProvider.maybeWhen(
      error: (err, _) =>
          Text('download failed before upload, lang: ${langs[0]}, error: $err'),
      orElse: () => SingleChildScrollView(
        child: Column(
          children: [
            Text(
              'Found ${widget.csvContent.content.length} items to import',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            Text(
              'Select Language',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(
              height: 8,
            ),
            Table(
              border: TableBorder.all(),
              children: [
                row(
                  widget.csvContent.header
                      .map(
                        (e) => Text(
                          e.trim(),
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      )
                      .toList(),
                ),
                row(
                  widget.csvContent.header.map((e) {
                    final index = widget.csvContent.header.indexOf(e);
                    return textStringsProvider.maybeWhen(
                      loading: () => const SizedBox(
                        height: 16,
                        child: Center(
                          child: CircularProgressIndicator.adaptive(),
                        ),
                      ),
                      orElse: () => Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          TextButton(
                            onPressed: () async {
                              await showDialog(
                                barrierColor: Colors.transparent,
                                context: context,
                                builder: (context) => LanguagePickerDialog(
                                  isDividerEnabled: true,
                                  titlePadding: const EdgeInsets.all(8),
                                  searchInputDecoration: const InputDecoration(
                                    hintText: 'Search...',
                                  ),
                                  isSearchable: true,
                                  title: const Text(
                                    'Select language',
                                  ),
                                  onValuePicked: (lang) {
                                    setState(() {
                                      langs[index] = lang.isoCode;
                                    });
                                  },
                                  itemBuilder: (lang) => Text(lang.name),
                                ),
                              );
                            },
                            child: Column(
                              children: [
                                Text.rich(
                                  TextSpan(
                                    children: [
                                      if (langs[index] != null)
                                        TextSpan(
                                          text: Language.fromIsoCode(
                                            langs[index]!,
                                          ).name,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodySmall,
                                        ),
                                      if (langs[index] != null)
                                        const TextSpan(text: ' (Change?)')
                                      else
                                        const TextSpan(
                                          text: '(Select Language)',
                                        ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 8),
                        ],
                      ),
                    );
                  }).toList(),
                )
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            if (langs.any((e) => e == null))
              Text(
                'Select languages before uploading',
                style: Theme.of(context)
                    .textTheme
                    .bodySmall!
                    .copyWith(color: Theme.of(context).errorColor),
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  onPressed: widget.onCancel,
                  child: const Text('Cancel'),
                ),
                ElevatedButton(
                  onPressed: langs.every(
                    (element) => element == null,
                  )
                      ? null
                      : textStringsProvider.whenOrNull(
                          data: (data) => () {
                            widget.onUpload(
                              widget.csvContent.copyWith(langs: langs),
                            );
                          },
                        ),
                  child: const Text('Upload'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  TableRow row(List<Widget> cols) {
    return TableRow(
      children: [
        for (var item in cols)
          TableCell(
            child: Center(
              child: item,
            ),
          )
      ],
    );
  }
}
