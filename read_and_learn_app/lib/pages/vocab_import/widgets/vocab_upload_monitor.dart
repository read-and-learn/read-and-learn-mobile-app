import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../stories/stories_page.dart';

class VocabUploadMonitor extends ConsumerWidget {
  const VocabUploadMonitor({
    required this.csvContent,
    required this.onCancel,
    Key? key,
  }) : super(key: key);
  final CsvContent csvContent;
  final Function() onCancel;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final textStringsProvider =
        ref.watch(textStringsNotifierProvider(csvContent.langs[0]));

    return textStringsProvider.when(
      data: (data) => Column(
        children: [
          Text(
            'upload Completed ',
            style: Theme.of(context).textTheme.bodySmall,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () {
                  context.goNamed(StoriesPage.name);
                },
                child: const Text('Go to Stories'),
              ),
              ElevatedButton(
                onPressed: onCancel,
                child: const Text('Import another'),
              ),
            ],
          )
        ],
      ),
      error: (err, _) =>
          Text('upload failed, lang: ${csvContent.langs[0]!}, error: $err'),
      loading: () => const Center(child: LinearProgressIndicator()),
    );
  }
}
