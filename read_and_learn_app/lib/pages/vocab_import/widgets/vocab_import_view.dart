// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../common/widgets/drawer_widget.dart';

import '../../common/widgets/view_wrapper.dart';
import 'file_selector.dart';

import 'vocab_upload_monitor.dart';
import 'vocab_uploader.dart';

class VocabImportView extends ConsumerWidget {
  const VocabImportView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vocabImportState = ref.watch(vocabImportNotifierProvider);
    return ViewWrapper(
      title: 'Import Vocabulary',
      drawer: const AppDrawer(),
      body: Container(
        alignment: Alignment.topLeft,
        // height: 200.0,
        width: double.infinity,

        padding: const EdgeInsets.all(24),
        child: StepBuilder(
          steps: [
            StepContent(
              title: 'Select a File',
              subtitle: (vocabImportState.errMessage == null)
                  ? null
                  : Text(
                      vocabImportState.errMessage ?? '',
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(color: Colors.red.shade300),
                    ),
              content: FileSelector(
                onFileSelect: (fileContent) => ref
                    .read(vocabImportNotifierProvider.notifier)
                    .fileContent = fileContent,
                onError: (errMessage) => ref
                    .read(vocabImportNotifierProvider.notifier)
                    .errMessage = errMessage,
              ),
            ),
            StepContent(
              title: 'Confirm Upload',
              content: vocabImportState.fileContent == null
                  ? const Text('Unexpected State1, contact developer')
                  : VocabUploader(
                      csvContent: vocabImportState.fileContent!,
                      onUpload: (uploadContent) {
                        ref
                            .read(vocabImportNotifierProvider.notifier)
                            .uploadContent = uploadContent;
                        ref
                            .read(
                              textStringsNotifierProvider(
                                uploadContent.langs[0],
                              ).notifier,
                            )
                            .addTextStringsfromCSV(uploadContent);
                      },
                      onCancel: () => ref
                          .read(vocabImportNotifierProvider.notifier)
                          .clear(),
                    ),
            ),
            StepContent(
              title: 'Upload Status',
              content: vocabImportState.uploadContent == null
                  ? const Text('Unexpected State2, contact developer')
                  : VocabUploadMonitor(
                      csvContent: vocabImportState.uploadContent!,
                      onCancel: () => ref
                          .read(vocabImportNotifierProvider.notifier)
                          .clear(),
                    ),
            ),
          ],
          currStep: vocabImportState.currStep,
          useDefaultButtons: false,
        ),
      ),
    );
  }
}
