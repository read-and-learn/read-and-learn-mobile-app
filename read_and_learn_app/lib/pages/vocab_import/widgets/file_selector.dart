import 'dart:async';
import 'dart:convert';

import 'package:csv/csv.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../../file_io.dart';

class FileSelector extends ConsumerStatefulWidget {
  const FileSelector({
    required this.onFileSelect,
    required this.onError,
    Key? key,
  }) : super(key: key);
  final Function(CsvContent csvContent) onFileSelect;
  final Function(String error) onError;

  @override
  ConsumerState<FileSelector> createState() => _FileSelectorState();
}

class _FileSelectorState extends ConsumerState<FileSelector> {
  late String? foreignLang;
  late String? nativeLanguage;

  bool processing = false;
  @override
  Widget build(BuildContext context) {
    ref.watch(prefNotifierProvider('lang_pref')).whenOrNull(
      data: (data) {
        final langPref = LangPref.fromJson(data.value);
        foreignLang = langPref.currLearningLang;
        nativeLanguage = langPref.nativeLanguage;
      },
    );
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(child: FittedBox(child: Icon(MdiIcons.help))),
              Expanded(child: helpText())
            ],
          ),
          if (kIsWeb)
            Text(
              'Import not suppported in Web, Use the app for Android/iOS/MacOS',
              style: Theme.of(context)
                  .textTheme
                  .bodySmall!
                  .copyWith(color: Theme.of(context).disabledColor),
            )
          else
            processing
                ? const CircularProgressIndicator()
                : ElevatedButton.icon(
                    onPressed: () async {
                      setState(() {
                        processing = true;
                      });
                      await pickFile();
                      setState(() {
                        processing = false;
                      });
                    },
                    icon: const Icon(MdiIcons.fileDownloadOutline),
                    label: const Text('Select a CSV file'),
                  )
        ],
      ),
    );
  }

  Widget helpText() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Store the list of words in a csv file with one word per line.'),
        Text('The column separator is comma(,).'),
        Text('Currently supported fields are meaning, usage.'),
        Text('If any unsupported column is found, it will be ignored.'),
      ],
    );
  }

  Future<void> pickFile() async {
    try {
      final filePath = await CSVFilePicker.pickFile();

      if (filePath == null) {
        return;
      }
      final csvContent = await fromFile(
        filePath: filePath,
        foreignLang: foreignLang,
        nativeLanguage: nativeLanguage,
      );
      await widget.onFileSelect(csvContent);
    } on Exception catch (e) {
      widget.onError(e.toString());
    }
  }

  static Future<CsvContent> fromFile({
    required String filePath,
    List<String>? header,
    String? foreignLang,
    String? nativeLanguage,
  }) async {
    final replaceLineEnd = StreamTransformer<String, String>.fromHandlers(
      handleData: (event, output) {
        final modified = event.replaceAll('\r\n', '\n').replaceAll('\r', '\n');
        output.add(modified);
      },
    );
    final Stream<List<int>> input;

    input = FileIOHandler(filename: filePath).read();

    final wordlistStream =
        input.transform(utf8.decoder).transform(replaceLineEnd).transform(
              const CsvToListConverter(
                eol: '\n',
              ),
            );
    final content = await wordlistStream
        .map(
          (event) => event.map((element) {
            return (element as String).trim();
          }).toList(),
        )
        .toList();
    header ??= content.removeAt(0);

    if (content.isEmpty) {
      throw Exception('Nothing to import');
    }
    if (header[0] != 'word') {
      throw Exception('First column in the CSV is not word');
    }
    if (!header.any(
      (element) =>
          (element.trim() == attr2String[Attribute.meaning]) ||
          (element.trim() == attr2String[Attribute.meaning]),
    )) {
      throw Exception('No importable columns found');
    }
    for (final col in header) {
      if (![
        'word',
        attr2String[Attribute.meaning],
        attr2String[Attribute.usage]
      ].contains(col)) {
        throw Exception('Unexpected column $col found');
      }
    }
    final langs = header
        .map(
          (e) => ['word', attr2String[Attribute.usage]].contains(e)
              ? foreignLang
              : nativeLanguage,
        )
        .toList();

    final csvContent =
        CsvContent(header: header, content: content, langs: langs);

    return csvContent;
  }
}
