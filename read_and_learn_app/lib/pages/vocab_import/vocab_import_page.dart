import 'package:flutter/material.dart';

import 'widgets/vocab_import_view.dart';

class VocabImportPage extends StatefulWidget {
  const VocabImportPage({Key? key}) : super(key: key);
  static const name = 'vacab-import';
  static const String path = '/$name';

  @override
  VocabImportPageState createState() => VocabImportPageState();
}

class VocabImportPageState extends State<VocabImportPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const VocabImportView();
  }
}
