import 'dart:convert';

import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'panel/panel_main.dart';
import 'reader_layer.dart';

class ReaderView extends ConsumerWidget {
  const ReaderView({
    required this.story,
    Key? key,
  }) : super(key: key);
  final Story story;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final minFontSize =
        jsonDecode(Pref.defaultPrefList['min_font_size'] as String) as double;
    final maxFontSize =
        jsonDecode(Pref.defaultPrefList['max_font_size'] as String) as double;
    final defautlFontSize =
        jsonDecode(Pref.defaultPrefList['reader_font_size'] as String)
            as double;

    final fontSize =
        ref.watch(prefNotifierProvider('reader_font_size')).maybeWhen(
              data: (fontSize) => jsonDecode(fontSize.value) as double,
              orElse: () =>
                  jsonDecode(Pref.defaultPrefList['reader_font_size'] as String)
                      as double,
            );
    return SafeArea(
      top: false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.surface,
          elevation: 1,
          title: Text(story.title),
          actions: [
            const Spacer(),
            IconButton(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              onPressed: () {
                if (fontSize > minFontSize) {
                  ref
                      .read(prefNotifierProvider('reader_font_size').notifier)
                      .set((fontSize - 1).toString());
                }
              },
              icon: const Icon(MdiIcons.formatFontSizeDecrease),
            ),
            IconButton(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              onPressed: () {
                ref
                    .read(prefNotifierProvider('reader_font_size').notifier)
                    .set(defautlFontSize.toString());
              },
              icon: const Icon(MdiIcons.formatFont),
            ),
            IconButton(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              onPressed: () {
                if (fontSize < maxFontSize) {
                  ref
                      .read(prefNotifierProvider('reader_font_size').notifier)
                      .set((fontSize + 1).toString());
                }
              },
              icon: const Icon(MdiIcons.formatFontSizeIncrease),
            ),
          ],
        ),
        body: Stack(
          children: [
            ReaderLayer(story),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: PanelMain(
                story: story,
              ),
            )
          ],
        ),
      ),
    );
  }
}
