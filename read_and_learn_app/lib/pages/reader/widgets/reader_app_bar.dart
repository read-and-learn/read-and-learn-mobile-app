import 'dart:convert';

import 'package:data_storage/data_storage.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../settings/widgets/dark_mode_switch.dart';

class ReaderAppBar extends ConsumerWidget {
  const ReaderAppBar({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final minFontSize =
        jsonDecode(Pref.defaultPrefList['min_font_size'] as String);
    final maxFontSize =
        jsonDecode(Pref.defaultPrefList['max_font_size'] as String);

    final defautlFontSize =
        jsonDecode(Pref.defaultPrefList['reader_font_size'] as String);

    final fontSize = ref
        .watch(prefNotifierProvider('reader_font_size'))
        .maybeWhen(
          data: (fontSize) => jsonDecode(fontSize.value),
          orElse: () =>
              jsonDecode(Pref.defaultPrefList['reader_font_size'] as String),
        );

    return Padding(
      padding: const EdgeInsets.only(bottom: 4, right: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          if (!kIsWeb)
            IconButton(
              onPressed: () {
                context.pop();
              },
              icon: const Icon(MdiIcons.arrowLeft),
            ),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              IconButton(
                onPressed: () {
                  if ((fontSize as double) > (minFontSize as double)) {
                    ref
                        .read(
                          prefNotifierProvider('reader_font_size').notifier,
                        )
                        .set((fontSize - 1).toString());
                  }
                },
                icon: const Icon(MdiIcons.formatFontSizeDecrease),
              ),
              IconButton(
                onPressed: () {
                  ref
                      .read(prefNotifierProvider('reader_font_size').notifier)
                      .set(defautlFontSize.toString());
                },
                icon: const Icon(MdiIcons.formatFont),
              ),
              IconButton(
                onPressed: () {
                  if ((fontSize as double) < (maxFontSize as double)) {
                    ref
                        .read(
                          prefNotifierProvider('reader_font_size').notifier,
                        )
                        .set((fontSize + 1).toString());
                  }
                },
                icon: const Icon(MdiIcons.formatFontSizeIncrease),
              ),
            ],
          ),
          const DarkModeModifier2(),
        ],
      ),
    );
  }
}
