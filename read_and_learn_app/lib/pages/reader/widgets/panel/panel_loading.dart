import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:info_cards/info_cards.dart';

import '../../../../constants.dart';
import 'cool_swiper.dart';

class PanelLoading extends StatelessWidget {
  const PanelLoading({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const cardsHeight = Constants.panelHeightOpenedinPercentage;
    return ProviderScope(
      overrides: [
        panelStateProvider.overrideWithValue(
          PanelState(
            textString: TextString(
              text: 'not set yet',
              lang: PanelState.placeholderLang,
            ),
            panelHeight: PanelHeight(
              whenOpen: Constants.panelHeightOpenedinPercentage,
              whenClosed: Constants.panelHeightClosedinPercentage,
            ),
          ),
        ),
      ],
      child: CoolSwiper(
        children: [
          for (var i = 0; i < 2; i++)
            const WaitingCard(cardsHeight: cardsHeight),
        ],
      ),
    );
  }
}
