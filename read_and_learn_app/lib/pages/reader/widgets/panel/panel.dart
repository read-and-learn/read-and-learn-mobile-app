import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:info_cards/info_cards.dart';

import 'audio_player.dart';
import 'cool_swiper.dart';

class Panel extends ConsumerWidget {
  const Panel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final panelState = ref.watch(panelStateProvider);

    return CoolSwiper(
      children: [
        MeaningCard(
          key: ValueKey('meaningCard${panelState.word}'),
          audioPlayer: const AudioPlayerWord(),
        ),
        if (panelState.hasSentence)
          const SentenceCard(audioPlayer: AudioPlayerSentence())
      ],
    );
  }
}

class AudioPlayerWord extends ConsumerWidget {
  const AudioPlayerWord({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final panelState = ref.watch(panelStateProvider);
    return AudioPlayer(
      key: ValueKey(panelState.word),
      word: panelState.word,
      lang: panelState.sourceLang,
    );
  }
}

class AudioPlayerSentence extends ConsumerWidget {
  const AudioPlayerSentence({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final panelState = ref.watch(panelStateProvider);
    return AudioPlayer(
      key: ValueKey(panelState.sentence),
      word: panelState.sentence,
      lang: panelState.sourceLang,
    );
  }
}
