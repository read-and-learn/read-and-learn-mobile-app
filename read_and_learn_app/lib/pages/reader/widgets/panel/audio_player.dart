import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:soundpool/soundpool.dart';

import '../../../settings/widgets/audio_settings.dart';

class AudioPlayer extends ConsumerWidget {
  const AudioPlayer({
    required this.word,
    required this.lang,
    super.key,
  });

  final String word;
  final String lang;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final ttsAudioAsync =
        ref.watch(ttsAudioProvider(QueryWord(word: word, lang: lang)));

    if (!kIsWeb) {
      return ttsAudioAsync.maybeWhen(
        orElse: () => const CircularProgressIndicator(),
        data: (ttsAudio) {
          if (ttsAudio == null) {
            return const Icon(MdiIcons.wifiOff);
          }
          return LayoutBuilder(
            builder: (context, constraints) => SoundpoolInitializer(
              ttsAudio: ttsAudio,
            ),
          );
        },
      );
    } else {
      return const Icon(MdiIcons.wifiOff);
    }
  }
}

class SoundpoolInitializer extends StatefulWidget {
  const SoundpoolInitializer({
    required this.ttsAudio,
    Key? key,
  }) : super(key: key);

  final Uint8List ttsAudio;

  @override
  SoundpoolInitializerState createState() => SoundpoolInitializerState();
}

class SoundpoolInitializerState extends State<SoundpoolInitializer> {
  Soundpool? _pool;
  SoundpoolOptions _soundpoolOptions = SoundpoolOptions.kDefault;

  @override
  void initState() {
    super.initState();
    if (!kIsWeb) {
      _initPool(_soundpoolOptions);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleApp(
      pool: _pool!,
      onOptionsChange: _initPool,
      ttsAudio: widget.ttsAudio,
    );
  }

  void _initPool(SoundpoolOptions soundpoolOptions) {
    _pool?.dispose();
    setState(() {
      _soundpoolOptions = soundpoolOptions;
      _pool = Soundpool.fromOptions(options: _soundpoolOptions);
    });
  }
}

class SimpleApp extends ConsumerStatefulWidget {
  const SimpleApp({
    required this.pool,
    required this.onOptionsChange,
    required this.ttsAudio,
    Key? key,
  }) : super(key: key);
  final Soundpool pool;
  final Uint8List ttsAudio;
  final ValueSetter<SoundpoolOptions> onOptionsChange;

  @override
  ConsumerState createState() => SimpleAppState();
}

class SimpleAppState extends ConsumerState<SimpleApp> {
  late Auth? authData;
  int? playerActiveId;
  Soundpool get _soundpool => widget.pool;
  late Future<int> audioStreamID;
  bool isPlaying = false;
  late AudioPref audioPref;
  @override
  void initState() {
    super.initState();

    audioPref = AudioPref();
    audioStreamID = _soundpool.loadUint8List(widget.ttsAudio);
  }

  @override
  void didUpdateWidget(SimpleApp oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.pool != widget.pool) {
      audioStreamID = _soundpool.loadUint8List(widget.ttsAudio);
    }
  }

  @override
  Widget build(BuildContext context) {
    {
      final audioPrefProvider = ref.watch(prefNotifierProvider('audio_pref'));
      final pref = audioPrefProvider.maybeWhen(
        data: (pref) => pref.value,
        orElse: () => null,
      );
      if (pref != null) {
        audioPref = AudioPref.fromJson(pref);
      }
    }

    return CardMenuItem(
      text: isPlaying ? 'Stop' : 'listen',
      iconData: MdiIcons.playPause,
      onPressed: isPlaying ? _stopSound : _playSound,
      onLongPressed: () async {
        await showModalBottomSheet<void>(
          context: context,
          builder: (context) {
            return const Padding(
              padding: EdgeInsets.only(bottom: 32),
              child: AudioSettings(),
            );
          },
        );
      },
    );
  }

  Future<void> _playSound() async {
    setState(() {
      isPlaying = true;
    });
    final soundID = await audioStreamID;
    await _soundpool.setVolume(
      soundId: soundID,
      volume: audioPref.volume,
    );
    playerActiveId = await _soundpool.play(soundID, rate: audioPref.rate);
    setState(() {
      isPlaying = false;
    });
  }

  Future<void> _stopSound() async {
    if (playerActiveId != null) {
      await _soundpool.stop(playerActiveId!);
      playerActiveId = null;
      setState(() {
        isPlaying = false;
      });
    }
  }
}
