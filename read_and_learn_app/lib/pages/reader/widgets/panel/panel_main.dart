import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../constants.dart';
import 'panel.dart';
import 'panel_error.dart';
import 'panel_inactive.dart';
import 'panel_loading.dart';

class PanelMain extends ConsumerWidget {
  const PanelMain({
    required this.story,
    Key? key,
  }) : super(key: key);
  final Story story;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (!ref.watch(panelActiveStateNotifierProvider) ||
        (story.queryWord == null)) {
      return const PanelInactive();
    }
    final qWord = story.queryWord!;
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final textStringsAsync = ref.watch(textStringsNotifierProvider(qWord.lang));

    return langPrefProvider.when(
      error: (err, _) => PanelError(
        errorMessage: 'Error: ${err.toString()}',
      ),
      loading: () => const PanelLoading(),
      data: (pref) => textStringsAsync.when(
        error: (err, _) => PanelError(
          errorMessage: 'Error: ${err.toString()}',
        ),
        loading: () => const PanelLoading(),
        data: (textStrings) {
          final textString = textStrings?.items[qWord.word];
          if (textString == null) {
            return const PanelInactive();
          }

          return ProviderScope(
            overrides: [
              panelStateProvider.overrideWithValue(
                PanelState(
                  textString: textString,
                  sentenceParts: qWord.sentence,
                  nativeLanguage: LangPref.fromJson(pref.value).nativeLanguage,
                  panelHeight: PanelHeight(
                    whenOpen: Constants.panelHeightOpenedinPercentage,
                    whenClosed: Constants.panelHeightClosedinPercentage,
                  ),
                ),
              ),
            ],
            child: const Panel(),
          );
        },
      ),
    );
  }
}

/*
    return ProviderScope(overrides: [
      
    ], child: Panel(query: qWord!));

*/
