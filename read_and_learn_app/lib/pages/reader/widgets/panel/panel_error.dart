import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:info_cards/info_cards.dart';

import '../../../../constants.dart';
import 'cool_swiper.dart';

class PanelError extends ConsumerWidget {
  const PanelError({
    this.errorMessage,
    Key? key,
  }) : super(key: key);
  final String? errorMessage;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ProviderScope(
      overrides: [
        panelStateProvider.overrideWithValue(
          PanelState(
            textString: TextString(
              text: 'not set yet',
              lang: PanelState.placeholderLang,
            ),
            panelHeight: PanelHeight(
              whenOpen: Constants.panelHeightOpenedinPercentage,
              whenClosed: Constants.panelHeightClosedinPercentage,
            ),
          ),
        ),
      ],
      child: Padding(
        padding: const EdgeInsets.only(bottom: 12),
        child: Center(
          child: CoolSwiper(
            children: [
              ErrorCard(
                child: Text(errorMessage ?? 'Error in Panel creation'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
