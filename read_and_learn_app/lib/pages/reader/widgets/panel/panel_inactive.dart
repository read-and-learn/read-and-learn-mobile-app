import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PanelInactive extends ConsumerWidget {
  const PanelInactive({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Container(
      /* height: ref.watch(
          panelStateProvider.select((value) => value.panelHeight.whenClosed)), */
      alignment: Alignment.center,
      color: Theme.of(context).shadowColor,
      child: Text(
        'Tab on a word to learn more',
        maxLines: 2,
        style: Theme.of(context)
            .textTheme
            .labelLarge!
            .copyWith(color: Theme.of(context).canvasColor),
      ),
    );
  }
}
