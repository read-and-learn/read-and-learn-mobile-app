import 'dart:convert';

import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:text_reader/text_reader.dart';

import '../../../constants.dart';
import '../../../logger.dart';
import 'reader_app_bar.dart';

class ReaderLayer extends ConsumerStatefulWidget {
  const ReaderLayer(this.story, {Key? key}) : super(key: key);
  final Story story;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => ReaderLayerState();
}

class ReaderLayerState extends ConsumerState<ReaderLayer> {
  late ScrollController controller;
  @override
  void initState() {
    controller = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future<bool> onTapText(
    TapDownDetails? tapDownDetails,
    int? textOffset,
    ScrollPosition? scrollPosition,
  ) async {
    logger.d('ReaderReader.onTapText');
    if (textOffset == null) {
      return false;
    }
    ref.read(panelActiveStateNotifierProvider.notifier).enablePanel();
    ref.read(storyNotifierProvider(widget.story.id).notifier).offset =
        textOffset;
    ref.read(storyNotifierProvider(widget.story.id));

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.surface,
      child: Column(
        children: [
          Expanded(
            child: CustomScrollView(
              controller: controller,
              slivers: <Widget>[
                SliverAppBar(
                  backgroundColor: Theme.of(context).colorScheme.surface,
                  automaticallyImplyLeading: false,
                  elevation: 0,
                  title: Text(
                    widget.story.title,
                    style: Theme.of(context).textTheme.displayLarge!.copyWith(
                          fontSize: 4 +
                              ref
                                  .watch(
                                    prefNotifierProvider('reader_font_size'),
                                  )
                                  .maybeWhen(
                                    data: (fontSize) =>
                                        jsonDecode(fontSize.value) as double,
                                    orElse: () => jsonDecode(
                                      Pref.defaultPrefList['reader_font_size']
                                          as String,
                                    ) as double,
                                  ),
                        ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: SimpleTextReader(
                    controller: controller,
                    title: '${widget.story.title} '
                        '(${widget.story.lang})',
                    content: widget.story.tapMarkedContent,
                    onTap: onTapText,
                    appBar: const ReaderAppBar(),
                    isPanelOpen: widget.story.word != null &&
                        widget.story.sentence != null,
                    panelClosedSizeFract:
                        Constants.panelHeightClosedinPercentage,
                    panelOpenSizeFract: Constants.panelHeightOpenedinPercentage,
                    initialFontSize: ref
                        .watch(prefNotifierProvider('reader_font_size'))
                        .maybeWhen(
                          data: (fontSize) =>
                              jsonDecode(fontSize.value) as double,
                          orElse: () => jsonDecode(
                            Pref.defaultPrefList['reader_font_size'] as String,
                          ) as double,
                        ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
