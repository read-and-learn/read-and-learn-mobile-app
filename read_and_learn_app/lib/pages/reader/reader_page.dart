import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common/widgets/error_view.dart';
import '../common/widgets/loading_view.dart';
import 'widgets/reader_view.dart';

class ReaderPage extends ConsumerWidget {
  const ReaderPage({
    required this.id,
    Key? key,
  }) : super(key: key);
  static const name = 'read-story';
  static const String path = '/$name';
  final int id;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final storyProvider = ref.watch(storyNotifierProvider(id));
    return storyProvider.when(
      data: (story) {
        if (story == null) {
          return const ErrorView();
        }
        final textStrings = ref.watch(textStringsNotifierProvider(story.lang));
        return textStrings.when(
          data: (data) => ReaderView(story: story),
          error: (err, _) => const ErrorView(),
          loading: () => const LoadingView(),
        );
      },
      error: (err, _) => const ErrorView(),
      loading: () => const LoadingView(),
    );
  }
}
