import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'widgets/server_uri_view.dart';

class ServerURIPage extends ConsumerWidget {
  const ServerURIPage({Key? key}) : super(key: key);
  static const name = 'server';
  static const String path = '/$name';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final serverURI = ref.watch(authNotifierProvider)?.server;

    return ServerURIView(
      serverURI: serverURI,
    );
  }
}
