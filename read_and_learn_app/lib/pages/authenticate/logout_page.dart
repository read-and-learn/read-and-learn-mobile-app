import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../common/widgets/view_wrapper.dart';

class LogOutUserPage extends ConsumerStatefulWidget {
  const LogOutUserPage({Key? key}) : super(key: key);
  static const name = 'logout-user';
  static const String path = '/$name';

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LogOutUserPageState();
}

class _LogOutUserPageState extends ConsumerState<LogOutUserPage> {
  @override
  Widget build(BuildContext context) {
    return const LogOutUserView();
  }
}

class LogOutUserView extends ConsumerStatefulWidget {
  const LogOutUserView({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LogOutUserViewState();
}

class _LogOutUserViewState extends ConsumerState<LogOutUserView> {
  bool confirmLogout = false;
  bool deleteUser = false;
  @override
  Widget build(BuildContext context) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final auth = ref.watch(authNotifierProvider);
    return ViewWrapper(
      title: 'Log out',
      body: Padding(
        padding: const EdgeInsets.all(8),
        child: Center(
          child: langPrefProvider.when(
            loading: () => const CLLoading(),
            error: (err, stack) => ShowError(
              errorMessage: err.toString(),
            ),
            data: (langPrefString) {
              final langPref = LangPref.fromJson(langPrefString.value);
              final loggedIn = ref.watch(authNotifierProvider) != null;
              return loggedIn
                  ? Column(
                      children: [
                        const Spacer(),
                        ListTile(
                          title: Text.rich(
                            TextSpan(
                              children: [
                                const TextSpan(text: 'You are connected to '),
                                if (auth?.server ==
                                    ref.read(defaultServerURIProvider))
                                  const TextSpan(
                                    text: 'Sandbox server',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  )
                                else
                                  TextSpan(
                                    text:
                                        " server: ${auth?.server ?? "unknown"}",
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                const TextSpan(text: '.')
                              ],
                            ),
                          ),
                        ),
                        const Spacer(),
                        ListTile(
                          trailing: InkWell(
                            onTap: () {
                              setState(() {
                                confirmLogout = !confirmLogout;
                              });
                            },
                            child: Icon(
                              confirmLogout
                                  ? MdiIcons.checkboxMarkedOutline
                                  : MdiIcons.checkboxBlankOutline,
                            ),
                          ),
                          title: const Text(
                              'Logging out from the app will remove the locally'
                              ' stored data and all the preferences.'
                              ' Select if you want to proceed?'),
                          subtitle: const Text(
                              'If you are not syncing with server, you will loose all your data.'
                              ' If you have backed up your stories on the server,'
                              ' it will be downloaded when you log in again.\n'),
                        ),
                        if (confirmLogout) ...[
                          const SizedBox(
                            height: 8,
                          ),
                          ListTile(
                            trailing: InkWell(
                              onTap: () {
                                setState(() {
                                  deleteUser = !deleteUser;
                                });
                              },
                              child: Icon(
                                deleteUser
                                    ? MdiIcons.checkboxMarkedOutline
                                    : MdiIcons.checkboxBlankOutline,
                              ),
                            ),
                            title: const Text(
                              'Select if you also want to delete your account',
                            ),
                            subtitle: const Text(
                              "This will also delete all your stories, but don't remove vocabulary from the server.",
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          ElevatedButton(
                            onPressed: () {
                              logout(langPref, deleteUser: deleteUser);
                            },
                            child: Text(
                              deleteUser
                                  ? 'Yes, delete my account'
                                  : 'Yes, Log me out',
                            ),
                          ),
                        ],
                        const Spacer(),
                      ],
                    )
                  : const Text('Successfully Logged out');
            },
          ),
        ),
      ),
    );
  }

  Future<void> logout(LangPref langPref, {bool deleteUser = false}) async {
    try {
      final isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected),
      );
      await ref.read(storiesNotifierProvider.notifier).clear();

      for (final lang in langPref.learningLanguages) {
        await ref.read(textStringsNotifierProvider(lang).notifier).clear();
      }

      await ref.read(prefNotifierProvider('lang_pref').notifier).clear();
      await ref.read(prefNotifierProvider('reader_font_size').notifier).clear();

      await ref.read(prefNotifierProvider('min_font_size').notifier).clear();
      await ref.read(prefNotifierProvider('max_font_size').notifier).clear();

      await ref.read(prefNotifierProvider('audio_pref').notifier).clear();
      await ref.read(prefNotifierProvider('sync_status').notifier).clear();
      //print("clearing darkMod");
      await ref.read(prefNotifierProvider('dark_mode').notifier).clear();

      await ref
          .read(authNotifierProvider.notifier)
          .logout(isConnected, deleteUser: deleteUser);
    } on Exception catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
  }
}
