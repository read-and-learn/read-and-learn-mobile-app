import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:read_and_learn/pages/exceptions/offline_page.dart';

import 'widgets/auth_view.dart';

class AuthenticatePage extends ConsumerStatefulWidget {
  const AuthenticatePage({Key? key}) : super(key: key);
  static const String name = 'login';
  static const String path = '/$name';

  @override
  ConsumerState<AuthenticatePage> createState() => AuthenticatePageState();
}

class AuthenticatePageState extends ConsumerState<AuthenticatePage> {
  final GlobalKey childKey = GlobalKey();
  bool signUp = true;

  @override
  Widget build(BuildContext context) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    // Move this to router !
    if (!isConnected) {
      return const OfflinePage();
    }

    return CLFullscreenBox(
      child: MediaQuery(
        data: MediaQuery.of(context),
        child: AuthView(
          key: childKey,
        ),
      ),
    );
  }
}
