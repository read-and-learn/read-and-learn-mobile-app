import 'package:data_storage/data_storage.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../server_uri_page.dart';

class AuthenticateCancel extends ConsumerWidget {
  const AuthenticateCancel({Key? key}) : super(key: key);
  static const double padUnit = 8;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final auth = ref.watch(authNotifierProvider);
    return Padding(
      padding: const EdgeInsets.fromLTRB(padUnit, padUnit, padUnit, padUnit),
      child: DottedBorder(
        radius: const Radius.circular(12),
        borderType: BorderType.RRect,
        dashPattern: const [8, 4],
        color: Theme.of(context).colorScheme.surface,
        child: Card(
          //color: Colors.transparent,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          child: Align(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 8, 8, 8),
              child: DefaultTextStyle(
                style: Theme.of(context).textTheme.bodyLarge!,
                child: Text.rich(
                  TextSpan(
                    children: [
                      const TextSpan(
                        text: 'You are trying to login/sign up on \n',
                      ),
                      if (auth?.server == ref.read(defaultServerURIProvider))
                        const TextSpan(
                          text: 'Sandbox server',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      else
                        TextSpan(
                          text: " server: ${auth?.server ?? "unknown"}",
                          style: const TextStyle(fontWeight: FontWeight.bold),
                        ),
                      const TextSpan(
                        text: '. ',
                      ),
                      TextSpan(
                        text: 'Click here',
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            context.goNamed(ServerURIPage.name);
                          },
                        style: TextStyle(color: Colors.blue.shade400),
                      ),
                      const TextSpan(text: ' if you want to change')
                    ],
                  ),
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
