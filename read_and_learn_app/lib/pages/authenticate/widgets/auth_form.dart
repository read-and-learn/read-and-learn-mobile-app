import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'package:read_and_learn/pages/authenticate/widgets/auth_textfield.dart';
import 'package:read_and_learn/pages/authenticate/widgets/authenticate_cancel.dart';
import 'package:read_and_learn/pages/authenticate/widgets/checkbox_formfield.dart';

class AuthForm extends ConsumerStatefulWidget {
  const AuthForm({
    required this.size,
    super.key,
  });
  final Size size;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AuthFormState();
}

class _AuthFormState extends ConsumerState<AuthForm> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minWidth: widget.size.width,
        minHeight: widget.size.height,
      ),
      child: IntrinsicHeight(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor.withAlpha(128),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              LogInForm(),
              AuthenticateCancel(),
            ],
          ),
        ),
      ),
    );
  }
}

class LogInForm extends ConsumerStatefulWidget {
  const LogInForm({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LogInFormState();
}

class _LogInFormState extends ConsumerState<LogInForm> {
  final _formKey = GlobalKey<FormState>();
  static const double padUnit = 8;
  bool signUp = false;
  late final TextEditingController nameController;
  late final TextEditingController passwordController;
  late final TextEditingController confirmPasswordController;

  late final FocusNode nameFocus;
  late final FocusNode passwordFocus;
  late final FocusNode confirmPasswordFocus;
  late final FocusNode checkboxFocus;
  late final FocusNode submitFocus;
  bool privacyPolicyAccepted = false;

  @override
  void initState() {
    nameController = TextEditingController();
    passwordController = TextEditingController();
    confirmPasswordController = TextEditingController();

    nameFocus = FocusNode();
    passwordFocus = FocusNode();
    confirmPasswordFocus = FocusNode();
    checkboxFocus = FocusNode(skipTraversal: true);
    submitFocus = FocusNode();
    WidgetsBinding.instance.addPostFrameCallback(setFocus);
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    nameFocus.dispose();
    passwordFocus.dispose();
    confirmPasswordFocus.dispose();
    checkboxFocus.dispose();
    submitFocus.dispose();

    super.dispose();
  }

  void setFocus(Duration _) {
    // Autofocus USrename not working
    if (nameFocus.canRequestFocus) {
      nameFocus.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(padUnit, padUnit, padUnit, padUnit),
        child: DottedBorder(
          radius: const Radius.circular(12),
          borderType: BorderType.RRect,
          dashPattern: const [8, 4],
          color: Theme.of(context).colorScheme.surface,
          child: Card(
            //color: Colors.transparent,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
            child: Align(
              child: Padding(
                padding: EdgeInsets.zero,
                child: DefaultTextStyle(
                  style: Theme.of(context).textTheme.bodyLarge!,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.fromLTRB(0, padUnit, 0, padUnit),
                        child: Text(
                          title,
                          style: Theme.of(context).textTheme.displayLarge,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(
                          padUnit,
                          padUnit,
                          padUnit,
                          padUnit,
                        ),
                        child: AuthTextFormField(
                          controller: nameController,
                          focusNode: nameFocus,
                          validator: nameValidator,
                          isPassword: false,
                          labelText: 'USER NAME',
                          prefix: const Icon(MdiIcons.account),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {},
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(
                          padUnit,
                          0,
                          padUnit,
                          padUnit,
                        ),
                        child: AuthTextFormField(
                          controller: passwordController,
                          focusNode: passwordFocus,
                          validator: passwordValidator,
                          isPassword: true,
                          labelText: 'PASSWORD',
                          prefix: const Icon(MdiIcons.lock),
                          textInputAction: signUp
                              ? TextInputAction.next
                              : TextInputAction.done,
                          onFieldSubmitted: signUp ? (_) {} : (_) => onSubmit(),
                        ),
                      ),
                      if (signUp) ...[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(
                            padUnit,
                            0,
                            padUnit,
                            padUnit,
                          ),
                          child: AuthTextFormField(
                            controller: confirmPasswordController,
                            focusNode: confirmPasswordFocus,
                            validator: confirmPasswordValidator,
                            isPassword: true,
                            labelText: 'CONFIRM PASSWORD',
                            prefix: const Icon(MdiIcons.lock),
                            textInputAction: TextInputAction.done,
                            onFieldSubmitted: (_) {
                              /* confirmPasswordFocus.unfocus();
                              FocusScope.of(context).requestFocus(submitFocus); */
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(
                            padUnit,
                            0,
                            padUnit,
                            padUnit,
                          ),
                          child: CheckboxFormField(
                            title: Text.rich(
                              TextSpan(
                                children: [
                                  const TextSpan(
                                    text: 'I read and agree with ',
                                  ),
                                  TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        URLManager.getURLManager(
                                          url:
                                              'https://cloudonlanapps.com/readandlearn_privacy_policy',
                                        ).launch(context);
                                      },
                                    text: 'Privacy Policy',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyLarge!
                                        .copyWith(color: Colors.blue.shade400),
                                  ),
                                  const TextSpan(text: ' '),
                                  WidgetSpan(
                                    alignment: PlaceholderAlignment.middle,
                                    child: InkWell(
                                      onTap: () {
                                        URLManager.getURLManager(
                                          url:
                                              'https://cloudonlanapps.com/readandlearn_privacy_policy',
                                        ).launch(context);
                                      },
                                      child: Icon(
                                        MdiIcons.openInNew,
                                        color: Colors.blue.shade400,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                            validator: (value) => privacyPolicyAccepted
                                ? null
                                : 'Please accept Privacy Policy',
                            onChanged: (value) {
                              setState(() {
                                privacyPolicyAccepted =
                                    value ?? privacyPolicyAccepted;
                              });
                            },
                            focusNode: checkboxFocus,
                          ),
                        )
                      ],
                      Padding(
                        padding: const EdgeInsets.fromLTRB(
                          padUnit,
                          0,
                          padUnit,
                          padUnit,
                        ),
                        child: Align(
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Theme.of(context).primaryColor,
                              ),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(20)),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(
                                padUnit,
                                2,
                                padUnit,
                                2,
                              ),
                              child: TextButton(
                                focusNode: submitFocus,
                                onPressed: onSubmit,
                                child: Text(
                                  buttonText,
                                  style: Theme.of(context)
                                      .textTheme
                                      .displayLarge!
                                      .copyWith(
                                        color: Theme.of(context).primaryColor,
                                      ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.fromLTRB(0, padUnit, 0, padUnit),
                        child: Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(text: signUporLoginQ),
                              const TextSpan(text: ' '),
                              TextSpan(
                                text: signUporLoginLink,
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    setState(() {
                                      _formKey.currentState?.reset();
                                      passwordController.clear();
                                      confirmPasswordController.clear();
                                      signUp = !signUp;
                                      nameFocus.requestFocus();
                                    });
                                  },
                                style: const TextStyle()
                                    .copyWith(color: Colors.blue.shade400),
                              )
                            ],
                          ),
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  String get title => signUp ? 'Create Account' : 'Login';
  //String get subtitle => signUp ? "" : "Please sign in to continue.";

  String get signUporLoginQ =>
      signUp ? 'Already Have an account?' : "Don't have an account?";
  String get signUporLoginLink => signUp ? 'Sign In' : 'Sign Up';
  String get buttonText => signUp ? 'Sign Up' : 'Login';

  String? nameValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Provide user name';
    }
    if (value.length < 5) {
      return 'a minimum of 5 characters is required';
    }
    if (value.length > 16) {
      return 'too long for user name. use maximum of 16 characters';
    }
    final userRegExp = RegExp(r'^[A-Za-z]([A-Za-z0-9]){4,15}$');
    if (!userRegExp.hasMatch(value)) {
      return 'first letter must be an alphabet and remaining may contain alphanumeric characters '
          'use A-Za-z0-9 only. Unicode not supported';
    }

    return null;
  }

  String? passwordValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'You must set a password';
    }
    return null;
  }

  String? confirmPasswordValidator(String? value) {
    if (!signUp) {
      return null;
    }
    if (value == null || value.isEmpty) {
      return 'Reenter password to confirm';
    }
    if (confirmPasswordController.text != passwordController.text) {
      return 'password not matching';
    }
    return null;
  }

  void onSubmit() {
    FocusScope.of(context).focusedChild?.unfocus();

    if (_formKey.currentState!.validate()) {
      if (signUp) {
        _signupUser(nameController.text, passwordController.text);
      } else {
        _loginUser(nameController.text, passwordController.text);
      }
    }
  }

  Future<void> _loginUser(String name, String password) async {
    final isConnected = ref.read(connectivityStateNotifierProvider).isConnected;

    final result = await ref
        .read(authNotifierProvider.notifier)
        .login(isConnected: isConnected, name: name, password: password);
    if (result == null) {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Successfully Logged in, redirecting to home page'),
          ),
        );
        setState(() {
          signUp = false;
        });
      }
    } else {
      if (mounted) {
        nameController.clear();
        passwordController.clear();
        ReportError.snackBar(
          context,
          errorString: result,
        );
      }
    }
  }

  Future<void> _signupUser(String name, String password) async {
    final isConnected = ref.read(connectivityStateNotifierProvider).isConnected;

    final result = await ref
        .read(authNotifierProvider.notifier)
        .signup(isConnected: isConnected, name: name, password: password);
    if (result == null) {
      if (mounted) {
        confirmPasswordController.clear();

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Successfully registered. Please login'),
          ),
        );
        setState(() {
          signUp = false;
        });
      }
    } else {
      if (mounted) {
        confirmPasswordController.clear();
        nameController.clear();
        passwordController.clear();
        ReportError.snackBar(
          context,
          errorString: 'Failed to Register',
          details: result,
        );
      }
    }
  }
}
