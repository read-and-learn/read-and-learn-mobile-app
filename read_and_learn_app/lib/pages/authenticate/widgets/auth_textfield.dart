import 'package:flutter/material.dart';

class AuthTextFormField extends StatelessWidget {
  const AuthTextFormField({
    required this.controller,
    required this.focusNode,
    required this.validator,
    required this.isPassword,
    required this.labelText,
    required this.prefix,
    required this.textInputAction,
    required this.onFieldSubmitted,
    super.key,
  });
  final TextEditingController controller;
  final FocusNode focusNode;
  final String? Function(String?) validator;
  final bool isPassword;
  final String labelText;
  final Widget prefix;
  final TextInputAction textInputAction;
  final Function(String? val) onFieldSubmitted;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: isPassword,
      obscuringCharacter: '*',
      controller: controller,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        labelText: labelText,
        errorMaxLines: 3,
        prefixIcon: prefix,
      ),
      validator: validator,
      textInputAction: textInputAction,
      onFieldSubmitted: onFieldSubmitted,
      autocorrect: false,
      enableSuggestions: false,
    );
  }
}
