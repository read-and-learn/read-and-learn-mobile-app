import 'dart:ui';

import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';

import 'package:read_and_learn/pages/authenticate/widgets/auth_form.dart';

class AuthView extends StatefulWidget {
  const AuthView({required GlobalKey key})
      : _key = key,
        super(key: key);
  final GlobalKey _key;

  @override
  State<StatefulWidget> createState() => _AuthViewState();
}

class _AuthViewState extends State<AuthView> {
  Size size = const Size(100, 100);
  bool showForm = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback(_setBoundary);
    super.didChangeDependencies();
  }

  void _setBoundary(Duration _) {
    final parentRenderBox =
        widget._key.currentContext?.findRenderObject() as RenderBox?;

    try {
      if (parentRenderBox == null) {
        throw Exception('unable to get RenderBox');
      }
      final size = parentRenderBox.size;
      setState(() {
        this.size = size;
        showForm = true;
      });
    } on Exception {
      /// This will suppress null error if we can't find RenderObject
      /// This is a software bug !! but let the screen to build witth
      /// some value
      size = CLFullscreenBox.size(context);
      showForm = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: size.height,
        width: size.width,
        child: showForm
            ? Stack(
                children: [
                  SizedBox(
                    height: size.height,
                    width: size.width,
                    child: SingleChildScrollView(
                      child: DecoratedBox(
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                              'assets/icon/Logo Design.002.png',
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: Center(
                          child: AuthForm(
                            size: Size(size.width, size.height),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : const Center(child: CircularProgressIndicator()),
      ),
    );
  }
}

class ImageLogo extends StatelessWidget {
  const ImageLogo({
    Key? key,
  }) : super(key: key);

  static const double sigmaX = 10; // from 0-10
  static const double sigmaY = 0; // from 0-10
  static const double opacity = 1; // from 0-1.0

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: sigmaX),
      child: FittedBox(
        fit: BoxFit.fill,
        child: Card(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          child: Image.asset('assets/icon/Logo Design.002.png'),
        ),
      ),
    );
  }
}
