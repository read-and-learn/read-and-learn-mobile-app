import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ServerLocationEditor extends ConsumerStatefulWidget {
  const ServerLocationEditor({
    required this.serverURI,
    required this.onCancel,
    required this.onOK,
    Key? key,
  }) : super(key: key);
  final String? serverURI;

  final void Function() onCancel;
  final Future<String?> Function(String serverURI) onOK;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ServerLocationEditorState();
}

class _ServerLocationEditorState extends ConsumerState<ServerLocationEditor> {
  late TextEditingController textEditingController;
  bool verifying = false;
  String? addInfo;

  @override
  void initState() {
    textEditingController = TextEditingController(text: widget.serverURI);
    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CustomTextField(
          labelText: 'Server Location',
          controller: textEditingController,
          onChanged: (text) {
            if (addInfo != null) {
              setState(() {
                addInfo = null;
              });
            }
          },
        ),
        const SizedBox(
          height: 8,
        ),
        if (addInfo != null) ...[
          Text(
            addInfo!,
            style: TextStyle(color: Theme.of(context).errorColor),
          ),
          const SizedBox(
            height: 8,
          ),
        ],
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(
              onPressed: verifying ? null : widget.onCancel,
              child: const Text('Cancel'),
            ),
            ElevatedButton(
              onPressed: verifying
                  ? null
                  : () async {
                      setState(() {
                        verifying = true;
                      });
                      final res = await widget.onOK(textEditingController.text);

                      setState(() {
                        addInfo = res;

                        verifying = false;
                      });
                    },
              child: const Text('Ok'),
            ),
          ],
        )
      ],
    );
  }
}
