import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../common/widgets/view_wrapper.dart';
import 'server_uri_main.dart';

class ServerURIView extends ConsumerWidget {
  const ServerURIView({
    required this.serverURI,
    Key? key,
  }) : super(key: key);
  final String? serverURI;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ViewWrapper(
      title: 'Server',
      body: ServerURIMain(serverURI: serverURI),
    );
  }
}
