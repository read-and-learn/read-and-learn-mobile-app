import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../authenticate_page.dart';
import 'server_location_editor.dart';

class ServerURIMain extends ConsumerStatefulWidget {
  const ServerURIMain({
    required this.serverURI,
    Key? key,
  }) : super(key: key);
  final String? serverURI;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _ServerURIMainState();
}

class _ServerURIMainState extends ConsumerState<ServerURIMain> {
  bool enableEditor = false;

  @override
  Widget build(BuildContext context) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final buttonStyle = Theme.of(context).textTheme.displayLarge!.copyWith(
          color: Colors.blue.shade300,
          fontWeight: FontWeight.bold,
        );

    if (enableEditor) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child: ServerLocationEditor(
          serverURI: (widget.serverURI == ref.read(defaultServerURIProvider))
              ? null
              : widget.serverURI,
          onCancel: () => setState(() {
            enableEditor = false;
          }),
          onOK: (newURI) async {
            final status = await RestApi(newURI).getURLStatus();
            if (status == null) {
              await ref
                  .read(authNotifierProvider.notifier)
                  .newServer(serverURI: newURI, isConnected: isConnected);
              if (mounted) {
                context.goNamed(AuthenticatePage.name);
              }
              return null;
            } else {
              return status;
            }
          },
        ),
      );
    }
    return DefaultTextStyle(
      style: Theme.of(context).textTheme.displayLarge!,
      child: Align(
        alignment: Alignment.topLeft,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
          child: Column(
            children: [
              const SectionSplitter(msg: ' '),
              if (widget.serverURI != null) ...[
                Text(
                  (widget.serverURI == ref.read(defaultServerURIProvider))
                      ? 'Now connected with  SandBox Server'
                      : 'Now connected with  ${widget.serverURI}',
                ),
                TextButton(
                  onPressed: () => context.goNamed(AuthenticatePage.name),
                  child: Align(
                    child: Text(
                      'Continue?',
                      style: buttonStyle,
                    ),
                  ),
                ),
                const SectionSplitter(msg: 'Or'),
              ],
              if (widget.serverURI != ref.read(defaultServerURIProvider)) ...[
                TextButton(
                  onPressed: () =>
                      ref.read(authNotifierProvider.notifier).newServer(
                            serverURI: ref.read(defaultServerURIProvider),
                            isConnected: isConnected,
                          ),
                  child: Text('Try with Sandbox Server', style: buttonStyle),
                ),
                const SectionSplitter(msg: 'Or'),
              ],
              TextButton(
                onPressed: () => setState(() {
                  enableEditor = true;
                }),
                child: Text('Join another server ', style: buttonStyle),
              ),
              const SectionSplitter(msg: ' '),
            ],
          ),
        ),
      ),
    );
  }
}
