import 'package:flutter/material.dart';

class CheckboxFormField extends FormField<bool> {
  CheckboxFormField({
    required Widget title,
    required FormFieldValidator<bool> validator,
    required ValueChanged<bool?> onChanged,
    required FocusNode focusNode,
    bool initialValue = false,
    Key? key,
  }) : super(
          key: key,
          validator: validator,
          initialValue: initialValue,
          builder: (state) {
            return CheckboxListTile(
              dense: true,
              title: title,
              value: state.value,
              onChanged: (value) {
                onChanged(value);
                state.didChange(value);
              },
              subtitle: state.hasError
                  ? Builder(
                      builder: (context) => Text(
                        state.errorText!,
                        style: TextStyle(color: Theme.of(context).errorColor),
                      ),
                    )
                  : null,
              controlAffinity: ListTileControlAffinity.leading,
              focusNode: focusNode,
            );
          },
        );
}
