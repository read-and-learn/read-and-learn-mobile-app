import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import '../editor/widgets/lang_selection_view.dart';
import '../stories/stories_page.dart';
import 'widgets/vocabulary_view.dart';

class VocabularyPage extends ConsumerWidget {
  const VocabularyPage({Key? key}) : super(key: key);
  static const name = 'vocabulary';
  static const String path = '/$name';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    if (lang == null) {
      return LangSelectionView(
        onCancel: () => context.goNamed(StoriesPage.name),
      );
    }
    return const VocabularyView();
  }
}
