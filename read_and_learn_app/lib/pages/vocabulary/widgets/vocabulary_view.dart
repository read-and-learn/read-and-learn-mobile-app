import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../common/widgets/drawer_widget.dart';
import '../../common/widgets/view_wrapper.dart';
import '../../settings/widgets/lang_picker_popup.dart';
import 'filter/filter_view_main.dart';
import 'search/search_view_main.dart';
import 'word_list/word_list_main.dart';

class VocabularyView extends ConsumerWidget {
  const VocabularyView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vacabState = ref.watch(vocabStateProvider);

    return ViewWrapper(
      title: 'Vocabulary',
      centerTitle: false,
      showBottomSheet: !vacabState.searchView,
      actions: (vacabState.displayLang == null) || vacabState.searchView
          ? null
          : [
              if (vacabState.learningLanguages.isNotEmpty)
                LangPickerPopup(
                  title: const Icon(Icons.language),
                  lang: vacabState.displayLang!,
                  langList: vacabState.learningLanguages,
                  onChanged: (lang) {
                    ref.read(vocabStateProvider.notifier).displayLang = lang;
                    ref.read(vocabStateProvider.notifier);
                  },
                ),
              IconButton(
                onPressed: () => showMaterialModalBottomSheet(
                  context: context,
                  builder: (context) => const FilterViewMain(),
                ),
                icon: const Icon(Icons.filter_list),
              ),
              IconButton(
                onPressed: () {
                  ref.read(vocabStateProvider.notifier).searchView = true;
                },
                icon: const Icon(Icons.search_outlined),
              ),
              // const DarkModeModifier2()
            ],
      drawer: const AppDrawer(),
      body: vacabState.searchView
          ? SearchViewMain(
              onCloseSearchbar: () {
                ref.read(vocabStateProvider.notifier).searchView = false;
              },
            )
          : const WordListMain(),
    );
  }
}
