import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../../reader/widgets/panel/audio_player.dart';
import 'word_brief.dart';

class WordTileView extends ConsumerWidget {
  const WordTileView({
    required this.text,
    required this.onTap,
    required this.tapped,
    required this.onLongTap,
    Key? key,
  }) : super(key: key);
  final String text;
  final Function() onTap;
  final Function() onLongTap;
  final bool tapped;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final selectionAsync = ref.watch(vocabSelectNotifierProvider(lang));
    return ListTile(
      selected: tapped,
      onTap: onLongTap,
      onLongPress: onTap,
      title: Transform.translate(
        offset: const Offset(-16, 0),
        child: Text(
          text,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
      ),
      leading: AudioPlayer(
        word: text,
        lang: lang!, //Potentially a bug??
      ),
      trailing: selectionAsync.when(
        error: (err, _) => Icon(
          MdiIcons.alert,
          color: Theme.of(context).errorColor,
        ),
        loading: () => const CircularProgressIndicator(),
        data: (vocabSelect) => IconButton(
          onPressed: () =>
              ref.read(vocabSelectNotifierProvider(lang).notifier).toggle(text),
          icon: Icon(
            vocabSelect.isSelected(text)
                ? MdiIcons.checkboxMarkedOutline
                : MdiIcons.checkboxBlankOutline,
          ),
        ),
      ),
      subtitle: WordBrief(text: text),
    );
  }
}
