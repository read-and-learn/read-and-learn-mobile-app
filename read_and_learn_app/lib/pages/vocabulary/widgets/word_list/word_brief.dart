import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WordBrief extends ConsumerWidget {
  const WordBrief({
    required this.text,
    Key? key,
  }) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final textStringsAsync = ref.watch(textStringsNotifierProvider(lang));
    return textStringsAsync.maybeWhen(
      orElse: Container.new,
      data: (textStrings) {
        if (textStrings == null) {
          return Container();
        }
        final tString = textStrings.items[text]!;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text.rich(
              style: Theme.of(context).textTheme.bodyLarge,
              TextSpan(
                children: [
                  if (tString.meaningList.isNotEmpty)
                    const TextSpan(text: ' - '),
                  for (final meaning in tString.meaningList)
                    TextSpan(
                      // style: Theme.of(context).textTheme.bodyLarge,
                      children: [
                        TextSpan(text: meaning.value.text),
                        TextSpan(text: ' (${meaning.value.lang})'),
                        if (tString.meaningList.last != meaning)
                          const TextSpan(text: ', ')
                      ],
                    ),
                  if (tString.meaningList.isNotEmpty &&
                      tString.usageList.isNotEmpty)
                    const TextSpan(text: ': '),
                  if (tString.usageList.isNotEmpty)
                    for (final usage in tString.usageList)
                      TextSpan(
                        // style: Theme.of(context).textTheme.bodyMedium,
                        children: [
                          TextSpan(text: '"${usage.value.text}"'),
                          if (tString.usageList.last != usage)
                            const TextSpan(text: ', '),
                        ],
                      ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
