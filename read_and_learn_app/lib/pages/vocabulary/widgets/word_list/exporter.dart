import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:read_and_learn/pages/word/learn_page.dart';

import '../../../common/widgets/error_view.dart';
import '../../../common/widgets/loading_view.dart';

class Exporter extends ConsumerWidget {
  const Exporter({Key? key, this.child}) : super(key: key);
  final Widget? child;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final vacobState = ref.watch(vocabStateProvider);
    final lang = vacobState.displayLang;
    final selectView = vacobState.selectView;
    final searchView = vacobState.searchView;
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final selectionAsync = ref.watch(vocabSelectNotifierProvider(lang));

    return langPrefProvider.when(
      loading: () => const LoadingView(),
      error: (err, _) => ErrorView(errorMessage: err.toString()),
      data: (pref) {
        return DecoratedBox(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(
                color: Theme.of(context).dividerColor,
                width: 2,
              ),
            ),
          ),
          //  color: Theme.of(context).highlightColor,
          child: selectionAsync.when(
            loading: () => const LoadingView(),
            error: (err, _) => ErrorView(errorMessage: err.toString()),
            data: (vocabSelect) {
              var anySelected = false;
              anySelected = vocabSelect.selected.isNotEmpty;

              return Column(
                children: [
                  if (anySelected)
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: searchView
                              ? MainAxisAlignment.end
                              : MainAxisAlignment.spaceBetween,
                          children: [
                            if (!searchView)
                              TextButton(
                                onPressed: searchView
                                    ? null
                                    : () => ref
                                        .read(vocabStateProvider.notifier)
                                        .selectView = !selectView,
                                child: Text(
                                  (!selectView) ? 'Show selected' : 'Show All',
                                  style: searchView
                                      ? Theme.of(context).textTheme.displaySmall
                                      : Theme.of(context)
                                          .textTheme
                                          .headlineLarge!
                                          .copyWith(
                                            color: Colors.blueAccent.shade400,
                                          ),
                                ),
                              ),
                            TextButton(
                              child: Text(
                                'Clear All',
                                style: Theme.of(context)
                                    .textTheme
                                    .headlineLarge!
                                    .copyWith(
                                      color: Colors.blueAccent.shade400,
                                    ),
                              ),
                              onPressed: () {
                                ref
                                    .read(
                                      vocabSelectNotifierProvider(lang)
                                          .notifier,
                                    )
                                    .clear();
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  if (anySelected)
                    Text(
                      ' ${vocabSelect.selected.length} selected',
                      style: Theme.of(context).textTheme.displaySmall,
                    ),
                  if (anySelected)
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ActionChip(
                            label: Text(
                              'Export',
                              style: Theme.of(context).textTheme.displaySmall,
                            ),
                            onPressed: () {},
                            avatar: const Icon(MdiIcons.exportVariant),
                          ),
                          ActionChip(
                            label: Text(
                              'Learn',
                              style: Theme.of(context).textTheme.displaySmall,
                            ),
                            onPressed: vocabSelect.selected.isEmpty
                                ? () {}
                                : () {
                                    final langPref =
                                        LangPref.fromJson(pref.value);
                                    // ignore: unused_local_variable
                                    final learnersLang =
                                        langPref.nativeLanguage;
                                    ref
                                        .read(
                                          learnStateNotifierProvider.notifier,
                                        )
                                        .setWords(
                                          words: vocabSelect.selected,
                                          vocablang: lang!,
                                          learnersLang: learnersLang,
                                        );

                                    context.goNamed(LearnPage.name);
                                  },
                            avatar: const Icon(MdiIcons.bookshelf),
                          ),
                        ],
                      ),
                    ),
                  if (child != null) child!,
                  const SizedBox(
                    height: 30,
                  )
                ],
              );
            },
          ),
        );
      },
    );
  }
}
