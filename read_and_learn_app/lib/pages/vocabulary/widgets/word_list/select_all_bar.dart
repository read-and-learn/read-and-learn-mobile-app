import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:language_picker/languages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SelectAllBar extends ConsumerStatefulWidget {
  const SelectAllBar({
    required this.items,
    required this.showingSelected,
    required this.onTap,
    Key? key,
  }) : super(key: key);
  final List<String> items;

  final bool showingSelected;

  final Function() onTap;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SelectAllBarState();
}

class _SelectAllBarState extends ConsumerState<SelectAllBar> {
  @override
  Widget build(BuildContext context) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final selectionAsync = ref.watch(vocabSelectNotifierProvider(lang));
    final textStringsAsync = ref.watch(textStringsNotifierProvider(lang));
    final totalItems = textStringsAsync.maybeWhen(
      orElse: () => null,
      data: (textStrings) => textStrings?.items.length,
    );
    return selectionAsync.when(
      error: (err, _) => ShowError(errorMessage: err.toString()),
      loading: () => const CLLoading(),
      data: (vocabSelect) {
        bool anySelected;
        bool allSelected;
        anySelected = widget.items
            .any((element) => vocabSelect.selected.contains(element));
        allSelected = widget.items
            .every((element) => vocabSelect.selected.contains(element));

        return Column(
          children: [
            GestureDetector(
              onTap: widget.onTap,
              child: (widget.showingSelected && !anySelected)
                  ? const Text('Nothing selected')
                  : ListTile(
                      leading: Transform.scale(
                        scale: 1.2,
                        child: Transform.translate(
                          offset: const Offset(24, 8),
                          child: RotationTransition(
                            turns: const AlwaysStoppedAnimation(-15 / 360),
                            child: Text(
                              Language.fromIsoCode(lang!).name,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                          ),
                        ),
                      ),
                      title: (totalItems == null)
                          ? Container()
                          : Align(
                              child: Text(
                                'Showing ${widget.items.length}/$totalItems',
                              ),
                            ),
                      trailing: IconButton(
                        onPressed: () => all(allSelected: allSelected),
                        icon: Icon(
                          allSelected
                              ? MdiIcons.checkboxMarkedOutline
                              : anySelected
                                  ? MdiIcons.checkboxIntermediate
                                  : MdiIcons.checkboxBlankOutline,
                        ),
                      ),
                    ),
            ),
          ],
        );
      },
    );
  }

  void all({required bool allSelected}) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    if (!allSelected) {
      ref
          .read(vocabSelectNotifierProvider(lang).notifier)
          .selectMultiple(List.from(widget.items));
    } else {
      ref
          .read(vocabSelectNotifierProvider(lang).notifier)
          .deselectMultiple(List.from(widget.items));
    }
  }
}
