import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:read_and_learn/pages/vocabulary/vocabulary_page.dart';

import '../../../word/word_page.dart';
import 'exporter.dart';
import 'select_all_bar.dart';
import 'word_tile_view.dart';

class WordListView extends ConsumerStatefulWidget {
  const WordListView({
    required this.scrollController,
    required this.words,
    this.child,
    Key? key,
  }) : super(key: key);
  final ScrollController scrollController;
  final List<String> words;
  final Widget? child;
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WordListViewState();
}

class _WordListViewState extends ConsumerState<WordListView> {
  int tappedIndex = -1;
  @override
  Widget build(BuildContext context) {
    /* Auth? auth = ref.watch(authNotifierProvider);
    bool isConnected = ref.watch(
        connectivityStateNotifierProvider.select((value) => value.isConnected)); */
    final vocabState = ref.watch(vocabStateProvider);

    final selectView = vocabState.selectView;
    final searchView = vocabState.searchView;
    final showExporter = !searchView || !vocabState.isFocussed;

    return Column(
      children: [
        if (selectView)
          FlashWarning(
            message: 'Showing only selected item',
            actionLabel: 'Show All',
            onClear: () =>
                ref.read(vocabStateProvider.notifier).selectView = !selectView,
          ),
        if (widget.words.isEmpty)
          Expanded(
            child: Center(
              child: Text(
                'Nothing found',
                style: Theme.of(context)
                    .textTheme
                    .displayMedium!
                    .copyWith(color: Theme.of(context).errorColor),
              ),
            ),
          )
        else
          Expanded(
            child: Column(
              children: [
                SelectAllBar(
                  items: widget.words,
                  showingSelected: selectView,
                  onTap: () {
                    widget.scrollController.animateTo(
                      0,
                      duration: const Duration(milliseconds: 1000),
                      curve: Curves.linear,
                    );
                  },
                ),
                const TileDivider(),
                Expanded(
                  child: ListView.separated(
                    controller: widget.scrollController,
                    separatorBuilder: (context, index) => const TileDivider(),
                    itemBuilder: (context, index) {
                      return WordTileView(
                        key: ValueKey('RANDKEYRRSRS${widget.words[index]}'),
                        text: widget.words[index],
                        onLongTap: () {
                          context.pushNamed(
                            WordPage.name,
                            queryParams: {
                              'from': VocabularyPage.name,
                              'text': widget.words[index]
                            },
                          );
                        },
                        onTap: () {
                          ref.read(vocabStateProvider.notifier).tappedIndex =
                              index;
                        },
                        tapped: vocabState.tappedIndex == index,
                      );
                    },
                    itemCount: widget.words.length,
                  ),
                ),
                if (showExporter) const TileDivider(),
                if (showExporter)
                  Exporter(
                    child: widget.child,
                  ),
              ],
            ),
          ),
      ],
    );
  }
}

class FlashWarning extends StatelessWidget {
  const FlashWarning({
    required this.onClear,
    required this.message,
    required this.actionLabel,
    Key? key,
  }) : super(key: key);
  final Function() onClear;
  final String message;
  final String actionLabel;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).disabledColor,
      height: 32,
      child: Row(
        children: [
          Expanded(
            child: Text(message, style: Theme.of(context).textTheme.bodySmall),
          ),
          TextButton(
            onPressed: onClear,
            child: Text(
              actionLabel,
              style: Theme.of(context).textTheme.headlineSmall,
            ),
          )
        ],
      ),
    );
  }
}

class TileDivider extends StatelessWidget {
  const TileDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      color: Theme.of(context).dividerColor,
      thickness: 0.5,
      height: 2,
    );
  }
}
