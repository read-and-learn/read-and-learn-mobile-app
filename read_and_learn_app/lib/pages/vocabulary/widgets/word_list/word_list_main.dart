import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'words_list_view.dart';

class WordListMain extends ConsumerStatefulWidget {
  const WordListMain({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WordListMainState();
}

class _WordListMainState extends ConsumerState<WordListMain> {
  late ScrollController scrollController;

  @override
  void initState() {
    scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final vocabState = ref.watch(vocabStateProvider);
    final lang = vocabState.displayLang;
    final showSelected = vocabState.selectView;

    final vocabFiltersAsync = ref.watch(vocabFiltersNotifierProvider(lang));
    final vocabSelectAsync = ref.watch(vocabSelectNotifierProvider(lang));
    return vocabFiltersAsync.when(
      loading: () => const CLLoading(),
      error: (err, _) => ShowError(errorMessage: err.toString()),
      data: (vocabFilters) => vocabSelectAsync.when(
        loading: () => const CLLoading(),
        error: (err, _) => ShowError(errorMessage: err.toString()),
        data: (vocabSelect) {
          var words = <String>[];

          words = showSelected
              ? vocabSelect.getSelectedWords(
                  list: vocabFilters.getFilterred,
                )
              : vocabFilters.getFilterred;

          return WordListView(
            words: words,
            scrollController: scrollController,
          );
        },
      ),
    );
  }
}
