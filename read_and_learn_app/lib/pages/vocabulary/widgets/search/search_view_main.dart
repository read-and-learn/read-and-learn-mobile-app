import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../word_list/words_list_view.dart';
import 'search_bar.dart' as searchbar;

class SearchViewMain extends ConsumerStatefulWidget {
  const SearchViewMain({
    required this.onCloseSearchbar,
    Key? key,
  }) : super(key: key);
  final Function() onCloseSearchbar;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SearchMainState();
}

class _SearchMainState extends ConsumerState<SearchViewMain> {
  late TextEditingController textEditingController;
  late FocusNode focusNode;
  late ScrollController scrollController;
  String searchText = '';
  bool isFocussed = true;
  @override
  void initState() {
    textEditingController = TextEditingController();
    focusNode = FocusNode();
    focusNode.addListener(onFocusChange);
    scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    focusNode.dispose();
    scrollController.dispose();
    super.dispose();
  }

  void onFocusChange() {
    setState(() {
      isFocussed = focusNode.hasFocus;
      ref.read(vocabStateProvider.notifier).isFocussed = isFocussed;
    });
    debugPrint('Focus: ${focusNode.hasFocus}');
  }

  @override
  Widget build(BuildContext context) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final textStringsAsync = ref.watch(textStringsNotifierProvider(lang));
    return Column(
      children: [
        Align(
          alignment: Alignment.centerRight,
          child: IconButton(
            onPressed: widget.onCloseSearchbar,
            icon: const Icon(Icons.close),
          ),
        ),
        const SizedBox(height: 8),
        searchbar.SearchBar(
          textEditingController: textEditingController,
          focusNode: focusNode,
          onCloseSearchbar: widget.onCloseSearchbar,
          onChanged: (str) {
            setState(() {
              searchText = str;
            });
          },
        ),
        Expanded(
          child: textStringsAsync.when(
            loading: () => const CLLoading(),
            error: (err, _) => ShowError(errorMessage: err.toString()),
            data: (textStrings) {
              if (textStrings == null) {
                return const Center(child: Text('Nothing to search'));
              }
              if (searchText.isEmpty) {
                return Column(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          'Start typing word',
                          style: Theme.of(context).textTheme.displayLarge,
                        ),
                      ),
                    ),
                    if (!isFocussed)
                      ActionChip(
                        label: Text(
                          'Close Search',
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                        onPressed: widget.onCloseSearchbar,
                        avatar: const Icon(MdiIcons.close),
                      ),
                    if (!isFocussed)
                      const SizedBox(
                        height: 30,
                      ),
                  ],
                );
              }

              final words = textStrings.items.keys
                  .toList()
                  .where((e) => e.contains(searchText))
                  .toList();

              return WordListView(
                scrollController: scrollController,
                words: words,
              );
            },
          ),
        ),
      ],
    );
  }
}
