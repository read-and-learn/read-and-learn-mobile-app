import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    required this.focusNode,
    required this.textEditingController,
    required this.onCloseSearchbar,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  final FocusNode focusNode;
  final TextEditingController textEditingController;
  final Function() onCloseSearchbar;

  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme.bodySmall!;
    final wordStyle = textStyle.copyWith(fontSize: textStyle.fontSize! + 6);
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: TextField(
              focusNode: focusNode,
              autofocus: true,
              controller: textEditingController,
              textInputAction: TextInputAction.done,
              showCursor: true,
              cursorColor: wordStyle.color,
              cursorWidth: 1,
              decoration: InputDecoration(
                /* prefixIcon: IconButton(
                  onPressed: () => onCloseSearchbar(),
                  icon: Icon(
                    Icons.search_off_sharp,
                    size: wordStyle.fontSize! * 2,
                    color: wordStyle.color!,
                  ),
                ), */
                suffixIcon: IconButton(
                  onPressed: () {
                    textEditingController.text = '';
                    onChanged?.call('');
                  },
                  icon: Icon(
                    MdiIcons.backspace,
                    color: wordStyle.color,
                  ),
                ),
                // filled: true,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(color: wordStyle.color!),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide:
                      BorderSide(color: Theme.of(context).disabledColor),
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12),
                  borderSide: BorderSide(color: Theme.of(context).errorColor),
                ),
              ),
              onChanged: onChanged,
              onEditingComplete: focusNode.unfocus,
            ),
          ),
        ),
      ],
    );
  }
}
