import 'dart:ui';

import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class ChipFilterMenu extends ConsumerWidget {
  const ChipFilterMenu({
    required this.item,
    Key? key,
  }) : super(key: key);

  final MapEntry<String, VocabFilter> item;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text.rich(
          TextSpan(
            children: [
              TextSpan(
                text: (item.value.title.length < 45)
                    ? item.value.title
                    : '${item.value.title.substring(0, 42)}...',
              ),
              const TextSpan(text: ' '),
              TextSpan(
                text: (item.value.isActive) ? 'on' : 'off',
                style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                  color: (item.value.isActive)
                      ? Colors.green.shade200
                      : Colors.red.shade200,
                  fontFeatures: [const FontFeature.enable('sups')],
                ),
              )
            ],
          ),
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 4,
          ), // only(left: 8, right: 8, top: 0, bottom: 4),
          child: Wrap(
            spacing: 2,
            children: [
              for (MapEntry selectableItems in item.value.keysLabel.entries)
                CustomFilterChip(
                  label: selectableItems.value as String,
                  isSelected: item.value.isKeySelected(selectableItems.key),
                  onSelected: (val) {
                    ref
                        .read(vocabFiltersNotifierProvider(lang).notifier)
                        .filterKeepKey(item.key, selectableItems.key, val);
                  },
                ),
              if (item.value.exceptsLabel != null)
                CustomFilterChip(
                  label: item.value.exceptsLabel!,
                  isSelected: item.value.keepMiss!,
                  onSelected: (val) {
                    ref
                        .read(vocabFiltersNotifierProvider(lang).notifier)
                        .filterKeepMiss(item.key, val);
                  },
                )
            ],
          ),
        ),
      ],
    );
  }
}
