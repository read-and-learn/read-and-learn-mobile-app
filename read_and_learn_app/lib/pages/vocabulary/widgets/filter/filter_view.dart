import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/app_theme.dart';
import 'chip_filter_menu.dart';

class FilterView extends ConsumerWidget {
  const FilterView({
    required this.vocabFilters,
    required this.lang,
    Key? key,
  }) : super(key: key);
  final VocabFilters vocabFilters;
  final String lang;
  void clearFilter(Ref ref, String lang) {
    ref.read(vocabFiltersNotifierProvider(lang).notifier).clear();
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final Map<String, VocabFilter> vocabFilter = vocabFilters.vocabFilters;
    return AppTheme(
      child: SizedBox(
        height: MediaQuery.of(context).size.height / 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        'Done',
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(color: Colors.blueAccent.shade400),
                      ),
                    ),
                    TextButton(
                      onPressed: () => clearFilter(ref as Ref, lang),
                      child: Text(
                        'Clear',
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(color: Colors.blueAccent.shade400),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 4),
                Expanded(
                  child: ListView(
                    shrinkWrap: true,
                    padding: EdgeInsets.zero,
                    children: [
                      for (final item in vocabFilter.entries.toList())
                        ChipFilterMenu(item: item),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
