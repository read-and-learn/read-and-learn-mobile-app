import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../common/widgets/error_view.dart';

import 'filter_view.dart';

class FilterViewMain extends ConsumerWidget {
  const FilterViewMain({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final vocabFiltersAsync = ref.watch(vocabFiltersNotifierProvider(lang));

    return vocabFiltersAsync.when(
      error: (err, _) => ErrorView(errorMessage: 'Error in Filter, $err'),
      loading: () => const CLLoading(),
      data: (vocabFilters) => (lang == null)
          ? const ErrorView(errorMessage: 'Language is not set correctly')
          : FilterView(vocabFilters: vocabFilters, lang: lang),
    );
  }
}
