import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SidebarMenuItem extends ConsumerWidget {
  const SidebarMenuItem({
    required this.title,
    required Function()? onTab,
    Key? key,
    this.level = 0,
    IconData? iconData, //  = Icons.outlined_flag_outlined,

    this.vPadding,
  })  : _iconData = iconData,
        _onTab = onTab,
        super(key: key);
  final double level;
  final String title;
  final dynamic _iconData;
  final Function()? _onTab;
  final double? vPadding;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //final double offset = ((level + 1) * 16);
    return Padding(
      padding: EdgeInsets.only(left: (_iconData == null) ? 8 : 0),
      child: ListTile(
        contentPadding: (vPadding != null)
            ? EdgeInsets.symmetric(
                horizontal: 20 + level * 25.0,
                vertical: vPadding!,
              )
            : EdgeInsets.symmetric(horizontal: 20 + level * 25.0),

        leading: (_iconData == null)
            ? null
            : (_iconData.runtimeType == String)
                ? Text(_iconData as String)
                : Icon(_iconData as IconData),
        title: Transform.translate(
          offset: const Offset(-16, 0),
          child: Text(
            title,
          ),
        ), //,
        onTap: () {
          _onTab!();
        },
      ),
    );
  }
}
