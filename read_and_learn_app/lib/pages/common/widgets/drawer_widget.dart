import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../authenticate/logout_page.dart';
import '../../settings/settings.dart';
import '../../stories/stories_page.dart';
import '../../vocab_import/vocab_import_page.dart';
import '../../vocabulary/vocabulary_page.dart';
import '../../word/learn_page.dart';
import 'sidebar_menu_item.dart';

class AppDrawer extends ConsumerStatefulWidget {
  const AppDrawer({Key? key}) : super(key: key);

  @override
  ConsumerState<AppDrawer> createState() => AppDrawerState();
}

class AppDrawerState extends ConsumerState<AppDrawer> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.6,
      child: Drawer(
        backgroundColor: Theme.of(context).primaryColor,
        child: Column(
          children: [
            myDrawerHeader(context),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 25.0 + 14),
                child: DecoratedBox(
                  decoration:
                      BoxDecoration(color: Theme.of(context).canvasColor),
                  child: Column(
                    children: [
                      Expanded(
                        child: Column(
                          children: [
                            SidebarMenuItem(
                              title: 'Stories',
                              iconData: Icons.home_filled,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.goNamed(StoriesPage.name);
                              },
                            ),
                            SidebarMenuItem(
                              title: 'Vocabulary',
                              iconData: MdiIcons.sortAlphabeticalVariant,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.goNamed(VocabularyPage.name);
                              },
                            ),
                            SidebarMenuItem(
                              level: 1,
                              title: 'Import',
                              iconData: Icons.file_download_sharp,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.goNamed(VocabImportPage.name);
                              },
                            ),
                            SidebarMenuItem(
                              level: 1,
                              title: 'Learn',
                              iconData: MdiIcons.bookshelf,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.goNamed(LearnPage.name);
                              },
                            ),
                            SidebarMenuItem(
                              title: 'Logout',
                              iconData: Icons.logout_sharp,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.pushNamed(LogOutUserPage.name);
                              },
                            ),
                            const Divider(),
                            const SizedBox(
                              height: 15,
                            ),
                            /* SidebarMenuItem(
                              title: "Debug only",
                              iconData: MdiIcons.testTube,
                              onTab: () {},
                            ),
                            SidebarMenuItem(
                              title: "Receive Image",
                              level: 1,
                              iconData: Icons.share,
                              onTab: () {
                                Navigator.of(context).pop();
                                context.pushNamed(ReceiveImagePage.name);
                              },
                            ) */
                          ],
                        ),
                      ),
                      SidebarMenuItem(
                        title: 'Settings',
                        iconData: Icons.settings_sharp,
                        onTab: () {
                          Navigator.of(context).pop();
                          context.goNamed(SettingsPage.name);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget myDrawerHeader(BuildContext context) {
    final authData = ref.watch(authNotifierProvider);
    return SizedBox(
      height:
          MediaQuery.of(context).padding.top + AppBar().preferredSize.height,
      width: double.infinity,
      child: DrawerHeader(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.only(left: 20),
        child: Row(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Hello, ${authData?.user?.name ?? "Guest"}",
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            const Spacer(flex: 6),
          ],
        ),
      ),
    );
  }
}
