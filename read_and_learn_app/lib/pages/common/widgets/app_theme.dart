import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AppTheme extends ConsumerWidget {
  const AppTheme({
    required this.child,
    Key? key,
  }) : super(key: key);
  final Widget child;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var themeData =
        ref.watch(styleNotifierProvider.select((value) => value.themeData));
    themeData = themeData.copyWith(
      textTheme: themeData.textTheme.copyWith(
        bodyMedium: TextStyle(
          fontSize: 14,
          color: themeData.brightness == Brightness.light
              ? Colors.black
              : Colors.white,
        ),
      ),
    );
    return Theme(
      data: themeData,
      child: child,
    );
  }
}
