import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';

import 'view_wrapper.dart';

class LoadingView extends StatelessWidget {
  const LoadingView({
    Key? key,
    this.title,
    this.message,
  }) : super(key: key);
  final String? title;
  final String? message;

  @override
  Widget build(BuildContext context) {
    return ViewWrapper(
      title: 'Please wait ...',
      body: CLLoading(message: message),
    );
  }
}
