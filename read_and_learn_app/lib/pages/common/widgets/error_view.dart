import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/material.dart';

import 'drawer_widget.dart';
import 'view_wrapper.dart';

// TODO(anandas): needs improvement
class ErrorView extends StatelessWidget {
  const ErrorView({
    this.errorMessage,
    Key? key,
  }) : super(key: key);
  final String? errorMessage;

  @override
  Widget build(BuildContext context) {
    return ViewWrapper(
      title: 'Error !!!',
      drawer: const AppDrawer(),
      body: ShowError(errorMessage: errorMessage),
    );
  }
}
