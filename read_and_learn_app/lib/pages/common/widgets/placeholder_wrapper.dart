import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'loading_view.dart';

class PlaceholderWrapper extends ConsumerWidget {
  const PlaceholderWrapper(
    this.asyncValue, {
    required this.onData,
    Key? key,
    this.placeHolder = const LoadingView(),
  }) : super(key: key);
  final AsyncValue<dynamic> asyncValue;
  final Widget Function(dynamic) onData;
  final Widget placeHolder;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return asyncValue.maybeWhen(orElse: () => placeHolder, data: onData);
  }
}
