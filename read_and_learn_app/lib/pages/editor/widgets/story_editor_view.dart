import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:language_picker/languages.dart';
import 'package:text_editor/text_editor.dart';

import '../../settings/widgets/lang_picker_popup.dart';

class EditorView extends ConsumerStatefulWidget {
  const EditorView({
    required this.onCancel,
    Key? key,
    this.story,
    this.langPref,
  }) : super(key: key);
  final Story? story;
  final LangPref? langPref;
  final Function() onCancel;
  @override
  ConsumerState<ConsumerStatefulWidget> createState() => EditStoryViewState();
}

class EditStoryViewState extends ConsumerState<EditorView> {
  late String lang;

  @override
  void initState() {
    if (widget.story == null && widget.langPref == null) {
      throw Exception(
        'Either story is missing or Language is not set correctly',
      );
    }
    lang = (widget.story != null)
        ? widget.story!.lang
        : widget.langPref!.currLearningLang!;
    super.initState();
  }

  Future<bool> onSave({required String title, required String content}) async {
    final story = Story(
      id: widget.story?.id ?? -1,
      title: title,
      content: content,
      contentType: null,
      lang: lang,
    );
    try {
      await ref.read(storiesNotifierProvider.notifier).insertStory(story);
      await ref.read(textStringsNotifierProvider(lang).notifier).clear();
      await ref.read(textStringsNotifierProvider(lang).notifier).onRefresh();
      widget.onCancel();
      return true;
    } on Exception catch (e) {
      if (mounted) {
        ReportError.snackBar(
          context,
          errorString: 'Unable to save the story',
          details: e.toString().replaceAll('Exception:', '').trim(),
          onClipboardCopy: (text) async => ClipboardManager.copy(text),
        );
      }
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Editor(
      title: widget.story?.title,
      content: widget.story?.text,
      onSave: onSave,
      onCancel: widget.onCancel,
      appBarActions: [
        Padding(
          padding: const EdgeInsets.all(4),
          child: Center(
            child: Text(Language.fromIsoCode(lang).name.toUpperCase()),
          ),
        ),
        if (widget.story == null)
          LangPickerPopup(
            title: const Icon(Icons.language),
            lang: lang,
            langList: widget.langPref!.learningLanguages,
            onChanged: (lang) => {
              setState(() {
                this.lang = lang;
              }),
            },
          ),
        const SizedBox(
          width: 8,
        )
      ],
    );
  }
}
