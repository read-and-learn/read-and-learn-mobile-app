import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:read_and_learn/pages/common/widgets/drawer_widget.dart';
import 'package:read_and_learn/pages/settings/widgets/lang_select.dart';

import '../../common/widgets/view_wrapper.dart';

class LangSelectionView extends ConsumerStatefulWidget {
  const LangSelectionView({
    required this.onCancel,
    Key? key,
  }) : super(key: key);

  final Function() onCancel;
  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      LangSelectionPageState();
}

// ignore: slash_for_doc_comments
/**
 *  leading: (!kIsWeb)
            ? IconButton(
                onPressed: () {
                  widget.onCancel();
                },
                icon: const Icon(
                  Icons.home,
                ))
            : null,
 */

class LangSelectionPageState extends ConsumerState<LangSelectionView> {
  @override
  Widget build(BuildContext context) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    return ViewWrapper(
      title: 'Select Language',
      drawer: const AppDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'You must have atleast one learning language ',
                style: Theme.of(context).textTheme.bodyLarge,
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 16,
              ),
              langPrefProvider.maybeWhen(
                data: (pref) {
                  final langPref = LangPref.fromJson(pref.value);

                  return GestureDetector(
                    onTap: () async => onSelectLanguage(
                      context,
                      onSelection: (langISOCode) => ref
                          .read(prefNotifierProvider('lang_pref').notifier)
                          .set(
                            langPref
                                .copyWith(
                                  learningLanguages: {
                                    langISOCode,
                                    ...langPref.learningLanguages
                                  }.toList(),
                                )
                                .toJson(),
                          ),
                    ),
                    child: Text(
                      'Select a language',
                      style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                            color: Colors.blue.shade400,
                            decoration: TextDecoration.underline,
                            fontWeight: FontWeight.bold,
                          ),
                    ),
                  );
                },
                orElse: Container.new,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/* 
                    return LangPicker(
                        initialValue: null,
                        onValuePicked: (Language? lang) {
                          if (lang != null) {
                            ref
                                .read(
                                    prefNotifierProvider("lang_pref").notifier)
                                .set(langPref
                                    .copyWith(
                                        learningLanguages: {
                                      lang.isoCode,
                                      ...langPref.learningLanguages
                                    }.toList())
                                    .toJson());
                          } */
