import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../common/widgets/placeholder_wrapper.dart';
import '../stories/stories_page.dart';
import 'widgets/lang_selection_view.dart';
import 'widgets/story_editor_view.dart';

class EditorPage extends ConsumerStatefulWidget {
  const EditorPage({Key? key, this.id}) : super(key: key);
  static const name = 'edit-story';
  static const String path = '/$name';
  final int? id;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _EditStoryPageState();
}

class _EditStoryPageState extends ConsumerState<EditorPage> {
  void onCancel() {
    context.goNamed(StoriesPage.name);
  }

  @override
  Widget build(BuildContext context) {
    final storyProvider = ref.watch(storyNotifierProvider(widget.id));
    return PlaceholderWrapper(
      storyProvider,
      onData: (dynamic p0) {
        final story = p0 as Story?;
        if (story != null) {
          return EditorView(
            story: story,
            onCancel: onCancel,
          );
        }
        final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
        return PlaceholderWrapper(
          langPrefProvider,
          onData: (dynamic p0) {
            final langPref = LangPref.fromJson((p0 as Pref).value);
            if (langPref.currLearningLang == null) {
              return LangSelectionView(
                onCancel: onCancel,
              );
            }
            return EditorView(onCancel: onCancel, langPref: langPref);
          },
        );
      },
    );
  }
}
