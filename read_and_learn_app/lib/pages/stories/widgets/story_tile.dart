import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../editor/editor_page.dart';
import '../../reader/reader_page.dart';
import '../../vocabulary/vocabulary_page.dart';

class StoryTile extends ConsumerWidget {
  const StoryTile({
    required this.story,
    Key? key,
  }) : super(key: key);

  final Story story;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final vocabFilterAsync =
        ref.watch(vocabFiltersNotifierProvider(story.lang));
    return Card(
      elevation: 2,
      child: ListTile(
        onTap: () {
          context.pushNamed(
            ReaderPage.name,
            params: {'sid': story.id.toString()},
          );
        },
        leading: vocabFilterAsync.maybeWhen(
          orElse: () => const CircularProgressIndicator(),
          data: (data) => IconButton(
            icon: const Icon(Icons.read_more_outlined),
            onPressed: () {
              ref.read(vocabStateProvider.notifier).displayLang = story.lang;
              ref.read(vocabStateProvider);
              ref
                  .read(
                    vocabFiltersNotifierProvider(story.lang).notifier,
                  )
                  .upsertFilter(
                    'story',
                    vocabularyOf(id: story.id, title: story.title),
                  );
              ref.read(vocabFiltersNotifierProvider(story.lang));
              context.goNamed(VocabularyPage.name);
            },
          ),
        ),
        title: Text(
          story.title,
          style: Theme.of(context).textTheme.bodyLarge,
        ),
        subtitle: Text(
          story.lang,
          style: Theme.of(context).textTheme.labelMedium,
        ), // lang name??
        trailing: !isConnected
            ? null
            : GestureDetector(
                onTap: () {
                  context.pushNamed(
                    EditorPage.name,
                    params: {'sid': story.id.toString()},
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Icon(
                    MdiIcons.pencil,
                    color: Theme.of(context).textTheme.bodyLarge!.color,
                  ),
                ),
              ),
      ),
    );
  }
}
