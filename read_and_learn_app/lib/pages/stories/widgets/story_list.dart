import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:read_and_learn/pages/exceptions/offline_page.dart';

import '../../receive_content/receive_image_page.dart';
import 'ocr_image_tile.dart';
import 'story_tile.dart';

class StoryList extends ConsumerWidget {
  StoryList({
    required this.stories,
    Key? key,
  }) : super(key: key);
  final Stories stories;

  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();
  Future<void> onRefresh() async {
    return;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final auth = ref.watch(authNotifierProvider);
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );

    if (stories.isEmpty) {
      if (!isConnected) {
        OfflineMessage(
          server: auth!.server,
        );
      }
      return RefreshIndicator(
        key: _refreshIndicatorKey,
        backgroundColor: Colors.blue,
        strokeWidth: 4,
        onRefresh: () async {
          if (isConnected) {
            await LocalStories().clear();
            // ignore: unused_result
            ref.refresh(storiesNotifierProvider);
          }
        },
        child: Center(
          child: ElevatedButton.icon(
            onPressed: () async {
              context.pushNamed(
                ImageHandlerPage.name,
                queryParams: {'from': 'stories'},
              );
            },
            icon: const Icon(Icons.add),
            label: const Text('Add story'),
          ),
        ),
      );
    }

    return RefreshIndicator(
      key: _refreshIndicatorKey,
      backgroundColor: Colors.blue,
      strokeWidth: 4,
      onRefresh: () async {
        if (isConnected) {
          await LocalStories().clear();
          // ignore: unused_result
          ref.refresh(storiesNotifierProvider);
        }
      },
      child: ListView.builder(
        itemCount: stories.stories.length,
        itemBuilder: (context, i) {
          return DismissMenu(
            onDismissed: (direction) async {
              await ref.read(repoReloaderProvider).deleteStory(
                    storyId: stories.stories[i].id,
                    lang: stories.stories[i].lang,
                  );

              //await ref.read(storiesNotifierProvider.notifier).removeStory(storyId);
            },
            child: stories.stories[i].isOCRImage
                ? OCRImageTile(
                    ocrImage: OCRImageStoryIO.fromStory(stories.stories[i]),
                  )
                : StoryTile(story: stories.stories[i]),
          );
        },
      ),
    );
  }
}

class DismissMenu extends ConsumerWidget {
  const DismissMenu({
    required this.child,
    required this.onDismissed,
    Key? key,
  }) : super(key: key);
  final Widget child;
  final Function(DismissDirection direction) onDismissed;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    return Dismissible(
      key: ValueKey(child),
      background: Container(
        color: Theme.of(context).errorColor,
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(right: 4),
        margin: const EdgeInsets.symmetric(
          horizontal: 2,
          vertical: 2,
        ),
        child: const Icon(
          Icons.delete,
          color: Colors.white,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) async {
        final result = await showOkCancelAlertDialog(
          context: context,
          message: 'Are you sure that you want to delete this story?',
          okLabel: 'Yes',
          cancelLabel: 'No',
        );
        return result == OkCancelResult.ok;
      },
      onDismissed: isConnected ? onDismissed : null,
      child: child,
    );
  }
}
