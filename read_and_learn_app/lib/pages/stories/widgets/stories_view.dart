import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../../common/widgets/drawer_widget.dart';
import '../../common/widgets/view_wrapper.dart';
import '../../receive_content/receive_image_page.dart';
import 'story_list.dart';

class StoriesView extends ConsumerWidget {
  const StoriesView({required this.stories, Key? key}) : super(key: key);
  final Stories stories;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    return ViewWrapper(
      title: 'Read and Learn',
      actions: !isConnected
          ? null
          : <Widget>[
              GestureDetector(
                onTap: () async {
                  await LocalStories().clear();
                  // ignore: unused_result
                  ref.refresh(storiesNotifierProvider);
                },
                child: const Padding(
                  padding: EdgeInsets.only(right: 8),
                  child: Icon(MdiIcons.refresh),
                ),
              ),
              IconButton(
                padding: EdgeInsets.zero,
                onPressed: () async {
                  context.pushNamed(
                    ImageHandlerPage.name,
                    queryParams: {'from': 'stories'},
                  );
                },
                icon: const Icon(MdiIcons.plus),
              ),
            ],
      body: StoryList(stories: stories),
      drawer: const AppDrawer(),
      /* floatingActionButton: !isConnected
          ? null
          : (stories.isEmpty)
              ? null
              : FloatingActionButton.extended(
                  onPressed: () {
                    context.pushNamed("NewStory");
                  },
                  icon: const Icon(MdiIcons.plus),
                  label: const Text('Add another story'),
                ), */
    );
  }
}
