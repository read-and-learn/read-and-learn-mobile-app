import 'package:dart_hocr/dart_hocr.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import 'package:language_picker/languages.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:read_and_learn/pages/ocr/ocr_page.dart';

import '../../../recieve_shared/providers/image_extension.dart';
import '../../reader/reader_page.dart';
import '../../vocabulary/vocabulary_page.dart';

class OCRImageTile extends ConsumerWidget {
  const OCRImageTile({
    required this.ocrImage,
    Key? key,
  }) : super(key: key);
  final OCRImage ocrImage;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final vocabFilterAsync =
        ref.watch(vocabFiltersNotifierProvider(ocrImage.lang1));
    final uiImageAsync = ref.watch(uiImageProvider(ocrImage.thumbnail));
    return Card(
      elevation: 2,
      child: ListTile(
        onTap: () {
          context.pushNamed(
            ReaderPage.name,
            params: {'sid': ocrImage.id.toString()},
          );
          //print("OCRImage id is : ${ocrImage.id} ");
          /* context.pushNamed(OCRPage.reader,
                queryParams: {"from": "stories"},
                params: {'sid': ocrImage.id.toString()}); */
        },
        leading: vocabFilterAsync.maybeWhen(
          orElse: () => const CircularProgressIndicator(),
          data: (data) => IconButton(
            icon: const Icon(Icons.read_more_outlined),
            onPressed: () {
              ref.read(vocabStateProvider.notifier).displayLang =
                  ocrImage.lang1;
              ref.read(vocabStateProvider);
              ref
                  .read(
                    vocabFiltersNotifierProvider(ocrImage.lang1).notifier,
                  )
                  .upsertFilter(
                    'story',
                    vocabularyOf(
                      id: ocrImage.id!,
                      title: ocrImage.title!,
                    ),
                  );
              ref.read(vocabFiltersNotifierProvider(ocrImage.lang1));
              context.goNamed(VocabularyPage.name);
            },
          ),
        ),
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8),
              child: SizedBox(
                width: 120,
                child: uiImageAsync.maybeWhen(
                  orElse: () => const CircularProgressIndicator(),
                  data: (image) => SizedBox(
                    child: RawImage(
                      image: image,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                ),
              ),
            ),
            Flexible(child: Text(ocrImage.title ?? 'Untitled'))
          ],
        ),
        subtitle: Text(
          ocrImage.langString
              .split(',')
              .map((e) => Language.fromIsoCode(e).name)
              .join('. '),
          style: Theme.of(context).textTheme.labelMedium,
        ), // lang name??
        trailing: !isConnected
            ? null
            : GestureDetector(
                onTap: () {
                  context.pushNamed(
                    OCRPage.editor,
                    queryParams: {'from': 'stories'},
                    params: {'sid': ocrImage.id.toString()},
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Icon(
                    MdiIcons.pencil,
                    color: Theme.of(context).textTheme.bodyLarge!.color,
                  ),
                ),
              ),
      ),
    );
  }
}
