import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import '../common/widgets/error_view.dart';
import '../common/widgets/loading_view.dart';
import 'widgets/stories_view.dart';

class StoriesPage extends ConsumerWidget {
  const StoriesPage({
    Key? key,
  }) : super(key: key);
  static const name = 'stories';
  static const String path = '/$name';
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final storyProvider = ref.watch(storiesNotifierProvider);

    return storyProvider.when(
      loading: () => const LoadingView(
        title: 'Read and Learn',
      ),
      error: (err, _) => ErrorView(
        errorMessage: err.toString(),
      ),
      data: (stories) => StoriesView(stories: stories),
    );
  }
}
