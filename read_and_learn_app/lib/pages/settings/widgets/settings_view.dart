import 'package:flutter/material.dart';

import '../../common/widgets/drawer_widget.dart';
import '../../common/widgets/view_wrapper.dart';
import '../../sync/widgets/sync_button.dart';
import 'audio_settings.dart';
import 'dark_mode.dart';
import 'learning_languages.dart';
import 'native_language.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewWrapper(
      title: 'Settings',
      actions: const [SyncButton()],
      drawer: const AppDrawer(),
      body: ListView(
        children: const [
          DarkModeSetting(),
          NativeLanguage(),
          LearningLanguages(),
          AudioSettings(),
        ],
      ),
    );
  }
}
