import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'dark_mode_switch.dart';

class DarkModeSetting extends StatelessWidget {
  const DarkModeSetting({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: ListTile(
        leading: const Icon(MdiIcons.themeLightDark),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Transform.translate(
              offset: const Offset(-24, 0),
              child: Text(
                'Dark Mode',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            ),
            const DarkModeModifier()
          ],
        ),
      ),
    );
  }
}
