import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class DarkModeModifier extends ConsumerWidget {
  const DarkModeModifier({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final darkModeProvider = ref.watch(prefNotifierProvider('dark_mode'));

    return darkModeProvider.maybeWhen(
      data: (pref) {
        final value = pref.value;

        final darkMode = ThemeModePref.str2enum(value);
        return DropdownButton<ThemeMode>(
          value: darkMode,
          items: [ThemeMode.system, ThemeMode.dark, ThemeMode.light]
              .map(
                (e) => DropdownMenuItem<ThemeMode>(
                  value: e,
                  child: Text(
                    ThemeModePref.enum2str(e),
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
              )
              .toList(),
          onChanged: (newValue) => setDarkMode(ref as Ref, newValue),
        );
        /* return Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [DarkMode.auto, DarkMode.on, DarkMode.off]
                .map((e) => Expanded(
                      child: RadioWithLabel(
                          value: e,
                          selection: darkMode,
                          onChanged: (DarkMode? newValue) =>
                              setDarkMode(ref, newValue)),
                    ))
                .toList(),
          ); */
      },
      error: (err, _) {
        return const Icon(MdiIcons.alert);
      },
      orElse: () => const Icon(MdiIcons.alert),
    );
  }

  void setDarkMode(Ref ref, ThemeMode? themeMode) {
    if (themeMode != null) {
      final prefValue = ThemeModePref.enum2str(themeMode);

      ref.read(prefNotifierProvider('dark_mode').notifier).set(prefValue);
    }
  }
}

class RadioWithLabel extends StatelessWidget {
  const RadioWithLabel({
    required this.value,
    required this.selection,
    required this.onChanged,
    Key? key,
  }) : super(key: key);
  final ThemeMode value;
  final ThemeMode selection;
  final Function(ThemeMode? newValue) onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(ThemeModePref.enum2str(value)),
        Radio(
          // title: Text(DarkMode.enum2str(DarkMode.auto)),
          value: value,
          groupValue: selection,
          onChanged: onChanged,
        ),
      ],
    );
  }
}

class DarkModeModifier2 extends ConsumerWidget {
  const DarkModeModifier2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final darkModeProvider = ref.watch(prefNotifierProvider('dark_mode'));

    return darkModeProvider.maybeWhen(
      data: (pref) {
        final value = pref.value;

        final themeMode = ThemeModePref.str2enum(value);

        return IconButton(
          onPressed: (themeMode == ThemeMode.system)
              ? null
              : () {
                  ref.read(prefNotifierProvider('dark_mode').notifier).set(
                        ThemeModePref.enum2str(
                          themeMode == ThemeMode.dark
                              ? ThemeMode.light
                              : ThemeMode.dark,
                        ),
                      );
                },
          icon: Icon(
            (themeMode == ThemeMode.system)
                ? MdiIcons.themeLightDark
                : (themeMode == ThemeMode.light)
                    ? Icons.nightlight
                    : Icons.wb_sunny,
          ),
        );
      },
      error: (err, _) => const Icon(MdiIcons.alert),
      orElse: () => const IconButton(
        onPressed: null,
        icon: Icon(MdiIcons.themeLightDark),
      ),
    );
  }
}
