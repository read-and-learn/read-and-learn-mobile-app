import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:language_picker/language_picker_dialog.dart';
import 'package:language_picker/languages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LearningLanguages extends ConsumerStatefulWidget {
  const LearningLanguages({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _LearningLanguagesState();
}

class _LearningLanguagesState extends ConsumerState<LearningLanguages> {
  bool isExpanded = true;
  Future<void> deleteLang(LangPref langPref, String lang) async {
    await showOkCancelAlertDialog(
      context: context,
      message:
          'Are you sure that you want to remove ${Language.fromIsoCode(lang).name} ',
      okLabel: 'Yes',
      cancelLabel: 'No',
    ).then((result) {
      if (result == OkCancelResult.ok) {
        ref.read(prefNotifierProvider('lang_pref').notifier).set(
              langPref
                  .copyWith(
                    learningLanguages: {
                      ...langPref.learningLanguages.where(
                        (element) => element != lang,
                      )
                    }.toList(),
                  )
                  .toJson(),
            );
        LocalTextStrings(lang: lang).clear();
      }
    });
  }

  Future<void> makeDefault(
    BuildContext ctx,
    LangPref langPref,
    String? lang,
  ) async {
    ref
        .read(prefNotifierProvider('lang_pref').notifier)
        .set(langPref.copyWith(currLearningLang: lang).toJson());
  }

  @override
  Widget build(BuildContext context) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    return langPrefProvider.maybeWhen(
      data: (pref) {
        final langPref = LangPref.fromJson(pref.value);
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: Theme(
            data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
            child: ExpansionTile(
              leading: Icon(
                MdiIcons.airplaneLanding,
                color: Theme.of(context).textTheme.bodySmall!.color,
              ),
              textColor: Theme.of(context).textTheme.bodySmall!.color,
              collapsedTextColor: Theme.of(context).textTheme.bodySmall!.color,
              title: Transform.translate(
                offset: const Offset(-24, 0),
                child: Text(
                  'The Languages I learn',
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ),
              subtitle: !isExpanded ? const Text('Tap to expand') : null,
              trailing: Icon(
                isExpanded
                    ? MdiIcons.arrowDownDropCircleOutline
                    : MdiIcons.arrowLeftDropCircleOutline,
                color: Theme.of(context).textTheme.bodySmall!.color,
              ),
              initiallyExpanded: isExpanded,
              onExpansionChanged: (expanded) {
                setState(() => isExpanded = expanded);
              },
              children: [
                ...langPref.learningLanguages.map(
                  (e) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: Slidable(
                      key: ValueKey('learninglang_$e'),
                      startActionPane: ActionPane(
                        motion: const BehindMotion(),
                        children: [
                          SlidableAction(
                            onPressed: (ctx) {
                              deleteLang(langPref, e);
                            },
                            icon: MdiIcons.delete,
                            foregroundColor: Colors.red.shade300,
                            label: 'Remove',
                          ),
                          SlidableAction(
                            onPressed: (ctx) {
                              makeDefault(ctx, langPref, e);
                            },
                            icon: MdiIcons.heartBoxOutline,
                            label: 'Default',
                          ),
                        ],
                      ),
                      child: ListTile(
                        leading: IconButton(
                          onPressed: () {},
                          icon: const Icon(MdiIcons.autorenew),
                        ),
                        title: Transform.translate(
                          offset: Offset.zero,
                          child: Text(
                            Language.fromIsoCode(e).name,
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                        ),
                        subtitle: (e == langPref.currLearningLang)
                            ? const Text('default')
                            : null,
                      ),
                    ),
                  ),
                ),
                ListTile(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 40),
                  leading: const Text(
                    '  ',
                  ),
                  title: Transform.translate(
                    offset: Offset.zero,
                    child: Text(
                      (langPref.learningLanguages.isEmpty)
                          ? 'Add a language'
                          : 'Add another language',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  onTap: () async {
                    await showDialog(
                      barrierColor: Colors.transparent,
                      context: context,
                      builder: (context) => LanguagePickerDialog(
                        isDividerEnabled: true,
                        titlePadding: const EdgeInsets.all(8),
                        searchInputDecoration:
                            const InputDecoration(hintText: 'Search...'),
                        isSearchable: true,
                        title: const Text('Select language'),
                        onValuePicked: (lang) {
                          ref
                              .read(
                                prefNotifierProvider('lang_pref').notifier,
                              )
                              .set(
                                langPref
                                    .copyWith(
                                      learningLanguages: {
                                        lang.isoCode,
                                        ...langPref.learningLanguages
                                      }.toList(),
                                    )
                                    .toJson(),
                              );
                          LocalTextStrings(lang: lang.isoCode).clear();
                          // ignore: unused_result
                          ref.refresh(
                            textStringsNotifierProvider(lang.isoCode),
                          );
                        },
                        itemBuilder: (lang) => Text(lang.name),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      },
      orElse: Container.new,
    );
  }
}
