import 'package:flutter/material.dart';
import 'package:language_picker/language_picker.dart';

Future<void> onSelectLanguage(
  BuildContext context, {
  required void Function(String langISOCode) onSelection,
}) async {
  await showDialog(
    barrierColor: Colors.transparent,
    context: context,
    builder: (context) => LanguagePickerDialog(
      isDividerEnabled: true,
      titlePadding: const EdgeInsets.all(8),
      searchInputDecoration: const InputDecoration(hintText: 'Search...'),
      isSearchable: true,
      title: const Text('Select language'),
      onValuePicked: (lang) => onSelection(lang.isoCode),
      itemBuilder: (lang) => Text(lang.name),
    ),
  );
}
/*
Future<void> Function(BuildContext context,
    {required void Function(String langISOCode) onSelection}) onSelectLanguage;
*/
