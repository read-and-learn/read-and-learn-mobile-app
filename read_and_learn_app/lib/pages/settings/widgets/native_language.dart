import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:language_picker/language_picker.dart';
import 'package:language_picker/languages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class NativeLanguage extends ConsumerWidget {
  const NativeLanguage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    return langPrefProvider.maybeWhen(
      data: (pref) {
        final langPref = LangPref.fromJson(pref.value);
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: ListTile(
            leading: const Icon(MdiIcons.airplaneTakeoff),
            title: Transform.translate(
              offset: const Offset(-24, 0),
              child: Text.rich(
                TextSpan(
                  text: 'My language is ',
                  children: [
                    TextSpan(
                      text: Language.fromIsoCode(langPref.nativeLanguage).name,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const TextSpan(
                      text: '. ',
                    ),
                    const TextSpan(
                      text: 'Change?',
                    )
                  ],
                ),
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            ),
            subtitle: Transform.translate(
              offset: const Offset(-24, 0),
              child: Text(
                ' The language to write meaning and notes.',
                style: Theme.of(context).textTheme.bodySmall,
              ),
            ),
            //isThreeLine: true,
            onTap: () async {
              await showDialog(
                barrierColor: Colors.transparent,
                context: context,
                builder: (context) => LanguagePickerDialog(
                  isDividerEnabled: true,
                  titlePadding: const EdgeInsets.all(8),
                  searchInputDecoration:
                      const InputDecoration(hintText: 'Search...'),
                  isSearchable: true,
                  title: const Text('Select language'),
                  onValuePicked: (lang) {
                    ref.read(prefNotifierProvider('lang_pref').notifier).set(
                          langPref
                              .copyWith(nativeLanguage: lang.isoCode)
                              .toJson(),
                        );
                  },
                  itemBuilder: (lang) => Text(lang.name),
                ),
              );
            },
          ),
        );
      },
      orElse: () => const Text('Something wrong??'),
    );
  }
}
