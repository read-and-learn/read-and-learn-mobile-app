import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class AudioSettings extends ConsumerStatefulWidget {
  const AudioSettings({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState<AudioSettings> createState() => _AudioSettingsState();
}

class _AudioSettingsState extends ConsumerState<AudioSettings> {
  bool isExpanded = true;
  late AudioPref audioPref;
  @override
  void initState() {
    audioPref = AudioPref();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    {
      final audioPrefProvider = ref.watch(prefNotifierProvider('audio_pref'));
      final pref = audioPrefProvider.maybeWhen(
        data: (pref) => pref.value,
        orElse: () => null,
      );
      if (pref != null) {
        audioPref = AudioPref.fromJson(pref);
      }
    }

    return Theme(
      data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        child: ExpansionTile(
          leading: Icon(
            MdiIcons.playSpeed,
            color: Theme.of(context).textTheme.bodySmall!.color,
          ),
          textColor: Theme.of(context).textTheme.bodySmall!.color,
          collapsedTextColor: Theme.of(context).textTheme.bodySmall!.color,
          title: Transform.translate(
            offset: const Offset(-24, 0),
            child: Text(
              'Audio Settings',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ),
          subtitle:
              Text('Rate: ${audioPref.rate}, Volume: ${audioPref.volume}'),
          trailing: Icon(
            isExpanded
                ? MdiIcons.arrowDownDropCircleOutline
                : MdiIcons.arrowLeftDropCircleOutline,
            color: Theme.of(context).textTheme.bodySmall!.color,
          ),
          initiallyExpanded: isExpanded,
          onExpansionChanged: (expanded) {
            setState(() => isExpanded = expanded);
          },
          childrenPadding: const EdgeInsets.only(left: 16, right: 16 + 16),
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Rate',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {
                    ref
                        .read(prefNotifierProvider('audio_pref').notifier)
                        .set(audioPref.copyWith(rate: 1).toJson());
                  },
                  child: Text(
                    'Reset',
                    style: Theme.of(context).textTheme.labelLarge,
                  ),
                )
              ],
            ),
            Slider.adaptive(
              min: 0.5,
              max: 2,
              divisions: 150,
              value: audioPref.rate,
              onChanged: (newRate) {
                ref
                    .read(prefNotifierProvider('audio_pref').notifier)
                    .set(audioPref.copyWith(rate: newRate).toJson());
              },
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Volume',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {
                    ref
                        .read(prefNotifierProvider('audio_pref').notifier)
                        .set(audioPref.copyWith(volume: 1).toJson());
                  },
                  child: Text(
                    'Reset',
                    style: Theme.of(context).textTheme.labelLarge,
                  ),
                )
              ],
            ),
            Slider.adaptive(
              min: 0.5,
              max: 2, // Make it platform specific
              divisions: 150,
              value: audioPref.volume,
              onChanged: (newVolume) {
                ref
                    .read(prefNotifierProvider('audio_pref').notifier)
                    .set(audioPref.copyWith(volume: newVolume).toJson());
              },
            )
          ],
        ),
      ),
    );
  }
}
