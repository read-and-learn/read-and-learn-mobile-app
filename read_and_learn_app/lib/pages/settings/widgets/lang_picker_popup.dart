import 'package:flutter/material.dart';
import 'package:language_picker/languages.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class LangPickerPopup extends StatelessWidget {
  const LangPickerPopup({
    required this.onChanged,
    required this.lang,
    required this.langList,
    required this.title,
    Key? key,
  }) : super(key: key);
  final Widget title;
  final String lang;
  final Function(String langCode) onChanged;
  final List<String> langList;

  @override
  Widget build(BuildContext context) {
    if (langList.length == 1) {
      return Center(child: title);
    }
    return PopupMenuButton<Language?>(
      icon: title,
      onSelected: (lang) {
        if (lang != null) {
          onChanged(lang.isoCode);
        }
      },
      onCanceled: () {},
      itemBuilder: (context) {
        return [
          const PopupMenuItem<Language>(
            child: Align(
              alignment: Alignment.centerRight,
              child: Icon(Icons.close),
            ),
          ),
          ...langList.map(
            (e) => PopupMenuItem<Language>(
              value: Language.fromIsoCode(e),
              child: ListTile(
                leading: e == lang
                    ? const Icon(MdiIcons.check)
                    : const Icon(MdiIcons.select),
                title: Text(Language.fromIsoCode(e).name),
              ),
            ),
          )
        ];
      },
    );
  }
}
