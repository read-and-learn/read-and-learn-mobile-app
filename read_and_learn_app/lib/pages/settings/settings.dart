import 'package:flutter/material.dart';

import 'widgets/settings_view.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key? key}) : super(key: key);
  static const name = 'settings';
  static const String path = '/$name';

  @override
  Widget build(BuildContext context) {
    return const SettingsView();
  }
}
