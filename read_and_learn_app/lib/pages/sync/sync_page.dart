import 'package:flutter/material.dart';
import 'widgets/sync_view.dart';

class SyncPage extends StatelessWidget {
  const SyncPage({Key? key}) : super(key: key);
  static const name = 'sync-screen';
  static const String path = '/$name';
  @override
  Widget build(BuildContext context) {
    return const SyncView();
  }
}
