import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';

import 'sync_lang.dart';
import 'sync_monitor.dart';
import 'sync_stories.dart';

class SyncMain extends StatelessWidget {
  const SyncMain({
    required this.langPref,
    required this.onComplete,
    Key? key,
  }) : super(key: key);

  final LangPref langPref;
  final Function() onComplete;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text('Please wait ...'),
                const SizedBox(
                  height: 8,
                ),
                Column(
                  children: [
                    const SyncStories(),
                    ...langPref.learningLanguages
                        .map(
                          (lang) => SyncLang(lang: lang),
                        )
                        .toList(),
                    SyncMonitor(
                      langs: langPref.learningLanguages,
                      onComplete: onComplete,
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
