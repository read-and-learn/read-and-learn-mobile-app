import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';

import '../../common/widgets/view_wrapper.dart';
import '../../stories/stories_page.dart';
import 'sync_main.dart';

class SyncView extends ConsumerWidget {
  const SyncView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final syncStatusProvider = ref.watch(prefNotifierProvider('sync_status'));

    return ViewWrapper(
      title: 'Syncing your data',
      body: langPrefProvider.when(
        loading: () => const CLLoading(),
        error: (err, stack) => ShowError(
          errorMessage: err.toString(),
        ),
        data: (langPrefString) {
          return syncStatusProvider.when(
            loading: () => const CLLoading(),
            error: (err, stack) => ShowError(
              errorMessage: err.toString(),
            ),
            data: (syncStatusPrefString) {
              final langPref = LangPref.fromJson(langPrefString.value);
              // ignore: unused_local_variable
              final syncStatus =
                  SyncStatus.fromJson(syncStatusPrefString.value);

              return SyncMain(
                langPref: langPref,
                onComplete: () {
                  context.goNamed(StoriesPage.name);
                },
              );
            },
          );
        },
      ),
    );
  }
}
