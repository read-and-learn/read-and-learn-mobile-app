import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SyncLang extends ConsumerWidget {
  const SyncLang({
    required this.lang,
    Key? key,
  }) : super(key: key);
  final String lang;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final langProvider = ref.watch(textStringsNotifierProvider(lang));
    return ListTile(
      leading: langProvider.when(
        data: (data) => const Icon(
          MdiIcons.checkBold,
          color: Colors.green,
        ),
        error: (err, _) => Icon(
          MdiIcons.alert,
          color: Theme.of(context).errorColor,
        ),
        loading: () => const CircularProgressIndicator(),
      ),
      title: langProvider.when(
        data: (words) => Text(
          '$lang: Sync Completed, ${words?.items.length ?? 0} words synced',
        ),
        error: (err, _) => Text('$lang: Sync Failed, Error: ${err.toString()}'),
        loading: () => Text('$lang: Syncing'),
      ),
    );
  }
}
