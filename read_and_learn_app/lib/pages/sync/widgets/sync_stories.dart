import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SyncStories extends ConsumerWidget {
  const SyncStories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final storiesProvider = ref.watch(storiesNotifierProvider);
    return ListTile(
      leading: storiesProvider.when(
        data: (data) => const Icon(
          MdiIcons.checkBold,
          color: Colors.green,
        ),
        error: (err, _) => Icon(
          MdiIcons.alert,
          color: Theme.of(context).errorColor,
        ),
        loading: () => const CircularProgressIndicator(),
      ),
      title: storiesProvider.when(
        data: (stories) => Text(
          'Stories: Sync Completed, ${stories.stories.length} stories found',
        ),
        error: (err, _) =>
            Text('Stories: Sync Failed, Error: ${err.toString()}'),
        loading: () => const Text('Stories: Syncing'),
      ),
    );
  }
}
