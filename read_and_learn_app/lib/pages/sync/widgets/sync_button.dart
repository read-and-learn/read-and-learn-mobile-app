import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'sync_main.dart';

class SyncButton extends ConsumerWidget {
  const SyncButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final syncStatusProvider = ref.watch(prefNotifierProvider('sync_status'));

    return langPrefProvider.when(
      loading: () => const CLLoading(),
      error: (err, stack) => ShowError(
        errorMessage: err.toString(),
      ),
      data: (langPrefString) {
        return syncStatusProvider.when(
          loading: () => const CLLoading(),
          error: (err, stack) => ShowError(
            errorMessage: err.toString(),
          ),
          data: (syncStatusPrefString) {
            final langPref = LangPref.fromJson(langPrefString.value);
            // ignore: unused_local_variable
            final syncStatus = SyncStatus.fromJson(syncStatusPrefString.value);

            return CardMenuItem(
              onPressed: () async {
                await LocalStories().clear();
                for (final lang in langPref.learningLanguages) {
                  await LocalTextStrings(lang: lang).clear();
                }
                // ignore: unused_result
                ref.refresh(storiesNotifierProvider);
                for (final lang in langPref.learningLanguages) {
                  // ignore: unused_result
                  ref.refresh(textStringsNotifierProvider(lang));
                }
                await showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) => AlertDialog(
                    content: SyncMain(
                      langPref: langPref,
                      onComplete: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                );
              },
              text: 'sync all',
              iconData: MdiIcons.autorenew,
            );
          },
        );
      },
    );
  }
}
