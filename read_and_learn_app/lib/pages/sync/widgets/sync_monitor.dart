import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class SyncMonitor extends ConsumerWidget {
  const SyncMonitor({
    required this.langs,
    required this.onComplete,
    Key? key,
  }) : super(key: key);
  final List<String> langs;
  final Function() onComplete;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final async2Monitor = [
      ref.watch(storiesNotifierProvider),
      ...langs
          .map(
            (lang) => ref.watch(textStringsNotifierProvider(lang)),
          )
          .toList()
    ];

    return (async2Monitor.any((e) => e is AsyncLoading))
        ? const ElevatedButton(onPressed: null, child: Text('wait'))
        : ElevatedButtonWithTimer(
            onPressed: onComplete,
            label: (async2Monitor.any((e) => e is AsyncError))
                ? 'Sync Completed with Error'
                : 'Sync Completed',
          );
  }
}
