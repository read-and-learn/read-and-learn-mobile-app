import 'package:data_storage/data_storage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:read_and_learn/pages/common/widgets/drawer_widget.dart';
import 'package:read_and_learn/pages/common/widgets/view_wrapper.dart';

import '../authenticate/server_uri_page.dart';

class OfflinePage extends ConsumerWidget {
  const OfflinePage({Key? key}) : super(key: key);
  static const String name = 'offline';
  static const String path = '/$name';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final auth = ref.watch(authNotifierProvider);
    return ViewWrapper(
      title: 'Offline...',
      drawer: const AppDrawer(),
      body: OfflineMessage(server: auth!.server),
    );
  }
}

class OfflineMessage extends ConsumerWidget {
  const OfflineMessage({
    required this.server,
    Key? key,
  }) : super(key: key);

  final String? server;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            MdiIcons.wifiOff,
            color: Theme.of(context).primaryColor.withAlpha(150),
            size: Theme.of(context).textTheme.bodyLarge!.fontSize! * 4,
          ),
          const SizedBox(height: 8),
          Text.rich(
            TextSpan(
              children: [
                if (server == ref.read(defaultServerURIProvider))
                  const TextSpan(
                    text: 'Sandbox server',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                else
                  TextSpan(
                    text: " server: ${server ?? "unknown"}",
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                const TextSpan(
                  text:
                      ' is currently offline. Check your internet connection or \n',
                ),
                TextSpan(
                  text: 'click here',
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      context.goNamed(ServerURIPage.name);
                    },
                  style: TextStyle(color: Colors.blue.shade300),
                ),
                const TextSpan(text: ' to change server')
              ],
            ),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ],
      ),
    );
  }
}
