import 'dart:io';

import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:dart_hocr/dart_hocr.dart';
import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:image_handler/image_handler.dart';
import 'package:read_and_learn/recieve_shared/providers/image_bucket.dart';

import '../common/widgets/loading_view.dart';
import '../ocr/ocr_handler.dart';
import '../ocr/ocr_main.dart';
import '../settings/widgets/lang_select.dart';

class ImageHandlerPage extends ConsumerWidget {
  const ImageHandlerPage({super.key, this.pushedFrom});
  static const String name = 'receive_image';
  static const String path = '/$name';

  final String? pushedFrom;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    return langPrefProvider.maybeWhen(
      orElse: () => const LoadingView(),
      data: (pref) {
        return _ImageHandlerPage(
          pushedFrom: pushedFrom,
          langPref: LangPref.fromJson(pref.value),
        );
      },
    );
  }
}

class _ImageHandlerPage extends ConsumerStatefulWidget {
  const _ImageHandlerPage({
    required this.langPref,
    this.pushedFrom,
  });
  final String? pushedFrom;
  final LangPref langPref;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      __ImageHandlerPageState();
}

class __ImageHandlerPageState extends ConsumerState<_ImageHandlerPage>
    with OCRHandler {
  OCRImage? ocrImage;
  bool isSaving = false;
  @override
  Widget build(BuildContext context) {
    final imageBucket = ref.watch(imageBucketProvider);
    final externalImagePath =
        imageBucket.isNotEmpty() ? imageBucket.imagePathList[0] : null;

    if (ocrImage == null) {
      return CLFullscreenBox(
        child: ImageHandlerView(
          externalImagePath: externalImagePath,
          onImageReady: onImageReady,
          processingTimeoutImageReady: RestApi.uploadimageTimeout,
          onDiscardImage: (ctx) => onDiscardImage(),
          defaultLanguageCode: widget.langPref.currLearningLang,
          onSelectLanguage: onSelectLanguage,
          onOpenTextEditor:
              (widget.pushedFrom != null) ? onOpenTextEditor : null,
        ),
      );
    }
    return Stack(
      children: [
        OCRMain(
          edit: true,
          langPref: widget.langPref,
          ocrImage: ocrImage!,
          pushedFrom: widget.pushedFrom,
        )
      ],
    );
  }

  Future<bool> onImageReady({
    required String imagePath,
    required String lang1,
    required String? lang2,
  }) async {
    try {
      // throw Exception("Testing snackbar");
      final ocrImage = await OCRImage.fromImageData(
        image: File(imagePath).readAsBytesSync(),
        lang1: lang1,
        lang2: lang2,
        ocrProcessAPI: RestApi('https://api.cloudonlanapps.in').uploadimage,
      );
      setState(() {
        this.ocrImage = ocrImage;
      });
      return true;
    } on Exception catch (e) {
      ReportError.snackBar(
        context,
        errorString: 'Unable to process the image now.\nTry Later.',
        details: e.toString().replaceAll('Exception:', '').trim(),
        onClipboardCopy: (text) async => ClipboardManager.copy(text),
      );
      return false;
    }
  }

  void onDiscardImage() {
    setState(() {
      ocrImage = null;
    });
    if (widget.pushedFrom != null) {
      context.pop();
    } else {
      final imageBucket = ref.watch(imageBucketProvider);
      final externalImagePath =
          imageBucket.isNotEmpty() ? imageBucket.imagePathList[0] : null;

      if (externalImagePath != null) {
        ref.read(imageBucketProvider.notifier).removeImage();
      }
    }
  }

  void onOpenTextEditor() {
    if (widget.pushedFrom != null) {
      context
        ..pop()
        ..pushNamed('NewStory', queryParams: {'from': widget.pushedFrom});
    } else {
      // as of now, this is not used
      context.goNamed('NewStory');
    }
  }
}
