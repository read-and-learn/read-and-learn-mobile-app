import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common/widgets/error_view.dart';

import '../common/widgets/loading_view.dart';
import 'widgets/word_view.dart';

class WordPage extends ConsumerWidget {
  const WordPage({
    required this.text,
    this.from,
    Key? key,
  }) : super(key: key);
  static const String name = 'word';
  static const String path = '/$name';
  final String? from;
  final String text;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));

    final textStringsAsync = ref.watch(textStringsNotifierProvider(lang));

    return langPrefProvider.when(
      loading: () => const LoadingView(),
      error: (err, _) => ErrorView(errorMessage: err.toString()),
      data: (pref) {
        return textStringsAsync.when(
          loading: () => const LoadingView(),
          error: (err, _) => ErrorView(errorMessage: err.toString()),
          data: (textStrings) {
            if (textStrings == null) {
              return const ErrorView(
                errorMessage: "textStrings can't be null",
              );
            }
            final langPref = LangPref.fromJson(pref.value);
            final learnersLang = langPref.nativeLanguage;

            return WordView(
              textString: textStrings.items[text]!,
              learnersLang: learnersLang,
              from: from,
            );
          },
        );
      },
    );
  }
}
