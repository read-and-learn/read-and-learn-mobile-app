import 'package:basic_services/basic_services.dart';
import 'package:data_storage/data_storage.dart';
import 'package:dynamic_form/dynamic_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:read_and_learn/pages/common/widgets/drawer_widget.dart';

import '../../common/widgets/app_theme.dart';
import '../../common/widgets/view_wrapper.dart';
import 'learn_empty_view.dart';
import 'textstring_form_handler.dart';
import 'word_card.dart';

class LearnView extends ConsumerStatefulWidget {
  const LearnView({
    required this.textStrings,
    Key? key,
  }) : super(key: key);
  final TextStrings textStrings;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _LearnViewState();
}

class _LearnViewState extends ConsumerState<LearnView> {
  late PageController pageController;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    pageController = PageController();
    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  void onDrawOpen() {
    scaffoldKey.currentState!.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    final learnState = ref.watch(learnStateNotifierProvider);

    if ((learnState.learnersLang == null) ||
        (learnState.vocabLang == null) ||
        learnState.words.isEmpty) {
      return const EmptyLearnView(
        showDrawer: true,
      );
    }
    final itemCount =
        ref.watch(learnStateNotifierProvider.select((value) => value.total));
    return AppTheme(
      child: ViewWrapper(
        scaffoldKey: scaffoldKey,
        drawer: const AppDrawer(),
        fullscreen: true,
        //showBottomSheet: false,
        title: null,
        body: Column(
          children: [
            Flexible(
              child: PageView.builder(
                controller: pageController,
                itemCount: itemCount,
                onPageChanged: (index) =>
                    ref.read(learnStateNotifierProvider.notifier).index = index,
                physics: const BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  return WordWrapper(
                    index: index,
                    textStrings: widget.textStrings,
                    onDrawOpen: onDrawOpen,
                  );
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  onPressed: () {
                    pageController.previousPage(
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeIn,
                    );
                  },
                  icon: const Icon(MdiIcons.arrowLeftThinCircleOutline),
                ),
                IconButton(
                  onPressed: () {
                    pageController.nextPage(
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeIn,
                    );
                  },
                  icon: const Icon(MdiIcons.arrowRightThinCircleOutline),
                ),
              ],
            ),
            const Progress(),
            const Divider()
          ],
        ),
      ),
    );
  }
}

class WordWrapper extends ConsumerWidget {
  const WordWrapper({
    required this.onDrawOpen,
    required this.index,
    required this.textStrings,
    Key? key,
  }) : super(key: key);
  final int index;
  final TextStrings textStrings;
  final Function() onDrawOpen;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final learnState = ref.watch(learnStateNotifierProvider);

    if ((learnState.learnersLang == null) ||
        (learnState.vocabLang == null) ||
        learnState.words.isEmpty) {
      return const EmptyLearnView(
        showDrawer: true,
      );
    }

    final textString =
        textStrings.items[learnState.words[learnState.currIndex]]!;

    final formInstanceIdentifier = FormInstanceIdentifier(
      formHandler: TextStringFormHandler(
        textString: textString,
        learnerLang: learnState.learnersLang!,
      ),
      jsonFile: 'assets/lang_support/marathi.json',
    );

    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            title: Text(textString.text),
            leading: IconButton(
              icon: const Icon(MdiIcons.menu),
              onPressed: onDrawOpen,
            ),
            backgroundColor: Theme.of(context).canvasColor,
            actions: !isConnected
                ? null
                : [
                    Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: IconButton(
                        onPressed: () async {
                          await DictionaryManager.getDictionaryManager(
                            text: textString.text,
                            sL: textString.lang,
                            tL: learnState.learnersLang!,
                          ).launch(context);
                        },
                        icon: const Icon(Icons.translate),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 16),
                      child: FormEdit(
                        formInstanceIdentifier: formInstanceIdentifier,
                        textStyle: Theme.of(context).textTheme.headlineMedium!,
                      ),
                    ),
                  ],
          ),
        ];
      },
      body: WordCard(
        formInstanceIdentifier: formInstanceIdentifier,
      ),
    );
  }
}

class Progress extends ConsumerWidget {
  const Progress({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final currIndex = ref.watch(
          learnStateNotifierProvider.select((value) => value.currIndex),
        ) +
        1;
    final total =
        ref.watch(learnStateNotifierProvider.select((value) => value.total));
    return Text('$currIndex  / $total');
  }
}
