import 'package:basic_services/basic_services.dart';
import 'package:data_storage/data_storage.dart';
import 'package:dynamic_form/dynamic_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:read_and_learn/pages/common/widgets/app_theme.dart';
import 'package:read_and_learn/pages/common/widgets/view_wrapper.dart';

import '../../common/widgets/drawer_widget.dart';
import 'textstring_form_handler.dart';
import 'word_card.dart';

class WordView extends ConsumerWidget {
  const WordView({
    required this.textString,
    required this.learnersLang,
    this.from,
    Key? key,
  }) : super(key: key);
  final String? from;
  final TextString textString;
  final String learnersLang;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final formInstanceIdentifier = FormInstanceIdentifier(
      formHandler: TextStringFormHandler(
        textString: textString,
        learnerLang: learnersLang,
      ),
      jsonFile: 'assets/lang_support/marathi.json',
    );

    return AppTheme(
      child: ViewWrapper(
        title: textString.text,
        centerTitle: false,
        fullscreen: true,
        from: from,
        showBottomSheet: false,
        actions: !isConnected
            ? null
            : [
                Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: IconButton(
                    onPressed: () async {
                      await DictionaryManager.getDictionaryManager(
                        text: textString.text,
                        sL: textString.lang,
                        tL: learnersLang,
                      ).launch(context);
                    },
                    icon: const Icon(Icons.translate),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16),
                  child: FormEdit(
                    formInstanceIdentifier: formInstanceIdentifier,
                    textStyle: Theme.of(context).textTheme.headlineMedium!,
                  ),
                ),
              ],
        drawer: (from == null) ? const AppDrawer() : null,
        body: WordCard(formInstanceIdentifier: formInstanceIdentifier),
      ),
    );
  }
}
