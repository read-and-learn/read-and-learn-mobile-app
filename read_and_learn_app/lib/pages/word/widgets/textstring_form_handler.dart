import 'package:data_storage/data_storage.dart';
import 'package:dynamic_form/dynamic_form.dart';
import 'package:flutter/material.dart';

@immutable
class TextStringFormHandler implements FormHandler {
  const TextStringFormHandler({
    required this.textString,
    required this.learnerLang,
  });
  final TextString textString;
  final String learnerLang;

  TextStringFormHandler copyWith({
    TextString? textString,
    String? learnerLang,
  }) {
    return TextStringFormHandler(
      textString: textString ?? this.textString,
      learnerLang: learnerLang ?? this.learnerLang,
    );
  }

  @override
  String toString() =>
      'TextStringFormHandler(textString: {${textString.text},${textString.lang}} , learnerLang: $learnerLang)';

  @override
  bool operator ==(covariant TextStringFormHandler other) {
    if (identical(this, other)) {
      return true;
    }

    return other.textString == textString && other.learnerLang == learnerLang;
  }

  @override
  int get hashCode => textString.hashCode ^ learnerLang.hashCode;

  @override
  List<String> getCurrentValues(String key) {
    final attribute = string2Attr[key] ?? Attribute.unknown;
    // Meaning is written in learner's language

    final lang = [Attribute.meaning].contains(attribute)
        ? learnerLang
        : [Attribute.usage].contains(attribute)
            ? textString.lang
            : 'grammer';
    final feats = textString.getFeature(attribute, lang);

    final currentValue = feats.isNotEmpty ? [feats.first.value.text] : [];

    return currentValue as List<String>;
  }

  @override
  String? getType() {
    //final learnerLang = handle["learnerLang"] as String;
    return textTypes2Map(textString.type);
  }

  @override
  Future<TextString?> saveUpdates(Map<String, List<String>> updates) async {
    var updatedTextString = textString;

    for (final update in updates.entries) {
      final attribute = string2Attr[update.key] ?? Attribute.unknown;
      if (attribute != Attribute.unknown) {
        final lang = [Attribute.meaning].contains(attribute)
            ? learnerLang
            : [Attribute.usage].contains(attribute)
                ? textString.lang
                : 'grammer';
        final type = [Attribute.meaning].contains(attribute)
            ? (update.value[0].contains(' ') ? TextType.phrase : TextType.word)
            : [Attribute.usage].contains(attribute)
                ? TextType.sentence
                : TextType.phrase;

        // First remove the existing meaning
        updatedTextString = updatedTextString.removeFeature(attribute, lang);
        if (update.value.isNotEmpty && update.value[0].isNotEmpty) {
          updatedTextString = updatedTextString.addFeature(
            Feat(
              attr: attribute,
              value: TextString(text: update.value[0], lang: lang, type: type),
            ),
          );
        }
      } else {
        throw Exception('Unsupported feature, Please update server');
      }
    }
    if (updatedTextString != textString) {
      return updatedTextString;
    }

    return null;
  }
}
