import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../common/widgets/app_theme.dart';
import '../../common/widgets/drawer_widget.dart';
import '../../common/widgets/view_wrapper.dart';
import '../../vocabulary/vocabulary_page.dart';

class EmptyLearnView extends StatelessWidget {
  const EmptyLearnView({
    required this.showDrawer,
    Key? key,
  }) : super(key: key);
  final bool showDrawer;

  @override
  Widget build(BuildContext context) {
    return AppTheme(
      child: ViewWrapper(
        title: 'Learn',
        drawer: showDrawer ? const AppDrawer() : null,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Center(
                child: Text('Select words from Vocabulary page'),
              ),
              ElevatedButton(
                onPressed: () {
                  context.goNamed(VocabularyPage.name);
                },
                child: const Text('Goto Vocabulary'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
