import 'package:custom_widgets/custom_widgets.dart';
import 'package:data_storage/data_storage.dart';
import 'package:dynamic_form/dynamic_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:language_picker/languages.dart';

import 'textstring_form_handler.dart';

class WordCard extends ConsumerStatefulWidget {
  const WordCard({
    required this.formInstanceIdentifier,
    Key? key,
  }) : super(key: key);
  final FormInstanceIdentifier formInstanceIdentifier;

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WordCardState();
}

class _WordCardState extends ConsumerState<WordCard> {
  bool isSaving = false;
  @override
  void initState() {
    super.initState();
  }

  Future<void> onSave({required Map<String, List<String>> updates}) async {
    try {
      final updatedTextString = await widget.formInstanceIdentifier.formHandler
          .saveUpdates(updates) as TextString?;
      if (updatedTextString != null) {
        if (mounted) {
          await ref
              .read(
                textStringsNotifierProvider(updatedTextString.lang).notifier,
              )
              .save(updatedTextString, replace: true);
        }
        if (mounted) {
          await Future.delayed(const Duration(seconds: 3));
        }
      }
    } on Exception catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Could not save values: Error reported: ${e.toString()}',
          ),
        ),
      );
    }
    if (mounted) {
      setState(() {
        isSaving = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final isConnected = ref.watch(
      connectivityStateNotifierProvider.select((value) => value.isConnected),
    );
    final textStringFormHandler =
        widget.formInstanceIdentifier.formHandler as TextStringFormHandler;
    final textString = textStringFormHandler.textString;
    final learnerLang = textStringFormHandler.learnerLang;

    if (isSaving) {
      return const CLLoading(
        message: 'Saving',
      );
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Transform.scale(
                scale: 1,
                child: Transform.translate(
                  offset: Offset.zero,
                  child: RotationTransition(
                    turns: const AlwaysStoppedAnimation(-15 / 360),
                    child: Text(
                      Language.fromIsoCode(textString.lang).name,
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                ),
              ),
              if (learnerLang != textString.lang) ...[
                Transform.scale(
                  scale: 1,
                  child: Transform.translate(
                    offset: Offset.zero,
                    child: RotationTransition(
                      turns: const AlwaysStoppedAnimation(-15 / 360),
                      child: Text(
                        'to',
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ),
                  ),
                ),
                Transform.scale(
                  scale: 1,
                  child: Transform.translate(
                    offset: Offset.zero,
                    child: RotationTransition(
                      turns: const AlwaysStoppedAnimation(-15 / 360),
                      child: Text(
                        Language.fromIsoCode(learnerLang).name,
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  ),
                ),
              ]
            ],
          ),
          FormSave(
            formInstanceIdentifier: widget.formInstanceIdentifier,
            label: 'Save Changes?',
            icon: Icons.save,
            textStyle: Theme.of(context).textTheme.headlineLarge!,
            onPressed: isConnected
                ? ({required updates}) async {
                    setState(() {
                      isSaving = true;
                    });

                    await onSave(updates: updates);
                  }
                : null,
          ),
          Expanded(
            child: DynamicForm(
              key: ValueKey(widget.formInstanceIdentifier.hashCode),
              formInstanceIdentifier: widget.formInstanceIdentifier,
            ),
          ),
        ],
      ),
    );
  }
}
