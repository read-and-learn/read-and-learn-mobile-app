import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../common/widgets/error_view.dart';
import '../common/widgets/loading_view.dart';
import 'widgets/learn_view.dart';

class LearnPage extends ConsumerWidget {
  const LearnPage({Key? key, this.from, this.words}) : super(key: key);
  static const String name = 'learn';
  static const String path = '/$name';
  final String? from;
  final List<String>? words;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final lang =
        ref.watch(vocabStateProvider.select((value) => value.displayLang));
    final langPrefProvider = ref.watch(prefNotifierProvider('lang_pref'));
    final textStringsAsync = ref.watch(textStringsNotifierProvider(lang));
    final vocabSelectAsync = ref.watch(vocabSelectNotifierProvider(lang));

    return vocabSelectAsync.when(
      loading: () => const LoadingView(),
      error: (err, _) => ErrorView(errorMessage: err.toString()),
      data: (vacabSelect) {
        return langPrefProvider.when(
          loading: () => const LoadingView(),
          error: (err, _) => ErrorView(errorMessage: err.toString()),
          data: (pref) {
            return textStringsAsync.when(
              loading: () => const LoadingView(),
              error: (err, _) => ErrorView(errorMessage: err.toString()),
              data: (textStrings) {
                if (textStrings == null) {
                  return const ErrorView(
                    errorMessage: "textStrings can't be null",
                  );
                }

                return LearnView(textStrings: textStrings);
              },
            );
          },
        );
      },
    );
  }
}
