import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';

final boottimeSharedImagesProvider = FutureProvider<List<String>>((ref) async {
  final sharedMediaFiles = await ReceiveSharingIntent.getInitialMedia();
  if (sharedMediaFiles.isNotEmpty) {
    return sharedMediaFiles.map((e) => e.path).toList();
  }
  return [];
  // For testing
  /* 
  
  import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
  
  Directory directory = await getApplicationDocumentsDirectory();
  var dbPath = join(directory.path, "testimage.jpg");
  ByteData data = await rootBundle.load("assets/hindi1.JPG");
  List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  await File(dbPath).writeAsBytes(bytes);
  return [dbPath]; */
});
