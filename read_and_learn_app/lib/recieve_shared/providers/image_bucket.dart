// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'dart:async';

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';

import '../models/image_bucket.dart';
import 'boottime_shared_images.dart';

class ImageBucketNotifier extends StateNotifier<ImageBucket> {
  late final StreamSubscription? intentDataStreamSubscription;
  ImageBucketNotifier(List<String> initialImages)
      : super(ImageBucket(imagePathList: initialImages)) {
    intentDataStreamSubscription =
        ReceiveSharingIntent.getMediaStream().listen(listen);
  }
  @override
  void dispose() {
    intentDataStreamSubscription?.cancel();
    super.dispose();
  }

  Future<void> listen(List<SharedMediaFile> sharedMediaFiles) async {
    //print("received an image ${sharedMediaFiles[0].path}");
    state = state.copyWith(
      images: [...sharedMediaFiles.map((e) => e.path), ...state.imagePathList],
    );
  }

  void removeImage() {
    state = state.copyWith(images: state.imagePathList..removeAt(0));
  }
}

final imageBucketProvider =
    StateNotifierProvider<ImageBucketNotifier, ImageBucket>((ref) {
  return ref.watch(boottimeSharedImagesProvider).maybeWhen(
        orElse: () => ImageBucketNotifier([]),
        data: ImageBucketNotifier.new,
      );
});
