/// Run the application with ProviderScope
/// Implement a FutureProvider that invokes all initialization
/// routies as well trigger loading other providers
/// Watch this FutureProvider and once it gets results, draw the app
/// we can handle  errors if needed
/// replacing the FutureProvider by StreamProvider, we may also
/// show the progress
///

import 'dart:async';

import 'package:data_storage/data_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

// ignore: unused_import
import 'package:shared_preferences/shared_preferences.dart';

import 'logger.dart';
import 'router.dart';

const clearPreferences = false;
Future<bool> initializeApp(Ref ref) async {
  if (clearPreferences) {
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  } else {}

  ref.listen<Auth?>(authNotifierProvider, (previous, next) {
    if (previous == next) {
      _infoLogger('Auth change notified, but no change found');
    }
    if (next != null) {
      if (previous != null) {
        if (previous.server != next.server) {
          _infoLogger(
            'server changed from ${previous.server} => ${next.server}',
          );
        }
        if (previous.user != next.user) {
          _infoLogger('server changed from ${previous.user} => ${next.user}');
        }
      } else {
        _infoLogger('New authentication: $next');
      }
    }
  });

  return true;
}

final appInitProvider = FutureProvider<void>((ref) async {
  try {
    final result = await initializeApp(ref);
    if (!result) {
      throw Exception('Initialization Failed');
    }
  } on Exception {
    // Handle or rethrow
    rethrow;
  }
  return;
});

///
void main() {
  // ignore: unused_local_variable
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  //FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
  );
  return runApp(const ProviderScope(child: StartApp()));
}

class StartApp extends ConsumerWidget {
  const StartApp({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final appInitAsync = ref.watch(appInitProvider);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: appInitAsync.when(
        data: (success) {
          //FlutterNativeSplash.remove();
          return const RaLRouter();
        },
        error: (err, _) {
          //FlutterNativeSplash.remove();
          return ReportError(err: err.toString());
        },
        loading: () => const ShowProgress(),
      ),
    );
  }
}

class ShowProgress extends ConsumerWidget {
  const ShowProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Stack(
        children: const [
          Center(
            child: Image(image: AssetImage('assets/icon/Logo Design.002.png')),
          ),
          Center(child: CircularProgressIndicator()),
        ],
      ),
    );
  }
}

class ReportError extends ConsumerWidget {
  const ReportError({
    required this.err,
    Key? key,
  }) : super(key: key);
  final String err;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(title: const Text('Error!!')),
      body: const Center(child: Text('err')),
    );
  }
}

bool _disableInfoLogger = true;
void _infoLogger(String msg) {
  if (!_disableInfoLogger) {
    logger.i(msg);
  }
}
