import 'dart:math';

class Separators {
  static const symbols = r"\s?':;,.!।" '"';
  static const linebreaks = r'\?!\.\n\r।';
}

extension TextCleanup on String {
  String get summary {
    final result =
        "${replaceAll(RegExp("[${Separators.linebreaks}]*"), '').substring(0, min(length, 10))}"
        "${length > 10 ? "..." : ""}";

    return result;
  }

  String get symbolTrim {
    return replaceAll(
      RegExp('[${Separators.symbols}${Separators.linebreaks}]*\$'),
      '',
    ).replaceAll(
      RegExp('^[${Separators.symbols}${Separators.linebreaks}]*'),
      '',
    );
  }
}
