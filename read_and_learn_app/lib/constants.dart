class Constants {
  static const String appName = 'Read and Learn';

  //used by FlutterLogin
  static const String logoTag = 'loginsample.logo';
  static const String titleTag = 'loginsample.title';

  // Convert to Settings if needed
  static const double panelHeightClosedinPercentage = 0.05;
  static const double panelHeightOpenedinPercentage = .40;

  static const supportedWordInfoTags = ['meaning', 'usage'];
  static const foriegnLanguageWordInfoTags = ['word', 'usage'];
}
