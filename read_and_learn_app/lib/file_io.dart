import 'dart:io' as io;

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class CSVFilePicker {
  static Future<String?> pickFile() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'csv',
      ],
    );

    if (result == null || result.files.single.path == null) {
      return null;
    }
    return result.files.single.path;
  }
}

class FileIOHandler {
  FileIOHandler({
    required this.filename,
  });
  String filename;

  Stream<List<int>> read() {
    if (kIsWeb) {
      throw Exception(UnimplementedError);
    } else {
      return io.File(filename).openRead();
    }
  }
}
