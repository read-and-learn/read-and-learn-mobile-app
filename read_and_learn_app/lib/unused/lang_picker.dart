import 'package:flutter/material.dart';
import 'package:language_picker/languages.dart';
import 'package:language_picker/utils/typedefs.dart';

///Provides a customizable [DropdownButton] for all languages
class LangPicker extends StatefulWidget {
  const LangPicker({
    Key? key,
    this.itemBuilder,
    this.initialValue,
    this.onValuePicked,
    this.languages,
  }) : super(key: key);

  ///This function will be called to build the child of DropdownMenuItem
  ///If it is not provided, default one will be used which displays
  ///flag image, isoCode and phoneCode in a row.
  ///Check _buildDefaultMenuItem method for details.
  final ItemBuilder? itemBuilder;

  ///Preselected language.
  final Language? initialValue;

  ///This function will be called whenever a Language item is selected.
  final ValueChanged<Language?>? onValuePicked;

  /// List of languages available in this picker.
  final List<Language>? languages;

  @override
  LangPickerState createState() => LangPickerState();
}

class LangPickerState extends State<LangPicker> {
  late List<Language> _languages;
  late Language? _selectedLanguage;

  @override
  void initState() {
    _languages = widget.languages ?? Languages.defaultLanguages;
    if (widget.initialValue != null) {
      try {
        _selectedLanguage = _languages
            .firstWhere((language) => language == widget.initialValue!);
      } on Exception {
        throw Exception(
          'The initialValue is missing from the list of displayed languages!',
        );
      }
    } else {
      _selectedLanguage = null;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final items = <DropdownMenuItem<Language?>>[
      const DropdownMenuItem<Language?>(
        child: Center(child: Text('=== Select a Language ===')),
      ),
      ..._languages
          .map(
            (language) => DropdownMenuItem<Language>(
              value: language,
              child: widget.itemBuilder != null
                  ? widget.itemBuilder!(language)
                  : _buildDefaultMenuItem(language),
            ),
          )
          .toList()
    ];

    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(),
        color: Theme.of(context).bottomAppBarColor,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: DropdownButton<Language?>(
          onChanged: (value) {
            if (value != null) {
              setState(() {
                _selectedLanguage = value;
                widget.onValuePicked!(value);
              });
            }
          },
          items: items,
          value: _selectedLanguage,
        ),
      ),
    );
  }

  Widget _buildDefaultMenuItem(Language language) {
    return Text('${language.name} (${language.isoCode})');
  }
}
