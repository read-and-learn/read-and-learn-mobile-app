/* import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../internal/rest_api.dart';

class OCRAssetImage {
  String image;
  String lang1;
  String? lang2;
  String? title;
  String? xml;
  OCRAssetImage(
      {required this.image,
      required this.lang1,
      this.lang2,
      this.title,
      this.xml});
}

Future<List<OCRImage>> loadImages(List<OCRAssetImage> assetsImages) async {
  List<OCRImage> ocrImages = [];
  for (MapEntry asset in assetsImages.asMap().entries) {
    String? xmlString;
    if (asset.value.xml != null) {
      xmlString = await rootBundle.loadString(asset.value.xml);
    }
    ByteData bytes =
        await rootBundle.load(asset.value.image); //load sound from assets
    if (xmlString != null) {
      ocrImages.add(await OCRImage.fromOCRData(
          id: asset.key,
          title: asset.value.title,
          lang1: asset.value.lang1,
          lang2: asset.value.lang2,
          image: bytes.buffer
              .asUint8List(bytes.offsetInBytes, bytes.lengthInBytes),
          xmlString: xmlString));
    } else {
      ocrImages.add(await OCRImage.fromImageData(
          id: asset.key,
          title: asset.value.title,
          lang1: asset.value.lang1,
          lang2: asset.value.lang2,
          image: bytes.buffer
              .asUint8List(bytes.offsetInBytes, bytes.lengthInBytes),
          ocrProcessAPI: RestApi("https://api.cloudonlanapps.in").uploadimage));
    }
  }
  return ocrImages;
}

final ocrAssetImagesProvider =
    FutureProvider.family<List<OCRImage>, List<OCRAssetImage>>(
        (ref, assetsImages) async {
  return loadImages(assetsImages);
});
 */
