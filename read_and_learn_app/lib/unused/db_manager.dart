/* import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqlite_api.dart';

import 'package:async/async.dart';

/// -----------------------------------------------------------------------------
/// Errors :
/// ----------------------------------------------------------------------------
enum DBErrors {
  dbUnclassifiedError,
  dbNotCreated,
  dbTablesMismatch,
}

class DBException implements Exception {
  DBErrors errorString;
  DBException(this.errorString);
}

/// ----------------------------------------------------------------------------
/// Database Manager
/// ----------------------------------------------------------------------------
class DBManager {
  static final DBManager _dbManager = DBManager._();
  factory DBManager() {
    return _dbManager;
  }

  DBManager._();

  static Database? _dbInstance;
  final _initDBMemoizer = AsyncMemoizer<Database>();

  Future<Database> get db async {
    if (_dbInstance != null) {
      return _dbInstance as Database;
    }
    _dbInstance = await _initDBMemoizer.runOnce(() async {
      return await _initDB();
    });
    return _dbInstance as Database;
  }

  Future<Database> _initDB() async {
    final dbPath = await sql.getDatabasesPath();

    try {
      _dbInstance = await sql.openDatabase(
          path.join(
            dbPath,
            "ReadAndLearn.db",
          ),
          version: 1,
          onCreate: (Database db, int version) async {});
    } catch (error) {
      throw DBException(DBErrors.dbNotCreated);
    }

    return _dbInstance as sql.Database;
  }
}

class TableColumn {
  final String name;
  final bool isUnique;
  final bool isPrimary;
  final bool isForeignKey;
  final bool canBeNull;
  final bool isAutoIncrement;
  final String dataType;
  TableColumn(
    this.name, {
    this.isUnique = false,
    this.isPrimary = false,
    this.isAutoIncrement = false,
    this.canBeNull = true,
    this.dataType = "TEXT",
    this.isForeignKey = false,
  });

  @override
  String toString() {
    List<String> fields = [
      name,
      dataType,
      isPrimary ? "PRIMARY KEY" : "",
      canBeNull ? "" : "NOT NULL",
      isAutoIncrement ? "AUTOINCREMENT" : "",
    ];
    return fields.join(" ");
  }
}

mixin DBTable {
  late String tableName;
  late List<TableColumn> columns;
  late String columnList;

  //DBTable({required this.name, required this.columns, this.constraints});

  Future<void> createTable() async {
    String command = "CREATE TABLE $tableName (\n $columnList\n);";

    var database = await DBManager().db;
    debugPrint(command);
    database.execute(command);
  }

  Future<int> insert(Map<String, dynamic> entry) async {
    var database = await DBManager().db;
    if (!await isTableExists()) {
      await createTable();
    }

    return await database.insert(
      tableName,
      entry,
    );
  }

  Future<int> update(Map<String, dynamic> entry) async {
    var database = await DBManager().db;

    var res = await database.update(
      tableName,
      entry,
      where: 'id = ?',
      whereArgs: [entry["id"]],
    );

    return res;
  }

  Future<int> upsert(Map<String, dynamic> entry) async {
    if (entry.keys.contains("id")) {
      return update(entry);
    } else {
      return insert(entry);
    }
  }

  Future<List<Object?>> insertAll(List<Map<String, dynamic>> entries) async {
    var database = await DBManager().db;
    if (!await isTableExists()) {
      await createTable();
    }
    Batch batch = database.batch();

    for (var e in entries) {
      batch.insert(tableName, e);
    }
    return await batch.commit(continueOnError: true);
  }

  Future<bool> isTableExists() async {
    var database = await DBManager().db;
    var res = await database.query(
      "sqlite_master",
      columns: ["count(name)"],
      where: "type='table' AND name=?",
      whereArgs: [tableName],
    );

    return (res[0]["count(name)"] as int == 1);
  }

  Future<List<Map<String, dynamic>>> getColumns(
      [List<String>? columnsSelected]) async {
    var database = await DBManager().db;

    //print(res);
    if (!await isTableExists()) {
      return [];
    }
    return await database.query(tableName, columns: columnsSelected);
  }

  Future<Map<String, Object?>> getItem(int id,
      [List<String>? columnsSelected]) async {
    var database = await DBManager().db;
    return (await database.query(
      tableName,
      columns: columnsSelected,
      where: 'id = ?',
      whereArgs: [id],
    ))[0];
  }

  Future<Map<String, Object?>?> getItemWhere(
      {required String where, required String whereArgs}) async {
    var database = await DBManager().db;
    try {
      List<Map<String, Object?>> data = await database.query(
        tableName,
        where: '$where = ?',
        whereArgs: [whereArgs],
      );
      if (data.isNotEmpty) {
        return data[0];
      }
    } on Exception catch(e) {
      return null;
    }
    return null;
  }

  Future<void> delete(int id) async {
    var database = await DBManager().db;
    database.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
 */
