// https://stackoverflow.com/questions/60029796/flutter-custom-appbar-display-with-user-progress-indicator

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart' as rp;
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class SyncProgress extends rp.ConsumerWidget {
  const SyncProgress({
    required this.numTasksCompleted,
    required this.tasksList,
    Key? key,
  }) : super(key: key);
  final int numTasksCompleted;
  final List<String> tasksList;

  @override
  Widget build(BuildContext context, rp.WidgetRef ref) {
    final activeColor = Theme.of(context).primaryColor;
    final inactiveColor = Theme.of(context).disabledColor;

    final items = <Widget>[];

    tasksList.asMap().forEach((i, title) {
      final iconColor = (numTasksCompleted >= i) ? activeColor : inactiveColor;
      final icon = (numTasksCompleted >= i)
          ? MdiIcons.timerSandComplete
          : MdiIcons.timerSandEmpty;
      items.add(
        ListTile(
          leading: Icon(icon, color: iconColor),
          title: Text(title),
        ),
      );
    });

    return Column(
      children: items,
    );
  }
}
