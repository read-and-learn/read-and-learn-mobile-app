import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../pages/stories/stories_page.dart';

class ShowDrawerOrGoHome extends StatelessWidget {
  const ShowDrawerOrGoHome({
    required this.scaffoldKey,
    Key? key,
  }) : super(key: key);
  final GlobalKey<ScaffoldState> scaffoldKey;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        try {
          scaffoldKey.currentState!.openDrawer();
        } on Exception {
          context.goNamed(StoriesPage.name);
        }
      },
      icon: const Icon(Icons.menu),
    );
  }
}
