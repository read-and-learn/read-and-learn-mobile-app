/* import 'package:basic_services/basic_services.dart';
import 'package:custom_widgets/custom_widgets.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'widgets/authenticate_cancel.dart';

class AuthForm extends ConsumerStatefulWidget {
  const AuthForm({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AuthFormState();
}

class _AuthFormState extends ConsumerState<AuthForm> {
  final h = 100.0;
  @override
  Widget build(BuildContext context) {
    return CLFullscreenBox(
      child: SizedBox(
        height: CLFullscreenBox.size(context).height,
        width: CLFullscreenBox.size(context).width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: h,
              child: SingleChildScrollView(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: CLFullscreenBox.size(context).width,
                    minHeight: h,
                  ),
                  child: IntrinsicHeight(
                    child: SignIn(
                      key: const ValueKey("TODO"),
                      signUp: true, // TODO
                      onFlip: () {}, // TODO
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SignIn extends ConsumerStatefulWidget {
  final Function() onFlip;
  final bool signUp;
  const SignIn({super.key, required this.onFlip, required this.signUp});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _SignInState();
}

class _SignInState extends ConsumerState<SignIn> {
  final formKey = GlobalKey<FormState>();

  bool signin = true;
  bool termsAccepted = false;
  late final TextEditingController nameController;
  late final TextEditingController passwordController;
  late final TextEditingController confirmPasswordController;

  late final FocusNode nameFocus;
  late final FocusNode passwordFocus;
  late final FocusNode confirmPasswordFocus;
  @override
  void initState() {
    nameController = TextEditingController();
    passwordController = TextEditingController();
    confirmPasswordController = TextEditingController();

    nameFocus = FocusNode();
    passwordFocus = FocusNode();
    confirmPasswordFocus = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    nameFocus.dispose();
    passwordFocus.dispose();
    confirmPasswordFocus.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(60, 10, 60, 0),
            child: Center(
                child: Image.asset(
              "assets/icon/Logo Design.002.png",
            )),
          ),
          const Padding(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: Center(child: AuthenticateCancel()),
          ),
          ListTile(
            title:
                Text("Login", style: Theme.of(context).textTheme.displayLarge!),
            subtitle: Text("Please sign in to continue.",
                style: Theme.of(context).textTheme.bodySmall!),
          ),
          /* Align(
              alignment: Alignment.center,
              child: Text(
                widget.signUp ? "New Account" : "Log in",
                style: Theme.of(context).textTheme.displayLarge!,
              )), */
          Container(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextFormField(
              controller: nameController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'User Name',
                errorMaxLines: 3,

                // errorText:
                //     "User name must contain atleast 4 alphanumeric characters starting with an alphabet",
                //errorText: "User name must be atleast 4 letters",
                //  suffix: IconButton(onPressed: () {}, icon: const Icon(MdiIcons.help))
              ),
              validator: (value) {
                if (value == null || value.isEmpty) return 'Provide user name';
                if (value.length < 4) {
                  return 'a minimum of 4 characters is required';
                }
                if (value.length < 16) {
                  return 'a maximum of 16 characters is required';
                }
                RegExp userRegExp = RegExp('^[A-Za-z]([A-Za-z0-9]){4,15}\$');
                if (!userRegExp.hasMatch(value)) {
                  return "first letter must be an alphabet and remaining may contain alphanumeric characters"
                      "use A-Za-z0-9 only. Unicode not supported";
                }

                return null;
              },
            ),
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextFormField(
              obscureText: true,
              controller: passwordController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
                //errorText: "Password can't be empty",
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'You must set a password';
                }
                return null;
              },
            ),
          ),
          if (widget.signUp) ...[
            Container(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: TextFormField(
                obscureText: true,
                controller: confirmPasswordController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Confirm Password',
                  //errorText: "Password can't be empty",
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Reenter password to confirm';
                  }
                  if (confirmPasswordController.text !=
                      passwordController.text) {
                    return "password not matching";
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () {
                        setState(() {
                          termsAccepted = !termsAccepted;
                        });
                      },
                      icon: Icon(termsAccepted
                          ? MdiIcons.checkboxMarkedOutline
                          : MdiIcons.checkboxBlankOutline)),
                  Flexible(
                    child: Text.rich(
                      TextSpan(children: [
                        const TextSpan(text: "I read and agree with "),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                URLManager.getURLManager(
                                        url:
                                            "https://cloudonlanapps.com/readandlearn_privacy_policy")
                                    .launch(context);
                              },
                            text: "Privacy Policy",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge!
                                .copyWith(color: Colors.blue.shade400)),
                        const TextSpan(text: " "),
                        WidgetSpan(
                            alignment: PlaceholderAlignment.middle,
                            child: InkWell(
                                onTap: () {
                                  URLManager.getURLManager(
                                          url:
                                              "https://cloudonlanapps.com/readandlearn_privacy_policy")
                                      .launch(context);
                                },
                                child: Icon(
                                  MdiIcons.openInNew,
                                  color: Colors.blue.shade400,
                                )))
                      ]),
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  )
                ],
              ),
            )
          ],
          /*  Padding(
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            child: Text(
              "You must accept the Privacy policy to create account",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge!
                  .copyWith(color: Theme.of(context).errorColor),
            ),
          ), */
          Center(
              child: Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: ElevatedButton(
              child: Text(widget.signUp ? 'Sign up' : 'LOGIN',
                  style: Theme.of(context).textTheme.bodyLarge!),
              onPressed: () {
                print(nameController.text);
                print(passwordController.text);
              },
            ),
          )),
          const Spacer(),
          Align(
            alignment: Alignment.center,
            child: Text.rich(TextSpan(children: [
              TextSpan(
                  text: "${widget.signUp ? "H" : "Doesn't h"}ave account?",
                  style: Theme.of(context).textTheme.bodyLarge),
              TextSpan(
                  recognizer: TapGestureRecognizer()..onTap = widget.onFlip,
                  text: " ${widget.signUp ? "Sign In" : "Sign Up"}",
                  style: Theme.of(context)
                      .textTheme
                      .displaySmall!
                      .copyWith(color: Colors.blue.shade400))
            ])),
          ),
        ],
      ),
    );
  }

  confirmPasswordValidator(value) {
    if (value == null || value.isEmpty) {
      return 'Reenter password to confirm';
    }
    if (confirmPasswordController.text != passwordController.text) {
      return "password not matching";
    }
    return null;
  }
}

//padding: const EdgeInsets.fromLTRB(10, 10, 10, 0), */
