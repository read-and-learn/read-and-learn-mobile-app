/* import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class TermsOfCondition extends ConsumerWidget {
  final Function() onToggle;
  const TermsOfCondition({super.key, required this.onToggle});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
              onPressed: () {
                setState(() {
                  termsAccepted = !termsAccepted;
                });
              },
              icon: Icon(termsAccepted
                  ? MdiIcons.checkboxMarkedOutline
                  : MdiIcons.checkboxBlankOutline)),
          Flexible(
            child: Text.rich(
              TextSpan(children: [
                const TextSpan(text: "I read and agree with "),
                TextSpan(
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        URLManager.getURLManager(
                                url:
                                    "https://cloudonlanapps.com/readandlearn_privacy_policy")
                            .launch(context);
                      },
                    text: "Privacy Policy",
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: Colors.blue.shade400)),
                const TextSpan(text: " "),
                WidgetSpan(
                    alignment: PlaceholderAlignment.middle,
                    child: InkWell(
                        onTap: () {
                          URLManager.getURLManager(
                                  url:
                                      "https://cloudonlanapps.com/readandlearn_privacy_policy")
                              .launch(context);
                        },
                        child: Icon(
                          MdiIcons.openInNew,
                          color: Colors.blue.shade400,
                        )))
              ]),
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          )
        ],
      ),
    );
  }
}
 */
