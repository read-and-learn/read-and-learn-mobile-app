import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SafePop extends StatelessWidget {
  const SafePop({
    required this.onFail,
    Key? key,
  }) : super(key: key);
  final Function() onFail;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        try {
          context.pop();
        } on Exception {
          onFail();
        }
      },
      icon: const Icon(Icons.close_fullscreen_sharp),
    );
  }
}
