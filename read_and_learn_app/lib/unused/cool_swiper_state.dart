/* import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'cool_swiper_card.dart';

class CoolSwiperState {
  final int topCard;
  final List<Widget> _cards;
  final int totalCards;
  CoolSwiperState({
    List<Widget>? cards,
    this.topCard = 1,
  })  : _cards = cards ?? [],
        totalCards = cards?.length ?? 0;

  CoolSwiperState copyWith({
    int? topCard,
  }) {
    return CoolSwiperState(
      topCard: topCard ?? this.topCard,
    );
  }

  List<Object> rotate(List<Object> list, int v) {
    if (list.isEmpty) return list;
    var i = v % list.length;
    return list.sublist(i)..addAll(list.sublist(0, i));
  }

  List<Widget> get cards => rotate(_cards, topCard) as List<Widget>;
  List<SwiperCard> get swiperCards => SwiperCard.listFromWidgets(cards);
  List<Widget> stStackChildren(
      {required Function() onAnimationTrigger,
      required double height,
      required double initAnimationOffset}) {
    return List.generate(
      swiperCards.length,
      (i) {
        return CoolSwiperCard(
          key: ValueKey('__animated_card_${i}__'),
          card: swiperCards[i],
          height: height,
          initAnimationOffset: initAnimationOffset,
          onAnimationTrigger: onAnimationTrigger,
          onVerticalDragEnd: () {},
        );
      },
    );
  }

  @override
  String toString() => 'CoolSwiperState( topCard: $topCard)';

  @override
  bool operator ==(covariant CoolSwiperState other) {
    if (identical(this, other)) return true;

    return other.topCard == topCard;
  }

  @override
  int get hashCode => topCard.hashCode;

  CoolSwiperState onSwipe() => copyWith(topCard: (topCard + 1) % totalCards);
}

class CoolSwiperStateNotifier extends StateNotifier<CoolSwiperState> {
  CoolSwiperState initialValue;
  CoolSwiperStateNotifier(this.initialValue) : super(initialValue);

  onSwipe() => state = state.onSwipe();
}

final coolSwiperStateProvider =
    StateNotifierProvider<CoolSwiperStateNotifier, CoolSwiperState>((ref) {
  return CoolSwiperStateNotifier(CoolSwiperState());
});
 */
