# Read and Learn 

https://cloudonlanapps.com/readandlearn_about

## Build for PlayStore

1. Sync the project from git.
2. copy these two files from Secured file area
   1. read_and_learn_app/android/key.properties
   2. read_and_learn_app/android/app/upload-keystore.jks
3. build appbundle
```
cd read_and_learn_app
flutter build appbundle --release --no-tree-shake-icons
```

## build APK
flutter build apk --release --no-tree-shake-icons
